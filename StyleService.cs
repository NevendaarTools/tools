﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace NevendaarTools
{
    public class Style
    {
        Dictionary<string, Color> colors = new Dictionary<string, Color>();
        Dictionary<string, Image> images = new Dictionary<string, Image>();
        Dictionary<string, string> custom = new Dictionary<string, string>();
        public string name;
        public string path;
        public Color GetColor(string name)
        {
            if (colors.ContainsKey(name))
                return colors[name];
            return Color.Black;
        }
        public void SetColor(string name, Color color)
        {
            if (colors.ContainsKey(name))
                colors[name] = color;
            else
                colors.Add(name, color);
        }

        public Image GetImage(string name)
        {
            if (images.ContainsKey(name))
            {
                if (images[name] != null)
                    return images[name].Clone() as Image;
                return null;
            }
            return new Bitmap(100, 100);
        }

        public void SetImage(string name, Image image)
        {
            if (images.ContainsKey(name))
                images[name] = image;
            else
                images.Add(name, image);
        }
        public string GetCustom(string name)
        {
            if (custom.ContainsKey(name))
                return custom[name];
            return "";
        }

        public int GetCustomInt(string name)
        {
            return int.Parse(GetCustom(name));
        }

        public void SetCustom(string name, string str)
        {
            if (custom.ContainsKey(name))
                custom[name] = str;
            else
                custom.Add(name, str);
        }
    };
    public class StyleService
    {
        public delegate void StyleChangedDelegate();
        public StyleChangedDelegate StyleChanged;

        private static StyleService _instance = null;

        public static StyleService Instance()
        {
            if (_instance == null)
                _instance = new StyleService();

            return _instance;
        }

        public static Color GetColor(string name)
        {
            Style current = StyleService.Instance().current;
            if (current == null)
                return new Color();
            return current.GetColor(name);
        }

        public static Image GetImage(string name)
        {
            Style current = StyleService.Instance().current;
            if (current == null)
                return new Bitmap(20,20);
            return current.GetImage(name);
        }

        public static string GetCustom(string name)
        {
            Style current = StyleService.Instance().current;
            if (current == null)
                return "";
            return current.GetCustom(name);
        }

        public static int GetCustomInt(string name)
        {
            Style current = StyleService.Instance().current;
            if (current == null)
                return 0;
            return current.GetCustomInt(name);
        }

        public static void ApplyStyle(Control control)
        {
            control.BackColor = GetColor("back");

            if (control is LinkLabel label)
                label.LinkColor = GetColor("font");
            else if (control is NumericUpDown upDown)
                upDown.BackColor = GetColor("font");
            else
                control.ForeColor = GetColor("font");

            foreach (Control subControl in control.Controls)
                ApplyStyle(subControl);
        }

        private StyleService()
        {
            Preload();
        }

        public List<Style> styles = new List<Style>();
        public Style current = null;
        public GameModel gameData = null;


        public void Preload()
        {
            string[] s;
            s = System.IO.Directory.GetFiles(System.IO.Directory.GetCurrentDirectory() + "/Styles/", "*.xml");
            foreach (string s2 in s)
            {
                //string name = System.IO.Path.GetFileNameWithoutExtension(s2);
                LoadStyle(s2);
            }
        }

        public void SetStyle(int index)
        {
            LoadStyle(styles[index].path);
            current = styles[index];
            StyleChanged?.Invoke();
        }

        public bool LoadStyle(string fileName)
        {
            if (File.Exists(fileName))// Checking the file if exist
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(fileName);
                Style style = new Style();
                style.path = fileName;
                foreach (XmlNode node in doc.DocumentElement)
                {
                    string key = node.Name;
                    string value = node.InnerText;
                    if (key == "name")
                    {
                        style.name = value;
                    }
                    if (key.StartsWith("image_"))
                    {
                        if (gameData == null)
                        {
                            continue;
                        }
                        key = key.Replace("image_", "");
                        if (value == "none")
                        {
                            style.SetImage(key, null);
                        }    
                        else if (value.StartsWith("Game:"))
                        {
                            string[] data = value.Split(new[] { ":" }, StringSplitOptions.None);
                            if (data.Length < 3)
                                continue;
                            Image img = gameData._ResourceModel.GetImageById(data[1], data[2]);
                            style.SetImage(key, img);
                        }
                        else
                        {
                            style.SetImage(key, Image.FromFile("Styles/" + value));
                        }

                    }
                    else if (key.StartsWith("color_"))
                    {
                        key = key.Replace("color_", "");
                        if (value.StartsWith("#"))
                        {
                            Color color = ColorTranslator.FromHtml(value);
                            style.SetColor(key, color);
                        }
                        else
                        {
                            string[] data = value.Split(new[] { "," }, StringSplitOptions.None);
                            if (data.Length < 3)
                                continue;
                            int a = 255;
                            if (data.Length == 4)
                                a = int.Parse(data[3]);
                            style.SetColor(key, Color.FromArgb(a, int.Parse(data[0]),
                                int.Parse(data[1]), int.Parse(data[2])));
                        }
                    }
                    else if (key.StartsWith("custom_"))
                    {
                        key = key.Replace("custom_", "");
                        style.SetCustom(key, value);
                    }
                }
                for(int i = 0; i < styles.Count; ++i)
                {
                    if (styles[i].name == style.name)
                    {
                        styles[i] = style;
                        return true;
                    }
                }
                styles.Add(style);
                return true;
            }
            return false;
        }
    }
}
