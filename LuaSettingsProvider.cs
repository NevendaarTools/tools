﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NevendaarTools
{
    public class LuaSettings
    {
        public string name = "";
        public string value;
        public Dictionary<string, LuaSettings> properties = new Dictionary<string, LuaSettings>();
        public string getProperty(string propName)
        {
            if (name == propName)
            {
                return value;
            }
            if (properties.ContainsKey(propName))
            {
                return properties[propName].getProperty(propName);
            }
            if (propName.Contains("."))
            {
                int index = propName.IndexOf(".");
                string first = propName.Substring(0, index);
                if (properties.ContainsKey(first))
                {
                    string second = propName.Substring(index + 1);
                    return properties[first].getProperty(second);
                }
               return "";
            }
            return "";
        }
    }
    public class LuaSettingsAdapter
    {
        private LuaSettings _Settings;

        public string getProperty(string name)
        {
            if (_Settings == null)
                return "";
            string tmp = "root." + name;
            return _Settings.getProperty(name);
        }


        public bool HasLuaSettings()
        {
            return _Settings != null;
        }

        public bool Read(string fileName)
        {
            _Settings = null;
            if (!File.Exists(fileName))
            {
                Logger.LogWarning("Lua settings file not found:" + fileName);
                return false;
            }
            string content = "";
            using (StreamReader sr = File.OpenText(fileName))
            {
                string s = String.Empty;
                while ((s = sr.ReadLine()) != null)
                {
                    if (s.Trim().StartsWith("--"))
                        continue;
                    content += s.Trim();
                }
            }

            //File.ReadAllText(fileName);
            int startIndex = content.IndexOf("{") + 1;
            int endIndex = content.LastIndexOf("}");
            content = content.Substring(startIndex, endIndex - startIndex - 2);
            _Settings = new LuaSettings();
            _Settings.name = "root";
            Read2(content, ref _Settings);
            return true;
        }
        string FindEndIndex(string content, ref int index)
        {
            string tmp = content.Substring(index);
            int startIndex = index ;
            int level = 0;
            for (int i = index; i < content.Length; i++)
            {
                if (content[i] == '{')
                {
                    level++;
                    continue;
                }
                if (content[i] == '}')
                {
                    level--;
                    if (level == 0)
                    {
                        startIndex = content.IndexOf("{") + 1;
                        index = i;
                        return content.Substring(startIndex, index - startIndex);
                    }
                    continue;
                }
                if (content[i] == ',' && level == 0)
                {
                    index = i - 1;
                    return content.Substring(startIndex, index - startIndex + 1);
                }
            }
            index = content.Length - 1;
            return content.Substring(startIndex, index - startIndex + 1);
        }
        void Read2(string content, ref LuaSettings settings)
        {
            string name = "";
            for (int i = 0; i < content.Length; i++)
            {
                if (content[i] == '=')
                {
                    LuaSettings child = new LuaSettings();
                    child.name = name.Trim();
                    i++;//skip '=' 
                    string tmp = content.Substring(i);
                    int tmpIndex = 0;
                    string childData = FindEndIndex(tmp, ref tmpIndex);
                    i += tmpIndex;
                    Read2(childData, ref child);
                    if (settings.properties.ContainsKey(child.name))
                    {
                        Logger.LogError("LuaSettings error! key already exist: "  + settings.name + ":" + child.name);
                    }
                    else
                        settings.properties.Add(child.name, child);
                    name = "";
                    continue;
                }
                
                if (content[i] == ',')
                {
                    continue;
                }
                name += content[i];
            }
            if (name != "")
                settings.value = name.Trim();
        }

        void Read(string content, ref LuaSettings settings)
        {
            string[] data = content.Split(',');
            foreach (string str in data)
            {
                if (str.Contains("{"))
                {
                    string tmp = str.Substring(str.IndexOf("{") + 1, str.LastIndexOf("}"));
                    LuaSettings child = new LuaSettings();
                    child.name = str.Substring(0, str.IndexOf("=")).Trim();
                    Read(tmp, ref child);
                    settings.properties.Add(child.name, child);
                }
                else
                {
                    string[] strData = str.Split('=');
                    LuaSettings child = new LuaSettings();
                    child.name = strData[0].Trim();
                    child.value = strData[1].Trim();
                    settings.properties.Add(child.name, child);
                }
            }
        }
    }
}
