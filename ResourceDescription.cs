﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace NevendaarTools
{
    
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class PackedImageInfo
    {
        public byte TransparentColor;
        public ushort Opacity;
        public uint Width;
        public uint Height;

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        public struct PaletteEntry
        {
            public byte Red;
            public byte Green;
            public byte Blue;
            public byte Flags;
        }

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 256)]
        public PaletteEntry[] Palette = new PackedImageInfo.PaletteEntry[256];

        public void Read(ref int index, ref byte[] bytes)
        {
            if (bytes.Length < index + Marshal.SizeOf(typeof(PackedImageInfo)))
            {
                throw new ArgumentException("Массив байтов слишком мал для чтения PackedImageInfo");
            }
            TransparentColor = bytes[index++];
            Opacity = BitConverter.ToUInt16(bytes, index);
            index += 2;
            Width = BitConverter.ToUInt32(bytes, index);
            index += 4;
            Height = BitConverter.ToUInt32(bytes, index);
            index += 4;
            for (int i = 0; i < 256; i++)
            {
                Palette[i].Red = bytes[index++];
                Palette[i].Green = bytes[index++];
                Palette[i].Blue = bytes[index++];
                Palette[i].Flags = bytes[index++];
            }
        }
    }


    public interface MQRCCustomData
    {
        byte[] GetData(string filename, int pos, int size);
        bool SetData(byte[] data);
    }

    public class CustomByteData : MQRCCustomData
    {
        public byte[] data = null;
        public byte[] GetData(string filename, int offset, int size)
        {
            if (data != null)
                return data;
            byte[] array = new byte[size];
            using (FileStream fstream = File.OpenRead(filename))
            {
                fstream.Seek(offset, SeekOrigin.Begin);
                fstream.Read(array, 0, array.Length);
            }
            return array;
        }
        public bool SetData(byte[] newData)
        {
            data = newData;
            return true;
        }
    }

    public class ResourceDesc
    {
        public Int32 uid;
        public int offset;
        public int size;
        public Int32 magic;//0
        public Int32 magic2;//size duplicate
        public Int32 magic3;//1
        public Int32 magic4;//0
        public MQRCCustomData data;

        public string name;//exra data, restored from "2" data block

        public override string ToString()
        {
            return name + "   " + uid.ToString() + " pos = " + offset.ToString() + " - " + size.ToString();
        }

        public long Read(FileStream stream)
        {
            offset = (int)stream.Position;
            stream.Position += 4;//"MQRC"
            magic = StreamHelper.ReadInt(stream);
            uid = StreamHelper.ReadInt(stream);
            //name = uid.ToString("X");
            size = (StreamHelper.ReadInt(stream));
            magic2 = StreamHelper.ReadInt(stream);
            magic3 = StreamHelper.ReadInt(stream);
            magic4 = StreamHelper.ReadInt(stream);//end of 28 bytes header
            data = new CustomByteData();
            
            return offset + size;
        }

        public byte[] GetData(string filename)
        {
            return data.GetData(filename, offset + 28, size);
        }

        public bool Write(FileStream stream)
        {

            return true;
        }
    };

    public class NamesListData : MQRCCustomData
    {
        public struct Binding
        {
            public Int32 id;
            public string name;
            public override string ToString()
            {
                return id.ToString() + " - " + name;
            }
        }

        public List<Binding> bindings = new List<Binding>();
        public byte[] GetData(string filename, int offset, int size)
        {
            return new byte[0];
        }
        public bool SetData(byte[] newData)
        {
            for (int k = 4; k < newData.Length; k += 260)
            {
                int hIndex = k;
                string name = StreamHelper.ReadString(ref newData, ref hIndex, 256);
                name = name.Replace("\0", "");
                hIndex = k + 256;
                Int32 id = StreamHelper.ReadInt(ref newData, ref hIndex, 4);
                bindings.Add(new Binding() { name = name, id = id });
            }

            return true;
        }
    }

    public class IndexesData : MQRCCustomData
    {
        public class IndexesDesc
        {
            public Int32 uid;
            public string name;
            public string imageName;
            public int relatedOffset;//in imgeMap or anim map
            public int relatedSize;
            public override string ToString()
            {
                return "index: " + uid.ToString() + "  name: " + name;
            }
        };
        public List<IndexesDesc> indexesData = new List<IndexesDesc>();
        public byte[] GetData(string filename, int offset, int size)
        {
            return new byte[0];
        }
        public bool SetData(byte[] newData)
        {
            int index = 0;
            int count = StreamHelper.ReadInt(ref newData, ref index, 4);
            for (int i = 0; i < count; ++i)
            {
                IndexesDesc indDesc = new IndexesDesc();
                indDesc.uid = StreamHelper.ReadInt(ref newData, ref index, 4);
                indDesc.name = StreamHelper.ReadStringWithTerm2(ref newData, ref index);
                indDesc.relatedOffset = StreamHelper.ReadInt(ref newData, ref index, 4);
                indDesc.relatedSize = StreamHelper.ReadInt(ref newData, ref index, 4);
                indexesData.Add(indDesc);
            }

            return true;
        }
    }

    public class AnimationsContainer : MQRCCustomData
    {
        public class AnimationsDesc
        {
            public List<string> frameNames = new List<string>();
            public int localOffset;
            public int size;
            public override string ToString()
            {
                return "Frames: " + string.Join(" ", frameNames);
            }
        };
        public List<AnimationsDesc> animationsData = new List<AnimationsDesc>();
        public byte[] GetData(string filename, int offset, int size)
        {
            return new byte[0];
        }
        public AnimationsDesc Read(ref int index, ref byte[] bytes)
        {
            AnimationsDesc resultData = new AnimationsDesc();
            resultData.localOffset = index;
            int count = StreamHelper.ReadInt(ref bytes, ref index, 4);
            for (int i = 0; i < count; ++i)
            {
                resultData.frameNames.Add(StreamHelper.ReadStringWithTerm2(ref bytes, ref index));
            }
            resultData.size = index - resultData.localOffset;
            return resultData;
        }

        public AnimationsDesc GetAnimationsDesc(int offset, int size, string path)
        {
            byte[] array = new byte[size];
            using (FileStream fstream = File.OpenRead(path))
            {
                fstream.Seek(offset, SeekOrigin.Begin);
                fstream.Read(array, 0, array.Length);
            }
            int tmpOffset = 0;
            return Read(ref tmpOffset, ref array);
        }

        public bool SetData(byte[] newData)
        {
            int index = 0;
            while (index < newData.Length)
            {
                animationsData.Add(Read(ref index, ref newData));
            }

            return true;
        }
    }

    public class ImagesContainer : MQRCCustomData
    {
        public class ImageRestoreData
        {
            public struct ImagePart
            {
                public int sourceX;
                public int sourceY;
                public int targetX;
                public int targetY;
                public int w;
                public int h;
            }
            public int w;
            public int h;
            public string name;
            public List<ImagePart> parts = new List<ImagePart>();
            public override string ToString()
            {
                return name + " - [" + w.ToString() + "x" + h.ToString() + "]  parts: " + parts.Count(); ;
            }
        };

        public class ImageData
        {
            public List<ImageRestoreData> restoreData = new List<ImageRestoreData>();
            public PackedImageInfo header = new PackedImageInfo();
            public int localOffset;
            public int localSize;
            public override string ToString()
            {
                return "Frames count: " + restoreData.Count;
            }
        }

        public ImageData GetImageData(int offset, int size, string path)
        {
            byte[] array = new byte[size];
            using (FileStream fstream = File.OpenRead(path))
            {
                fstream.Seek(offset, SeekOrigin.Begin);
                fstream.Read(array, 0, array.Length);
            }
            int tmpOffset = 0;
            return Read(ref tmpOffset, ref array);
        }
        public List<ImageData> imagesData = new List<ImageData>();
        public byte[] GetData(string filename, int offset, int size)
        {
            return new byte[0];
        }

        public ImageData Read(ref int index, ref byte[] bytes)
        {
            ImageData resultData = new ImageData();
            resultData.localOffset = index;
            resultData.header.Read(ref index, ref bytes);
            int imagesCount = StreamHelper.ReadInt(ref bytes, ref index, 4);
            for (int k = 0; k < imagesCount; ++k)
            {
                ImageRestoreData restoreData = new ImageRestoreData();
                restoreData.name = StreamHelper.ReadStringWithTerm2(ref bytes, ref index);
                int count = StreamHelper.ReadInt(ref bytes, ref index, 4);
                restoreData.w = StreamHelper.ReadInt(ref bytes, ref index, 4);
                restoreData.h = StreamHelper.ReadInt(ref bytes, ref index, 4);
                for (int i = 0; i < count; ++i)
                {
                    ImageRestoreData.ImagePart part = new ImageRestoreData.ImagePart();
                    part.sourceX = StreamHelper.ReadInt(ref bytes, ref index, 4);
                    part.sourceY = StreamHelper.ReadInt(ref bytes, ref index, 4);
                    part.targetX = StreamHelper.ReadInt(ref bytes, ref index, 4);
                    part.targetY = StreamHelper.ReadInt(ref bytes, ref index, 4);
                    part.w = StreamHelper.ReadInt(ref bytes, ref index, 4);
                    part.h = StreamHelper.ReadInt(ref bytes, ref index, 4);
                    restoreData.parts.Add(part);
                }
                resultData.restoreData.Add(restoreData);
            }
            resultData.localSize = index - resultData.localOffset;
            return resultData;
        }

        public bool SetData(byte[] newData)
        {
            int index = 0;
            
            while (index < newData.Length)
            {
                imagesData.Add(Read(ref index, ref newData));
            }

            return true;
        }
    }

}
