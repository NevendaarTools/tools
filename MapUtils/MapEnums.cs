﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.MapUtils
{
   
    public class Enums
    {
        public static uint TileTerrain(UInt32 tile)
        {
            return (tile & 7);
        }

        public static uint TileGround(UInt32 tile)
        {
            return ((tile >> 3) & 7);
        }

        public enum GroundType
        {
            TREE = 1,
            WATER = 3

        }

        public enum ResourceType
        {
            GOLG = 0,
            DEMONS = 1,
            EMPIRE = 2,
            UNDEAD = 3,
            CLANS = 4,
            ELVES = 5
        }

        public static string CrystalByResource(ResourceType resource)
        {
            switch (resource)
            {
                case ResourceType.GOLG:
                    return "GOLD";
                case ResourceType.DEMONS:
                    return "RED";
                case ResourceType.EMPIRE:
                    return "YELLOW";
                case ResourceType.UNDEAD:
                    return "ORANGE";
                case ResourceType.CLANS:
                    return "WHITE";
                case ResourceType.ELVES:
                    return "BLUE";
            }
            return "GOLD";
        }
    }

}
