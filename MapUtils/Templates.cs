﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.MapUtils
{
    public class StackTemplate
    {
        public string locationName = "";
        public string name = "";
        public int _LeaderIndex = 0;

        public string extraData = "";
        public bool template = false;
        public bool playableOwner = false;
        public int order = 1;
        public string orderTarget = "";
        public int aiOrder = 1;
        public string aiOrderTarget = "";
        public int mapX = 0;
        public int mapY = 0;


        public string[] _Unit = { "G000UU7505", "G000000000", "G000000000", "G000000000", "G000000000", "G000000000" };
        public int[] _UnitLvl = { 1, 0, 0, 0, 0, 0 };
        public int[] _Pos = { 0, -1, -1, -1, -1, -1 };
        public List<string>[] _Mods = { new List<string>(), new List<string>() , new List<string>(),
                                        new List<string>(), new List<string>() , new List<string>() };
        public List<string> _Items = new List<string>();
        public List<string> LeaderMods()
        {
            return _Mods[_LeaderIndex];
        }
        public void SetLeaderMods(List<string> mods)
        {
            _Mods[_LeaderIndex] = mods;
        }

        public override string ToString()
        {
            string result = locationName + " " + name + "\n";
            for (int i = 0; i < 6; i++)
            {
                result += i.ToString();
                if (_Pos[i] != -1)
                {
                    if (_LeaderIndex == i)
                        result += " Leader :";
                    result += " " + _Unit[_Pos[i]];
                    foreach (string mod in _Mods[_Pos[i]])
                        result += " " + mod;
                }
                result += "\n";
            }
            return result;
        }
        public string ToString(GameModel model)
        {
            string result = locationName + " " + name + "\n";
            for (int i = 0; i < 6; i++)
            {
                result += i.ToString();
                if (_Pos[i] != -1)
                {
                    if (_LeaderIndex == _Pos[i])
                        result += " Leader :";
                    Gunit unit = model.GetObjectT<Gunit>(_Unit[_Pos[i]]);
                    result += " " + unit.name_txt.value.text;
                    foreach (string mod in _Mods[_Pos[i]])
                        result += " " + mod;
                    
                }
                result += "\n";
            }
            LOrder order = model.GetObjectT<LOrder>(this.order);
            result += "order:" + order.text + " : " + this.orderTarget + "\n";
            return result;
        }
    }
}
