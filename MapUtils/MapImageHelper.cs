﻿using NevendaarTools.DataTypes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace NevendaarTools.MapUtils
{
    

    public class MapImageHelper
    {
        private static MapImageHelper _instance = null;
        Dictionary<int, Image> tilesMap = new Dictionary<int, Image>();
        bool initialised;
        
        public void Init(ref GameModel model)
        {
            if (initialised)
                return;
            List<Lterrain> terrains = model.GetAllT<Lterrain>();
            foreach (Lterrain terr in terrains)
            {
                string key = terr.text;
                key = key.Replace("L_", "");
                key = "ISO" + key;
                tilesMap.Add(terr.id, model._ResourceModel.GetImageById("PalMap", key));
            }
            tilesMap.Add(29, model._ResourceModel.GetImageById("PalMap", "ISOWA"));//water
            tilesMap.Add(37, model._ResourceModel.GetImageById("PalMap", "ISOMO"));//mountain
            initialised = true;
        }
        private MapImageHelper()
        {

        }

        public static MapImageHelper Instance()
        {
            if (_instance == null)
                _instance = new MapImageHelper();

            return _instance;
        }

        public static string ShortRaceByPlayerId(ref MapModel map, ref GameModel model, string id)
        {
            List<D2Player> players = map.GetListByType<D2Player>();
            foreach (D2Player player in players)
            {
                if (player._objId == id)
                {
                    Grace race = model.GetObjectT<Grace>(player._raceId);
                    string key = race.race_type.value.text;
                    return key.Replace("L_", "").Substring(0, 2);
                }
            }
            return null;
        }

        public static Image TileByValue(int value)
        {
            value = value & 255;
            if (Instance().tilesMap.ContainsKey(value))
                return Instance().tilesMap[value];
            value = value & ~8;//trees
            if (Instance().tilesMap.ContainsKey(value))
                return Instance().tilesMap[value];
            return new Bitmap(10, 10);
        }

        public static Image RaceListImage(ref MapModel map, ref GameModel model)
        {
            List<D2Player> players = map.GetListByType<D2Player>();
            int offset = 0;
            Bitmap racesImage = new Bitmap((players.Count - 1) * 14, 14);
            using (Graphics g = Graphics.FromImage(racesImage))
            {
                foreach (var player in players)
                {
                    Grace race = model.GetObjectT<Grace>(player._raceId);
                    if (race == null || !race.playable)
                        continue;
                    string key = race.race_type.value.text;
                    key = "ISO" + key.Replace("L_", "").Substring(0, 2);
                    Image raceCap = model._ResourceModel.GetImageById("PalMap", key);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    g.DrawImage(raceCap,
                        new Rectangle(offset, 0, 12, 12),
                        new Rectangle(0, 0, raceCap.Width, raceCap.Height),
                        units);
                    g.DrawRectangle(new Pen(Color.Black), new Rectangle(offset, 0, 12, 12));
                    if (player._alwaysai)
                    {
                        g.DrawLine(new Pen(Color.Red), offset, 0, offset + 12, 12);
                        g.DrawLine(new Pen(Color.Red), offset, 12, offset + 12, 0);
                    }
                    offset += 14;
                }
            }
            return racesImage;
        }

        public static Image MiniMap(ref MapModel map, ref GameModel model)
        {
            Instance().Init(ref model);

            D2Plan plan = map.GetById<D2Plan>("PN0000");
            int mapSize = plan._mapSize;
            int tileSize = 16;
            Image result = new Bitmap(mapSize * tileSize, mapSize * tileSize);
            List<D2MapBlock> blocks = map.GetListByType<D2MapBlock>();
            List<D2Village> villages = map.GetListByType<D2Village>();
            List<D2Capital> capitals = map.GetListByType<D2Capital>();
            List<D2Ruin> ruins = map.GetListByType<D2Ruin>();
            List<D2Stack> stacks = map.GetListByType<D2Stack>();
            List<D2Crystal> crystals = map.GetListByType<D2Crystal>();
            D2Mountains mountains = map.GetListByType<D2Mountains>()[0];
            using (Graphics g = Graphics.FromImage(result))
            {
                foreach (D2MapBlock block in blocks)
                {
                    int blockX = block.X();
                    int blockY = block.Y();
                    for (int i = 0; i < 32; ++i)
                    {
                        int x = blockX + i % 8;
                        int y = blockY + (int)(i / 8);
                        Image tile = TileByValue(block._blockData[i]);
                        GraphicsUnit units = GraphicsUnit.Pixel;
                        g.DrawImage(tile,
                            new Rectangle(tileSize * x, tileSize * y, tileSize, tileSize),
                            new Rectangle(0, 0, tile.Width, tile.Height),
                            units);
                    }
                }
                foreach (D2Village village in villages)
                {
                    int x = village._PosX;
                    int y = village._PosY;
                    string key = "CITY" + ShortRaceByPlayerId(ref map, ref model, village._Owner);
                    Image raceCap = model._ResourceModel.GetImageById("PalMap", key);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    g.DrawImage(raceCap,
                        new Rectangle(tileSize * x, tileSize * y, tileSize * 4, tileSize * 4),
                        new Rectangle(0, 0, raceCap.Width, raceCap.Height),
                        units);
                }
                foreach (D2Capital capital in capitals)
                {
                    int x = capital._PosX;
                    int y = capital._PosY;
                    string key = "CAP" + ShortRaceByPlayerId(ref map, ref model, capital._Owner);
                    Image raceCap = model._ResourceModel.GetImageById("PalMap", key);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    g.DrawImage(raceCap,
                        new Rectangle(tileSize * x, tileSize * y, tileSize * 5, tileSize * 5),
                        new Rectangle(0, 0, raceCap.Width, raceCap.Height),
                        units);
                }
                foreach (D2Mountains.Mountain mountain in mountains._mountains)
                {
                    int x = mountain.x;
                    int y = mountain.y;
                    string key = "MOUNTNE" + mountain.sizeX.ToString();
                    Image raceCap = model._ResourceModel.GetImageById("PalMap", key);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    g.DrawImage(raceCap,
                        new Rectangle(tileSize * x, tileSize * y, tileSize * mountain.sizeX, tileSize * mountain.sizeY),
                        new Rectangle(0, 0, raceCap.Width, raceCap.Height),
                        units);
                }
                foreach (D2Ruin ruin in ruins)
                {
                    int x = ruin._posX;
                    int y = ruin._posY;
                    string key = "RUIN";
                    Image raceCap = model._ResourceModel.GetImageById("PalMap", key);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    g.DrawImage(raceCap,
                        new Rectangle(tileSize * x, tileSize * y, tileSize * 2, tileSize * 2),
                        new Rectangle(0, 0, raceCap.Width, raceCap.Height),
                        units);
                }
                foreach (D2Stack stack in stacks)
                {
                    int x = stack._posX;
                    int y = stack._posY;
                    string key = "STCK" + ShortRaceByPlayerId(ref map, ref model, stack._owner);
                    Image raceCap = model._ResourceModel.GetImageById("PalMap", key);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    g.DrawImage(raceCap,
                        new Rectangle(tileSize * x, tileSize * y, tileSize, tileSize),
                        new Rectangle(0, 0, raceCap.Width, raceCap.Height),
                        units);
                }
                foreach (D2Crystal crystal in crystals)
                {
                    int x = crystal._PosX;
                    int y = crystal._PosY;
                    string key = "CRYS" + Enums.CrystalByResource((Enums.ResourceType)crystal._Resource);
                    Image raceCap = model._ResourceModel.GetImageById("PalMap", key);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    g.DrawImage(raceCap,
                        new Rectangle(tileSize * x, tileSize * y, tileSize, tileSize),
                        new Rectangle(0, 0, raceCap.Width, raceCap.Height),
                        units);
                }
            }
            return result;
        }

    }
}
