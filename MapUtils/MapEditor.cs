﻿using NevendaarTools.DataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.MapUtils
{
    public class IdsHelper
    {
        private Dictionary<string, List<int>> available = new Dictionary<string, List<int>>();
        public List<int> GetAvailableIds<T>(ref MapModel map, int count = 65536) where T : class, DataBlock
        {
            return map.GetAvailableIds<T>(count);
        }

        public string NextId<T>(ref MapModel map, string prefix) where T : class, DataBlock
        {
            string typeName = typeof(T).Name;
            if (!available.ContainsKey(typeName))
                available.Add(typeName, new List<int>());
            if (available[typeName].Count == 0)
                available[typeName] = GetAvailableIds<T>(ref map);
            string result = prefix + available[typeName][0].ToString("X").PadLeft(4, '0').ToLower();
            available[typeName].RemoveAt(0);
            return result;
        }
        public void Clear()
        {
            available.Clear();
        }
    }

    public class MapEditor
    {
        int CalcLeveledValue(int value, int startLevel, int resultLevel, int threshold, int up1, int up2)
        {
            int result = value;
            result += (Math.Min(resultLevel, threshold) - startLevel) * up1;
            result += (Math.Max(resultLevel, threshold) - threshold) * up2;
            return result;
        }

        public void ClearIds()
        {
            idsHelper.Clear();
        }

        public static void Rename(ref MapModel map, string name, string author)
        {
            D2ScenarioInfo info = map.GetById<D2ScenarioInfo>("IF0000");
            info._name = name;
            info._creator = author;
            map.Replace(info);
            map.Header._Name = name;
            map.Header._Author = author;
        }

        public bool RemoveItem(D2Item item, ref MapModel map, ref GameModel data)
        {
            GItem iteminfo = data.GetObjectT<GItem>(item._type);
            if (iteminfo.item_cat == (int)GItem.Category.Talisman)//talisman
            {
                D2TalismanCharges charges = map.GetListByType<D2TalismanCharges>()[0];
                for (int i = 0; i < charges._Charges.Count; ++i)
                {
                    if (charges._Charges[i].talismanId == item.ObjId())
                    {
                        charges._Charges.RemoveAt(i);
                        break;
                    }
                }
                map.Replace(charges);
            }
            map.RemoveDataBlockById(item._objId);

            return true;
        }

        MapModel map;
        GameModel model;


        IdsHelper idsHelper = new IdsHelper();
        public class MapCell
        {
            public int value = (int)D2MapBlock.TileType.Neutrals;
            public List<string> objIds = new List<string>();
        }

        public MapCell[,] grid;
        private void FillGrid()
        {
            int mapSize = map.Header._mapSize;
            grid = new MapCell[mapSize, mapSize];
            List<D2MapBlock> blocks = map.GetListByType<D2MapBlock>();
            foreach (D2MapBlock block in blocks)
            {
                int blockX = block.X();
                int blockY = block.Y();
                for (int i = 0; i < 32; ++i)
                {
                    int x = blockX + i % 8;
                    int y = blockY + (int)(i / 8);
                    grid[x, y] = new MapCell();
                    grid[x, y].value = block._blockData[i];
                }
            }
            D2Plan plan = map.GetById<D2Plan>("PN0000");
            foreach(D2Plan.PlanElement element in plan._elements)
            {
                grid[element._posX, element._posY].objIds.Add(element._id);
            }
            plan._elements.Clear();
        }

        private void CommitGrid()
        {
            int mapSize = map.Header._mapSize;

            //List<D2MapBlock> blocks = map.GetListByType<D2MapBlock>(); 
            //foreach(D2MapBlock block in blocks)
            //{
            //    map.RemoveDataBlockById(block._objId);
            //}
            for (int i = 0; i < mapSize; i += 4)
            {
                for (int k = 0; k < mapSize; k += 8)
                {
                    string id = "MB" + i.ToString("X").PadLeft(2, '0') + k.ToString("X").PadLeft(2, '0').ToLower();
                    D2MapBlock mapBlock = map.GetById<D2MapBlock>(id);

                    for (int q = 0; q < 32; ++q)
                    {
                        int x = k + q % 8;
                        int y = i + (int)(q / 8);
                        mapBlock._blockData[q] = grid[x, y].value;
                    }
                    map.Replace(mapBlock);
                }
            }
            D2Plan plan = map.GetById<D2Plan>("PN0000");
            plan._elements.Clear();
            for (int i = 0; i < mapSize; ++i)
            {
                for (int k = 0; k < mapSize; k++)
                {
                    foreach (string objid in grid[i, k].objIds)
                        plan._elements.Add(new D2Plan.PlanElement()
                        {
                            _posX = i,
                            _posY = k,
                            _id = objid
                        });
                }
            }
            map.Replace(plan);
        }

        public void Finish()
        {
            CommitGrid();
        }

        public MapModel Map()
        {
            return map;
        }

        public GameModel DataModel()
        {
            return model;
        }


        public void Init(ref MapModel map, ref GameModel data)
        {
            this.map = map;
            this.model = data;
            FillGrid();
        }

        public void Rename(string name, string author)
        {
            Rename(ref map, name, author);
        }

        public bool RemoveItem(D2Item item)
        {
            return RemoveItem(item, ref map, ref model);
        }

        public D2Item AddItem(string type)
        {
            D2Item d2item = new D2Item
            {
                _objId = idsHelper.NextId<D2Item>(ref map, "IM"),
                _type = type.ToUpper()
            };
            GItem iteminfo = model.GetObjectT<GItem>(d2item._type);
            if (iteminfo.item_cat == (int)GItem.Category.Talisman)//talisman
            {
                D2TalismanCharges charges = map.GetListByType<D2TalismanCharges>()[0];
                int chargesCount = model.GetAllT<GVars>()[0].talis_chrg;
                charges._Charges.Add(new D2TalismanCharges.TalismanChargeInfo() { talismanId = d2item.ObjId(), count = chargesCount });
                map.Replace(charges);
            }
            map.AddDataBlock(d2item);
            return d2item;
        }

        public void RemoveObjects(List<string> ids)
        {
            foreach (string id in ids)
                RemoveObject(id);
        }

        public bool RemoveObject(string objId)
        {
            DataBlock block = map.GetById<DataBlock>(objId);
            if (block.BlockType() == D2Item.Type())
                return RemoveItem(block as D2Item);
            map.RemoveDataBlockById(objId);
            return true;
        }

        public D2Unit AddUnit(string type, int level = 1)
        {
            Gunit unit = model.GetObjectT<Gunit>(type.ToLower());
            int resLevel = Math.Max(level, unit.level);
            D2Unit d2unit = new D2Unit
            {
                _ObjId = idsHelper.NextId<D2Unit>(ref map, "UN"),
                _Type = type.ToUpper(),
                _Level = resLevel,
                _HP = CalcLeveledValue(unit.hit_point, unit.level, resLevel, unit.dyn_upg_lv,
                    unit.dyn_upg1.value.hit_point, unit.dyn_upg1.value.hit_point)
            };
            map.AddDataBlock(d2unit);
            return d2unit;
        }

        public D2Stack AddStack(int x, int y, StackTemplate template, bool addToPlan = true)
        {
            D2Stack stack = new D2Stack()
            {
                _objId = idsHelper.NextId<D2Stack>(ref map, "KC"),
                _posX = x,
                _posY = y
            };
            for (int i = 0; i < 6; i++)
            {
                stack._Pos[i] = template._Pos[i];
                if (template._Unit[i] != "G000000000")
                {
                    D2Unit d2unit = AddUnit(template._Unit[i], template._UnitLvl[i]);

                    foreach (string modId in template._Mods[i])
                        d2unit._Mods.Add(modId);

                    if (template._LeaderIndex == i)
                    {
                        d2unit._Name = template.name;
                        stack._leaderId = d2unit.ObjId();
                    }
                    stack._Unit[i] = d2unit.ObjId();

                    map.AddDataBlock(d2unit);
                }
                else
                {
                    stack._Unit[i] = "000000";
                }
            }
            if (addToPlan)
            {
                grid[x, y].objIds.Add(stack._objId);
            }
            stack._items.Clear();
            foreach (string item in template._Items)
            {
                stack._items.Add(AddItem(item).ObjId());
            }
            map.AddDataBlock(stack);
            return stack;
        }

        public bool SetVariable(ref MapModel map, string name, int value)
        {
            D2SceneVariables variables = map.GetById<D2SceneVariables>("SV0000");
            if (variables == null)
            {
                variables = new D2SceneVariables();
                map.AddDataBlock(variables);
            }
            for (int i = 0; i < variables._Variables.Count; ++i)
            {
                if (variables._Variables[i]._Name == name)
                {
                    variables._Variables[i]._Value = value;
                    map.Replace(variables);
                    return true;
                }
            }

            return false;
        }

        public bool hasVariable(string name)
        {
            if (map.Header._MapType != "D2EESFISIG")
                return true;
            D2SceneVariables variables = map.GetById<D2SceneVariables>("SV0000");
            if (variables == null)
                return false;
            for (int i = 0; i < variables._Variables.Count; ++i)
            {
                if (variables._Variables[i]._Name == name)
                {
                    return true;
                }
            }

            return false;
        }

        public bool AddVariable(string name, int value)
        {
            D2SceneVariables variables = map.GetById<D2SceneVariables>("SV0000");
            if (variables == null)
            {
                variables = new D2SceneVariables();
                map.AddDataBlock(variables);
            }
            variables._Variables.Add(new D2SceneVariables.SceneVariable()
            {
                _Id = variables._Variables.Count,
                _Name = name,
                _Value = value
            });
            map.Replace(variables);
            return true;
        }

        public string GetVariable(ref MapModel map, string name)
        {
            D2SceneVariables variables = map.GetById<D2SceneVariables>("SV0000");
            for (int i = 0; i < variables._Variables.Count; ++i)
            {
                if (variables._Variables[i]._Name == name)
                {
                    return variables._Variables[i]._Value.ToString();
                }
            }

            return null;
        }
        public bool AddPlanObject(int x, int y, int w, int h, string name)
        {
            for (int i = 0; i < w; ++i)
            {
                for (int k = 0; k < h; k++)
                {
                    if (grid[i + x, k + y].objIds.Count > 0)
                    {
                        Logger.LogWarning("Midgard plan rewrite object in position." +
                            "pos = " + (i + x).ToString() + " " + (k + y).ToString() +
                            grid[i + x, k + y].objIds);
                    }

                    grid[i + x, k + y].objIds.Add(name);
                }
            }
            return true;
        }
        public bool RemovePlanObject(int x, int y, int w, int h, string name)
        {
            for (int i = 0; i < w; ++i)
            {
                for (int k = 0; k < h; k++)
                {
                    if (!grid[i + x, k + y].objIds.Contains(name))
                    {
                        Logger.LogError("Midgard plan failed to remove object." +
                            "pos = " + (i + x).ToString() + " " + (k + y).ToString() +
                            " -> " + name);
                    }
                    else
                        grid[i + x, k + y].objIds.Remove(name);
                }
            }
            return true;
        }

        public D2LandMark AddLandMark(int x, int y, string name)
        {
            GLmark lmark = model.GetObjectT<GLmark>(name);
            if (lmark.mountain)
                return null;
            D2LandMark landMark = new D2LandMark()
            {
                _objId = idsHelper.NextId<D2LandMark>(ref map, "MM"),
                _posX = x,
                _posY = y,
                _Type = name,
                _DescTxt = lmark.desc_txt.value.text
            };

            AddPlanObject(x, y, lmark.cx, lmark.cy, landMark._objId);

            map.AddDataBlock(landMark);
            return landMark;
        }

        public void AddMine(int x, int y, int resource)
        {
            D2Crystal crystal = new D2Crystal()
            {
                _ObjId = idsHelper.NextId<D2Crystal>(ref map, "CR"),
                _PosX = x,
                _PosY = y,
                _Resource = resource
            };
            map.AddDataBlock(crystal);
            AddPlanObject(x, y, 1, 1, crystal._ObjId);
        }

        public void AddTrainer(int x, int y, int type, string name = "", string desc = "")
        {
            D2Trainer site = new D2Trainer()
            {
                _objId = idsHelper.NextId<D2Site>(ref map, "SI"),
                _PosX = x,
                _PosY = y,
                _ImgIso = type,
                _TxtTitle = name,
                _TxtDesc = desc
            };
            map.AddDataBlock(site);
            AddPlanObject(x, y, 3, 3, site._objId);
        }
        public void AddMerchant(int x, int y, int type, ref List<D2Merchant.MerchantItemEntry> items, string name = "", string desc = "")
        {
            D2Merchant merchant = new D2Merchant()
            {
                _objId = idsHelper.NextId<D2Site>(ref map, "SI"),
                _PosX = x,
                _PosY = y,
                _Items = items,
                _ImgIso = type,
                _Title = name,
                _Desc = desc
            };
            map.AddDataBlock(merchant);
            AddPlanObject(x, y, 3, 3, merchant._objId);
        }

        public void AddMage(int x, int y, int type, ref List<string> items, string name = "", string desc = "")
        {
            D2Mage merchant = new D2Mage()
            {
                _objId = idsHelper.NextId<D2Site>(ref map, "SI"),
                _PosX = x,
                _PosY = y,
                _Spells = items,
                _ImgIso = type,
                _Title = name,
                _Desc = desc
            };
            map.AddDataBlock(merchant);
            AddPlanObject(x, y, 3, 3, merchant._objId);
        }

        public void AddMercs(int x, int y, int type, ref List<D2Mercs.MercsUnitEntry> units, string name = "", string desc = "")
        {
            D2Mercs mercs = new D2Mercs()
            {
                _objId = idsHelper.NextId<D2Site>(ref map, "SI"),
                _PosX = x,
                _PosY = y,
                _Units = units,
                _ImgIso = type,
                _Title = name,
                _Desc = desc
            };
            map.AddDataBlock(mercs);
            AddPlanObject(x, y, 3, 3, mercs._objId);
        }

        public void AddRuin(int x, int y, StackTemplate inside,
            string reward, int image, string name = "",
            string desc = "", string item = "")
        {
            if (name == "")
                name = "Ruin " + x.ToString() + "_" + y.ToString();
            D2Ruin ruin = new D2Ruin()
            {
                _objId = idsHelper.NextId<D2Ruin>(ref map, "RU"),
                _posX = x,
                _posY = y,
                _name = name,
                _cash = reward,
                _image = image,
                _desc = desc,
            };

            if (inside != null)
            {
                for (int i = 0; i < 6; i++)
                {
                    ruin._pos[i] = inside._Pos[i];
                    if (inside._Unit[i] != "G000000000")
                    {
                        D2Unit d2unit = AddUnit(inside._Unit[i].ToLower(), inside._UnitLvl[i]);

                        foreach (string modId in inside._Mods[i])
                            d2unit._Mods.Add(modId);

                        ruin._unit[i] = d2unit.ObjId();
                    }
                    else
                    {
                        ruin._unit[i] = "000000";
                    }
                }
                if (item == "")
                {
                    if (inside._Items != null && inside._Items.Count > 0)
                    {
                        if (inside._Items[0].Trim() != "")
                            ruin._item = inside._Items[0];
                    }
                }
                else
                    ruin._item = item;
            }

            map.AddDataBlock(ruin);
            AddPlanObject(x, y, 3, 3, ruin._objId);
        }

        public void AddVillage(int x, int y, int level, StackTemplate hero, StackTemplate inside, string name = "", string desc = "")
        {
            if (name == "")
                name = "Village " + x.ToString() + "_" + y.ToString();
            D2Village village = new D2Village()
            {
                _ObjId = idsHelper.NextId<D2City>(ref map, "FT"),
                _PosX = x,
                _PosY = y,
                _Size = level,
                _Name = name,
                _Desk = desc
            };

            AddPlanObject(x, y, 4, 4, village._ObjId);

            if (hero != null)
            {
                D2Stack stack = AddStack(x, y, hero);
                stack._inside = village._ObjId;
                map.ReplaceById(stack.ObjId(), stack);
                village._Stack = stack.ObjId();
            }
            if (inside != null)
            {
                for (int i = 0; i < 6; i++)
                {
                    village._Pos[i] = inside._Pos[i];
                    if (inside._Unit[i] != "G000000000")
                    {
                        D2Unit d2unit = AddUnit(inside._Unit[i].ToLower(),
                                                             inside._UnitLvl[i]);

                        foreach (string modId in inside._Mods[i])
                            d2unit._Mods.Add(modId);

                        village._Unit[i] = d2unit.ObjId();
                    }
                    else
                    {
                        village._Unit[i] = "000000";
                    }
                }
                foreach (string item in inside._Items)
                {
                    village._Items.Add(AddItem(item).ObjId());
                }
            }

            map.AddDataBlock(village);
        }

        public void AddMountain(int x, int y, int w, int h, int image)
        {
            D2Mountains.Mountain mountain = new D2Mountains.Mountain
            {
                x = x,
                y = y,
                sizeX = w,
                sizeY = h,
                image = image
            };
            AddMountain(mountain);
        }

        public void AddMountain(D2Mountains.Mountain mountain)
        {
            D2Mountains d2Mountains = map.GetById<D2Mountains>("ML0000");
            mountain.id = d2Mountains._mountains.Count;
            d2Mountains._mountains.Add(mountain);
            for (int i = 0; i < mountain.sizeX; i++)
            {
                for (int k = 0; k < mountain.sizeY; k++)
                    grid[i + mountain.x, k + mountain.y].value = (int)D2MapBlock.TileType.Mountain;
            }
            map.Replace(d2Mountains);
        }
    }
}
