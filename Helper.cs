﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevendaarTools
{
    public static class Helper
    {
        public static string ReplaceInvalidFilenameChars(string filename)
        {
            var invalidChars = Path.GetInvalidFileNameChars();
            var sb = new StringBuilder();
            foreach (var c in filename)
            {
                if (invalidChars.Contains(c))
                {
                    sb.Append("_");
                }
                else
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();

        }
        public static bool IsIn(this string source, params string[] values)
        {
            return values.Contains(source, StringComparer.Ordinal);
        }

        public static bool OrdinalEquals(this string source, string target)
        {
            return string.Equals(source, target, StringComparison.OrdinalIgnoreCase);
        }

        public static bool OrdinalContains(this string source, string target)
        {
            return string.IsNullOrEmpty(source) ? false : source.IndexOf(target, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public static int ByteSearch(byte[] searchIn, byte[] searchBytes, int start = 0)
        {
            return ByteSearch(ref searchIn, searchBytes, start);
        }

        public static int ByteSearch(ref byte[] searchIn, byte[] searchBytes, int start = 0)
        {
            int found = -1;
            bool matched = false;
            //only look at this if we have a populated search array and search bytes with a sensible start
            if (searchIn.Length > 0 && searchBytes.Length > 0 && start <= (searchIn.Length - searchBytes.Length) && searchIn.Length >= searchBytes.Length)
            {
                //iterate through the array to be searched
                for (int i = start; i <= searchIn.Length - searchBytes.Length; i++)
                {
                    //if the start bytes match we will start comparing all other bytes
                    if (searchIn[i] == searchBytes[0])
                    {
                        if (searchIn.Length > 1)
                        {
                            //multiple bytes to be searched we have to compare byte by byte
                            matched = true;
                            for (int y = 1; y <= searchBytes.Length - 1; y++)
                            {
                                if (searchIn[i + y] != searchBytes[y])
                                {
                                    matched = false;
                                    break;
                                }
                            }
                            //everything matched up
                            if (matched)
                            {
                                found = i;
                                break;
                            }

                        }
                        else
                        {
                            //search byte is only one bit nothing else to do
                            found = i;
                            break; //stop the loop
                        }

                    }
                }

            }
            return found;
        }

        public static byte[] stringToByteArray(string data)
        {
            var enc1251 = Encoding.GetEncoding(1251);

            return  enc1251.GetBytes(data);

            //byte[] result = new byte[data.Length];
            //for (int i = 0; i < data.Length; i++)
            //{
            //    result[i] = Convert.ToByte(data[i]);
            //}
            //return result;// System.Text.Encoding.Unicode.GetBytes(data);
        }

        public static byte[] stringToByteArray(string data, int resultSize)
        {
            var enc1251 = Encoding.GetEncoding(1251);
             
            byte[] result = enc1251.GetBytes(data); ;// = new byte[data.Length];
            //for (int i = 0; i < data.Length; i++)
            //{
            //    try
            //    {
            //        result[i] = Convert.ToByte(data[i]);
            //    }
            //    catch (Exception ex)
            //    Console.WriteLine("Exception caught in process: {0}", ex);
            //    }
            //}
            while (result.Length < resultSize)
            {
                result = Merge(result, new byte[] { 0 });
            }
            return result;
        }

        public static byte[] Merge(byte[] part1, byte[] part2)
        {
            byte[] result = new byte[part1.Length + part2.Length];
            Array.Copy(part1, 0, result, 0, part1.Length);
            Array.Copy(part2, 0, result, part1.Length, part2.Length);
            return result;
        }

        public static byte[] Merge(byte[] part1, byte[] part2, byte[] part3)
        {
            return Merge(part1, Merge(part2, part3));
        }
        public static byte[] Merge(byte[] part1, byte[] part2, byte[] part3, byte[] part4)
        {
            return Merge(Merge(part1, part2), Merge(part3, part4));
        }

        public static byte[] IntToByteArray(int value, int size)
        {
            byte[] data = BitConverter.GetBytes(value);
            byte[] result = new byte[size];
            Array.Copy(data, 0, result, 0, size);
            while (result.Length < size)
            {
                result = Merge(new byte[] { 0 }, result);
            }
            return result;
        }

        public static byte[] ReadRange(ref byte[] array, ref int index, int size)
        {
            byte[] result = new byte[size];
            Array.Copy(array, index, result, 0, size);
            index += size;
            return result;
        }

        public static string ReadString(byte[] array, int startIndex, int size, System.Text.Encoding encoding)
        {
            byte[] result = new byte[size];
            Array.Copy(array, startIndex, result, 0, size);
            return encoding.GetString(result);
        }

        public static string ReadStringWithTerm(ref byte[] array, ref int startIndex)///deprecated
        {
            int endIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray("\0"), startIndex);
            return ReadString(array, startIndex, endIndex - startIndex);
        }

        public static string ReadStringWithTerm2(ref byte[] array, ref int startIndex)
        {
            int endIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray("\0"), startIndex);
            string res = ReadString(array, startIndex, endIndex - startIndex);
            startIndex = endIndex + 1;
            return res;
        }

        public static string ReadString(byte[] array, int startIndex, int size)
        {
            byte[] result = new byte[size];
            Array.Copy(array, startIndex, result, 0, size);
            return System.Text.Encoding.GetEncoding(1251).GetString(result);
        }

        public static string ReadString(ref byte[] array, ref int startIndex, int size)
        {
            byte[] result = new byte[size];
            Array.Copy(array, startIndex, result, 0, size);
            startIndex += size + 1;//\0
            return System.Text.Encoding.GetEncoding(1251).GetString(result);
        }

        public static string ReadDefaultString(ref byte[] array, ref int startIndex, string tag)
        {
            string result;
            startIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray(tag), startIndex) + tag.Length + 8;//\v\0\0\0mapv
            result = Helper.ReadString(ref array, ref startIndex, 6);
            return result;
        }

        public static string ReadStringWithLenght(ref byte[] array, ref int startIndex, string tag)
        {
            string result;
            startIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray(tag), startIndex) + tag.Length;
            int lenght = Helper.ReadInt(ref array, ref startIndex, 4) - 1;
            result = Helper.ReadString(ref array, ref startIndex, lenght);

            return result;
        }

        public static int ReadInt(ref byte[] array, ref int startIndex, int size, bool bigEndian = false)
        {
            if (size == 1)
            {
                return (int)array[startIndex++];
            }
            byte[] result = new byte[4];
            Array.Copy(array, startIndex, result, 0, size);
            startIndex += size;
            if (bigEndian)
                Array.Reverse(result);
            if (size < 3)
                return BitConverter.ToInt16(result, 0);
            return BitConverter.ToInt32(result, 0);
        }

        public static byte[] SubArray(ref byte[] array, int startIndex, int endIndex)
        {
            byte[] result = new byte[endIndex - startIndex];
            Array.Copy(array, startIndex, result, 0, result.Length);
            return result;
        }

        public static int EndIndex(ref byte[] array, int startIndex)
        {
            return Helper.ByteSearch(ref array, Helper.stringToByteArray("ENDOBJECT\0"), startIndex) + 10;
        }

        public static bool ReadBool(ref byte[] array, ref int startIndex)
        {
            int val = ReadInt(ref array, ref startIndex, 1);
            return val > 0;
        }

        public static bool ReadBool(ref byte[] array, ref int startIndex, string tag)
        {
            startIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray(tag), startIndex) + tag.Length;
            int val = ReadInt(ref array, ref startIndex, 1);
            return val > 0;
        }

        public static int ReadInt(byte[] array, int startIndex, int size)
        {
            if (size == 1)
                return (int)array[startIndex];
            byte[] result = new byte[size];
            Array.Copy(array, startIndex, result, 0, size);
            if (size < 3)
                return BitConverter.ToInt16(result, 0);
            return BitConverter.ToInt32(result, 0);
        }

        public static bool HasTag(ref byte[] array, ref int startIndex, string tag)
        {
            int index = Helper.ByteSearch(ref array, Helper.stringToByteArray(tag), startIndex);
            return index != -1;
        }

        public static int ReadInt(ref byte[] array, ref int startIndex, string tag, int size)
        {
            startIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray(tag), startIndex) + tag.Length;
            return ReadInt(ref array, ref startIndex, size);
        }

        public static BitArray ReadBitArray(ref byte[] array, ref int startIndex, int size)
        {
            BitArray result = new BitArray(size * 8);
            byte[] bytes = new byte[size];
            Array.Copy(array, startIndex, bytes, 0, size);
            
            startIndex += size;
            return new BitArray(bytes);
        }

        public static int ReadDefaultInt(ref byte[] array, ref int startIndex, string tag)
        {
            if (startIndex < 0)
                return 0;
            startIndex = Helper.ByteSearch(ref array, Helper.stringToByteArray(tag), startIndex) + tag.Length;
            byte[] bytes = new byte[4];
            Array.Copy(array, startIndex, bytes, 0, 4);
            startIndex += 4;
            return BitConverter.ToInt32(bytes, 0);
        }

        public static string NextId(ref List<int> numbers, string prefix)
        {
            string result = prefix  + numbers[0].ToString("X").PadLeft(4, '0').ToLower();
            numbers.RemoveAt(0);
            return result;
        }
    }

    public class ByteBuffer
    {
        private List<Byte[]> _DataList = new List<byte[]>();
        private int _DataSize = 0;
        public void Write(byte[] data)
        {
            _DataList.Add(data);
            _DataSize += data.Length;
        }

        public void WriteString(string name, string value)
        {
            Write(Helper.Merge(Helper.stringToByteArray(name), Helper.IntToByteArray(value.Length + 1, 4), Helper.stringToByteArray(value + "\0", value.Length + 1)));
        }

        public void WriteBegin()
        {
            Write(Helper.stringToByteArray("BEGOBJECT\0"));
        }

        public void WriteEnd()
        {
            Write(Helper.stringToByteArray("ENDOBJECT\0"));
        }

        public void WriteDefaultString(string name, string version, string value)
        {
            if (value == "000000" || value == "G000000000")
                Write(Helper.stringToByteArray(name + "\v\0\0\0G000000000\0"));
            else
                Write(Helper.stringToByteArray(name + "\v\0\0\0" + version + value + "\0"));
        }

        public void WriteDefaultString(string name, string value)
        {
            if (value == "000000")
                Write(Helper.stringToByteArray(name + "\v\0\0\0G000000000\0"));
            else
                Write(Helper.stringToByteArray(name + "\v\0\0\0" + value + "\0"));
        }

        public void WriteBitArray(ref BitArray array)
        {
            var bytes = new byte[(array.Length - 1) / 8 + 1];
            array.CopyTo(bytes, 0);
            Write(bytes);
        }

        public void WriteRepitableString(string value, int count)
        {
            string result = "";
            for (int i = 0; i < count; ++i)
                result += value;
            WriteSimpleString(result);
        }

        public void WriteSimpleString(string value)
        {
            Write(Helper.stringToByteArray(value));
        }

        public void WriteBool(string name, bool value)
        {
            Write(Helper.Merge(Helper.stringToByteArray(name), Helper.IntToByteArray(value?1:0, 1)));
        }

        public void WriteDefaultInt(string name, int value)
        {
            Write(Helper.Merge(Helper.stringToByteArray(name), Helper.IntToByteArray(value, 4)));
        }

        public void WriteInt(string name, int value, int size)
        {
            Write(Helper.Merge(Helper.stringToByteArray(name), Helper.IntToByteArray(value, size)));
        }
        public void WriteInt(int value, int size, bool bigEndian = false)
        {
            byte [] data = Helper.IntToByteArray(value, size);
            if (bigEndian)
                Array.Reverse(data);
            Write(data);
        }

        public byte[] GetData()
        {
            byte[] result = new byte[_DataSize];
            int startIndex = 0;
            foreach(byte [] data in _DataList)
            {
                Array.Copy(data, 0, result, startIndex, data.Length);
                startIndex += data.Length;
            }
            return result;
        }
    }
}
