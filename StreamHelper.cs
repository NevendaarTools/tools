﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NevendaarTools
{
    public class StreamHelper
    {
        public static Int32 ReadInt(FileStream stream)
        {
            byte[] b = new byte[4];
            stream.Read(b, 0, 4);
            return BitConverter.ToInt32(b, 0);
        }

        public static string ReadString(ref byte[] array, ref int startIndex, int size)
        {
            byte[] result = new byte[size];
            Array.Copy(array, startIndex, result, 0, size);
            startIndex += size + 1;//\0
            return System.Text.Encoding.GetEncoding(1251).GetString(result);
        }

        public static int ReadInt(ref byte[] array, ref int startIndex, int size, bool bigEndian = false)
        {
            if (size == 1)
            {
                return (int)array[startIndex++];
            }
            byte[] result = new byte[4];
            Array.Copy(array, startIndex, result, 0, size);
            startIndex += size;
            if (bigEndian)
                Array.Reverse(result);
            if (size < 3)
                return BitConverter.ToInt16(result, 0);
            return BitConverter.ToInt32(result, 0);
        }

        public static byte[] StringToByteArray(string data)
        {
            var enc1251 = Encoding.GetEncoding(1251);

            return enc1251.GetBytes(data);
        }
        public static string ReadStringWithTerm2(ref byte[] array, ref int startIndex)
        {
            int endIndex = startIndex;
            for (int i = startIndex; i < array.Length; ++i)
            {
                if (array[i] == 0)
                {
                    endIndex = i;
                    break;
                }
            }
            string res = ReadString(ref array, ref startIndex, endIndex - startIndex);
            startIndex = endIndex + 1;
            return res;
        }
    }
}
