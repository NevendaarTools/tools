﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using NevendaarTools.DataTypes;

namespace NevendaarTools
{
    //G0100:R0000:Y0000:E0000:W0000:B0000
    //    G - золото
    //R - мана преисподней
    //Y - мана жизни
    //E - мана смерти
    //W - мана рун
    //B - мана рощи
    public class CostHelper
    {
        public static bool IsZero(string cost)
        {
            if (cost == "")
                return true;
            int G = CostHelper.G(cost);//gold
            int R = CostHelper.R(cost);
            int Y = CostHelper.Y(cost);
            int E = CostHelper.E(cost);
            int W = CostHelper.W(cost);
            int B = CostHelper.B(cost);

            return (G + R + Y + E + W + B) == 0;
        }
        public static int G(string value)
        {
            if (value.Length < 4)
                return 0;
            return int.Parse(value.Substring(1, 4));
        }
        public static int R(string value)
        {
            if (value.Length < 10)
                return 0;
            return int.Parse(value.Substring(7, 4));
        }
        public static int Y(string value)
        {
            if (value.Length < 16)
                return 0;
            return int.Parse(value.Substring(13, 4));
        }
        public static int E(string value)
        {
            if (value.Length < 22)
                return 0;
            return int.Parse(value.Substring(19, 4));
        }
        public static int W(string value)
        {
            if (value.Length < 28)
                return 0;
            return int.Parse(value.Substring(25, 4));
        }
        public static int B(string value)
        {
            if (value.Length < 34)
                return 0;
            return int.Parse(value.Substring(31, 4));
        }

        public static string Form(int g, int r, int y, int e, int w, int b)
        {
            if (g < 0) g = 0;
            if (r < 0) r = 0;
            if (y < 0) y = 0;
            if (e < 0) e = 0;
            if (w < 0) w = 0;
            if (b < 0) b = 0;
            return string.Format("G{0:0000}:R{1:0000}:Y{2:0000}:E{3:0000}:W{4:0000}:B{5:0000}", g, r, y, e, w, b);
        }

        public static string Inc(string val1, string val2)
        {
            return Form(G(val1) + G(val2),
                        R(val1) + R(val2),
                        Y(val1) + Y(val2),
                        E(val1) + E(val2),
                        W(val1) + W(val2),
                        B(val1) + B(val2));
        }

        public static string Dec(string val1, string val2)
        {
            return Form(G(val1) - G(val2),
                        R(val1) - R(val2),
                        Y(val1) - Y(val2),
                        E(val1) - E(val2),
                        W(val1) - W(val2),
                        B(val1) - B(val2));
        }
    }
    public class Option
    {
        public enum Type
        {
            Bool = 0,
            Float,
            Int,
            String,
            Enum,
            SemgaList,
            PathSelect
        }
        public enum Status
        {
            Normal = 0,
            Hiden,
            Disabled,
        }

        public string name;
        public string desc;
        public string value;
        public string min = "0";
        public string defValue = "0";
        public string max = "10000";
        public string group = "";
        public List<string> variants;
        public List<string> variantValues;
        public Type type;
        public Status status;

        public static Option EnumOption(string name, string value, string desc, List<string> variants)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.Enum,
                value = value,
                variants = variants
            };
        }

        public static Option EnumOption(string name, string value, string desc, List<string> variants, List<string> variantValues)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.Enum,
                value = value,
                variants = variants,
                variantValues = variantValues
            };
        }

        public static Option SemgaOption(string name, string defValue, string desc, List<string> variants, List<string> variantValues)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.SemgaList,
                defValue = defValue,
                value = defValue,
                variants = variants,
                variantValues = variantValues
            };
        }

        public static Option SelectPathOption(string name, string defValue, string desc, string filter, string dir)
        {
            Option result = new Option
            {
                name = name,
                desc = desc,
                type = Type.PathSelect,
                defValue = defValue,
                value = defValue,
                variantValues = new List<string>()
            };
            result.variantValues.Add(filter);
            result.variantValues.Add(dir);

            return result;
        }

        public static Option StringOption(string name, string value, string desc)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.String,
                value = value
            };
        }

        public static Option BoolOption(string name, bool value, string desc)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.Bool,
                value = value ? "True" : "False"
            };
        }
        public static Option BoolOption(string name, bool value)
        {
            return BoolOption(name, value, name + "_desc");
        }

        public static Option FloatOption(string name, float value, string desc, float min = 0.01f, float max = 10000)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.Float,
                min = min.ToString(),
                max = max.ToString(),
                value = value.ToString()
            };
        }

        public static Option FloatOption(string name, float value, float min = 0.01f, float max = 10000)
        {
            return FloatOption(name, value, name + "_desc", min, max);
        }

        public static Option IntOption(string name, int value, string desc, int min = 0, int max = 10000)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.Int,
                min = min.ToString(),
                max = max.ToString(),
                value = value.ToString()
            };
        }

        public static Option IntOption(string name, int value, int min = 0, int max = 10000)
        {
            return IntOption(name, value, name + "_desc", min, max);
        }

        public static Option FloatOption(string name, string value, string desc, string min, string max)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.Float,
                min = min,
                max = max,
                value = value
            };
        }

        public static Option IntOption(string name, string value, string desc, string min, string max)
        {
            return new Option
            {
                name = name,
                desc = desc,
                type = Type.Int,
                min = min,
                max = max,
                value = value
            };
        }

        public void FromString(string str)
        {
            string[] data = str.Split(new[] { " " }, StringSplitOptions.None);
            value = data[1];
        }

        public static bool SetValueByName(ref List<Option> options, string name, string value)
        {
            for (int i = 0; i < options.Count; i++)
            {
                if (options[i].name == name)
                {
                    options[i].value = value;
                    return true;
                }
            }
            return false;
        }

        public static Option GetOption(List<Option> options, string name)
        {
            foreach (Option option in options)
            {
                if (option.name == name)
                    return option;
            }
            return null;
        }

        public static string GetOptionValue(List<Option> options, string name)
        {
            foreach (Option option in options)
            {
                if (option.name == name)
                    return option.value;
            }
            return null;
        }

        public static bool GetBoolOption(Option option)
        {
            return option.value.Trim() == "True";
        }

        public static bool GetBoolOption(List<Option> options, string name)
        {
            return GetBoolOption(GetOption(options, name));
        }

        public static float GetFloatOption(List<Option> options, string name)
        {
            string value = GetOptionValue(options, name);
            value = value.Replace(",", ".");
            value = value.Replace(".", CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            return float.Parse(value);
        }

        public static int GetIntOption(List<Option> options, string name)
        {
            string value = GetOptionValue(options, name);
            return int.Parse(value);
        }
    }

    public class DescriptionHelper
    {
        static string UnitTypeById( string id, List<D2Unit> units)
        {
            foreach (D2Unit unit in units)
                if (unit.ObjId() == id)
                    return unit._Type.ToLower();
            return "";
        }

        static string ItemTypeById(string id, List<D2Item> units)
        {
            foreach (D2Item unit in units)
                if (unit.ObjId() == id)
                    return unit._type.ToLower();
            return "";
        }

        public static string StackDescription2(D2Stack stack, List<D2Unit> units, GameModel model)
        {
            string result = "";

            string unitType = UnitTypeById(stack._leaderId, units);
            result += model.GetObjectT<Gunit>(unitType).name_txt.value.text + " - " +
                stack._posX.ToString() + " : " + stack._posY.ToString() + "\n";

            for(int index = 0; index < 6; ++index)
            {
                result += index.ToString() + ": ";
                if (stack._Pos[index] != -1)
                {
                    string unitid = stack._Unit[stack._Pos[index]];
                    unitType = UnitTypeById(unitid, units);
                    result += model.GetObjectT<Gunit>(unitType).name_txt.value.text + "\n";
                }
                else
                    result += "-\n";
            }

            return result;
        }

        public static string StackDescription2(D2Stack stack, List<D2Unit> units, List<D2Item> items, GameModel model)
        {
            string result = "";

            string unitType = UnitTypeById(stack._leaderId, units);
            result += model.GetObjectT<Gunit>(unitType).name_txt.value.text + " - " +
                stack._posX.ToString() + " : " + stack._posY.ToString() + "\n";

            for (int index = 0; index < 6; ++index)
            {
                result += index.ToString() + ": ";
                if (stack._Pos[index] != -1)
                {
                    string unitid = stack._Unit[stack._Pos[index]];
                    unitType = UnitTypeById(unitid, units);
                    result += model.GetObjectT<Gunit>(unitType).name_txt.value.text + "\n";
                }
                else
                    result += "-\n";
            }
            foreach (string item in stack._items)
            {
                string itemtype = ItemTypeById(item, items);
                result += model.GetObjectT<Gunit>(unitType).name_txt.value.text + " ";
            }
            result += "\n";

            return result;
        }

        public static string StackDescription(D2Stack stack, List<D2Unit> units, GameModel model)
        {
            string result = "\n";
            
            string unitType = UnitTypeById(stack._leaderId, units);
            result += model.GetObjectT<Gunit>(unitType).name_txt.value.text + " - " +
                stack._posX.ToString() + " : " + stack._posY.ToString();

            result += StackRowDesc(stack, 0, units, model) + "\n";
            result += StackRowDesc(stack, 1, units, model) + "\n";
            result += StackRowDesc(stack, 2, units, model) + "\n";

            result += "\n";
            return result;
        }


        public static string StackRowDesc(D2Stack stack, int row, List<D2Unit> units, GameModel model)
        {
            string row1 = "";
            string unitType;
            if (stack._Pos[2 * row + 1] != -1)
            {
                if (stack._Pos[2 * row + 0] != -1 && stack._Pos[2 * row + 0] != stack._Pos[2 * row + 1])
                {
                    unitType = UnitTypeById(stack._Unit[stack._Pos[2 * row + 1]], units);
                    row1 = string.Format("{0, -20}", model.GetObjectT<Gunit>(unitType).name_txt.value.text);

                    unitType = UnitTypeById(stack._Unit[stack._Pos[2 * row + 0]], units);
                    row1 += string.Format("{0, -20}", model.GetObjectT<Gunit>(unitType).name_txt.value.text);
                }
                else
                {
                    unitType = UnitTypeById(stack._Unit[stack._Pos[2 * row + 1]], units);
                    Gunit unit = model.GetObjectT<Gunit>(unitType);
                    if (unit.size_small)
                        row1 = row1 = string.Format("{0, -20}", " ") + string.Format("{0, -20}", unit.name_txt.value.text);
                    else
                        row1 = string.Format("{0, 40}", unit.name_txt.value.text);
                }
            }
            else
            {
                if (stack._Pos[2 * row + 0] != -1)
                {
                    unitType = UnitTypeById(stack._Unit[stack._Pos[2 * row + 0]], units);
                    Gunit unit = model.GetObjectT<Gunit>(unitType);
                    row1 = string.Format("{0, -20}", " ") + string.Format("{0, -20}", unit.name_txt.value.text);
                }
            }
            return row1;
        }
    }
}
