﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace NevendaarTools
{
    //https://en.wikipedia.org/wiki/.dbf

    public class SimpleDBFAdapter
    {
        public struct HeaderData
        {
            public string name;
            public string typeStr;
            public Type type;
            public int fieldLen;
        };

        public int RowCount() { return _RowsData.Count; }
        public int ColumnCount() { return (_RowsData.Count > 0)? _RowsData[0].Count: 0; }
        public HeaderData GetHeader(int index) { return _Header[index]; }
        public List<string> GetRow(int index) { return _RowsData[index]; }

        public string GetString(string fieldName, int row)
        {
            int index = -1;
            for(int i = 0; i < _Header.Count; i++)
            {
                if (_Header[i].name.OrdinalEquals(fieldName))
                {
                    index = i;
                    break;
                }
            }
            if (index == -1)
                return null;

            return _RowsData[row][index];
        }
        public string GetString(int column, int row)
        {
            return _RowsData[row][column];
        }

        public bool Set(string fieldName, int row, string value)
        {
            int index = -1;
            for (int i = 0; i < _Header.Count; i++)
            {
                if (_Header[i].name.OrdinalEquals(fieldName))
                {
                    index = i;
                    break;
                }
            }
            if (index == -1)
                return false;
            _RowsData[row][index] = value;
            return true;
        }

        public int GetInt(string fieldName, int row, int valueIfEmpty = 0)
        {
            string value = GetString(fieldName, row);
            if (value == null || value.Trim() == "")
                return valueIfEmpty;
            return int.Parse(value);
        }

        public bool GetBool(string fieldName, int row, bool valueIfEmpty = false)
        {
            string value = GetString(fieldName, row);
            if (value == null || value.Trim() == "")
                return valueIfEmpty;
            return value == "T";
        }

        public bool ReadDBFFile(string filename)
        {
            _Header.Clear();
            _RowsData.Clear();
            _RowSize = 0;

            byte[] array;
            int maxRetries = 3;
            int currentRetry = 0;

            while (currentRetry < maxRetries)
            {
                try
                {
                    using (FileStream fstream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        if (fstream.Length < 1)
                            return false;

                        array = new byte[fstream.Length];
                        fstream.Read(array, 0, array.Length);

                        int bitesInHeader = Helper.ReadInt(array, 8, 2) - 32;
                        byte[] header = new byte[bitesInHeader];
                        Array.Copy(array, 32, header, 0, bitesInHeader);
                        if (!ReadHeader(header))
                            return false;

                        bitesInHeader += 32;// bytes before table
                        byte[] rowsData = new byte[array.Length - bitesInHeader];
                        Array.Copy(array, bitesInHeader, rowsData, 0, array.Length - bitesInHeader);
                        if (!ReadRowsData(rowsData))
                            return false;

                        return true;
                    }
                }
                catch (IOException ex)
                {
                    currentRetry++;
                    if (currentRetry >= maxRetries)
                    {
                        throw new IOException("Не удалось получить доступ к файлу " + Path.GetFileName(filename) + " после " + maxRetries + " попыток. Возможно, файл заблокирован другим процессом.", ex);
                    }
                    System.Threading.Thread.Sleep(100 * currentRetry); // Увеличивающаяся задержка между попытками
                }
            }

            return false;
        }

        private bool ReadHeader(byte [] header)
        {
            int startIndex = 0;
            while (startIndex < header.Length - 31)
            {
                HeaderData data = new HeaderData();
                data.name = Helper.ReadString(header, startIndex, 11).Replace("\0", "");
                data.typeStr = Helper.ReadString(header, startIndex + 11, 1);
                data.type = TypeFromString(data.typeStr);
                data.fieldLen = Helper.ReadInt(header, startIndex + 16, 1);

                _Header.Add(data);
                startIndex += 32;
            }
            foreach (HeaderData hData in _Header)
                _RowSize += hData.fieldLen;

            return true;
        }

        bool isEmptyRow(List<string> row)
        {
            foreach(string data in row)
            {
                if (data != null && data.Trim() != "")
                    return false;
            }
            return true;
        }

        private bool ReadRowsData(byte[] rowsData)
        {
            System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("cp866");
            int startIndex = 0;
            while (startIndex < rowsData.Length - _RowSize + 1)
            {
                List<string> row = new List<string>();
                string startSymb = Helper.ReadString(rowsData, startIndex, 1);
                if (startSymb == "*")
                {
                    startIndex += _RowSize + 1;
                    continue;
                }
                else startIndex += 1;

                foreach (HeaderData hData in _Header)
                {
                    row.Add(Helper.ReadString(rowsData, startIndex, hData.fieldLen, encoding).Trim());
                    startIndex += hData.fieldLen;
                }
                if (!isEmptyRow(row))
                    _RowsData.Add(row);
            }

            return true;
        }

        private Type TypeFromString(string typeStr)
        {
            return Type.GetType(typeStr);
            //if (typeStr = "L")
            //    return Boolean;
        }

        private int _RowSize = 0;
        private List<HeaderData> _Header = new List<HeaderData>();
        private List<List<string>> _RowsData = new List<List<string>>();
    }
}
