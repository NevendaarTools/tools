﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NevendaarTools
{
    public class GameModel : BaseDataModel
    {
        public LuaSettingsAdapter _luaSettings = new LuaSettingsAdapter();
        public GameResourceModel _ResourceModel = new GameResourceModel();
        public bool Load(string path, bool withRes = false)
        {
            tableDescs.Clear();
            Register(typeof(Gaction).Name, new BaseTable(path + "\\Globals\\Gaction.dbf"));
            Register(typeof(GAI).Name, new BaseTable(path + "\\Globals\\GAI.dbf"));
            Register(typeof(Gattack).Name, new BaseTable(path + "\\Globals\\Gattacks.dbf"));
            Register(typeof(Gbuild).Name, new BaseTable(path + "\\Globals\\Gbuild.dbf"));
            Register(typeof(GbuiList).Name, new BaseTable(path + "\\Globals\\GbuiList.dbf"));
            Register(typeof(GcityInf).Name, new BaseTable(path + "\\Globals\\GcityInf.dbf"));
            Register(typeof(GDynUpgr).Name, new BaseTable(path + "\\Globals\\GDynUpgr.dbf"));
            Register(typeof(Gimmu).Name, new BaseTable(path + "\\Globals\\Gimmu.dbf"));
            Register(typeof(GimmuC).Name, new BaseTable(path + "\\Globals\\GimmuC.dbf"));
            Register(typeof(GItem).Name, new BaseTable(path + "\\Globals\\GItem.dbf", 1));
            Register(typeof(GLabi).Name, new BaseTable(path + "\\Globals\\GLabi.dbf"));
            Register(typeof(GleaUpg).Name, new BaseTable(path + "\\Globals\\GleaUpg.dbf"));
            Register(typeof(GLmark).Name, new BaseTable(path + "\\Globals\\GLmark.dbf"));
            Register(typeof(Glord).Name, new BaseTable(path + "\\Globals\\Glord.dbf"));
            Register(typeof(GMabi).Name, new BaseTable(path + "\\Globals\\GMabi.dbf"));
            Register(typeof(Gmodif).Name, new BaseTable(path + "\\Globals\\Gmodif.dbf"));
            Register(typeof(GUmodif).Name, new BaseTable(path + "\\Globals\\GUmodif.dbf"));
            Register(typeof(GmodifL).Name, new BaseTable(path + "\\Globals\\GmodifL.dbf"));
            Register(typeof(Grace).Name, new BaseTable(path + "\\Globals\\Grace.dbf"));
            Register(typeof(GspellR).Name, new BaseTable(path + "\\Globals\\GspellR.dbf", 1));
            Register(typeof(Gspell).Name, new BaseTable(path + "\\Globals\\Gspells.dbf"));
            Register(typeof(GSubRace).Name, new BaseTable(path + "\\Globals\\GSubRace.dbf"));
            Register(typeof(GTileDBI).Name, new BaseTable(path + "\\Globals\\GTileDBI.dbf"));
            Register(typeof(Gtransf).Name, new BaseTable(path + "\\Globals\\Gtransf.dbf"));
            Register(typeof(Gunit).Name, new BaseTable(path + "\\Globals\\Gunits.dbf"));
            Register(typeof(GVars).Name, new BaseTable(path + "\\Globals\\GVars.dbf"));
            Register(typeof(Laction).Name, new BaseTable(path + "\\Globals\\Laction.dbf"));
            Register(typeof(LaiAtt).Name, new BaseTable(path + "\\Globals\\LaiAtt.dbf"));
            Register(typeof(LAiMsg).Name, new BaseTable(path + "\\Globals\\LAiMsg.dbf"));
            Register(typeof(LAiSpell).Name, new BaseTable(path + "\\Globals\\LAiSpell.dbf"));
            Register(typeof(LattC).Name, new BaseTable(path + "\\Globals\\LattC.dbf"));
            Register(typeof(LAttR).Name, new BaseTable(path + "\\Globals\\LAttR.dbf"));
            Register(typeof(LattS).Name, new BaseTable(path + "\\Globals\\LattS.dbf"));
            Register(typeof(Lbuild).Name, new BaseTable(path + "\\Globals\\Lbuild.dbf"));
            Register(typeof(Ldiff).Name, new BaseTable(path + "\\Globals\\Ldiff.dbf"));
            Register(typeof(LDthAnim).Name, new BaseTable(path + "\\Globals\\LDthAnim.dbf"));
            Register(typeof(LEvCond).Name, new BaseTable(path + "\\Globals\\LEvCond.dbf"));
            Register(typeof(LEvEffct).Name, new BaseTable(path + "\\Globals\\LEvEffct.dbf"));
            Register(typeof(LFort).Name, new BaseTable(path + "\\Globals\\LFort.dbf"));
            Register(typeof(Lground).Name, new BaseTable(path + "\\Globals\\Lground.dbf"));
            Register(typeof(LImmune).Name, new BaseTable(path + "\\Globals\\LImmune.dbf"));
            Register(typeof(LleadA).Name, new BaseTable(path + "\\Globals\\LleadA.dbf"));
            Register(typeof(LleadC).Name, new BaseTable(path + "\\Globals\\LleadC.dbf"));
            Register(typeof(LLMCat).Name, new BaseTable(path + "\\Globals\\LLMCat.dbf"));
            Register(typeof(Llord).Name, new BaseTable(path + "\\Globals\\Llord.dbf"));
            Register(typeof(LmagItm).Name, new BaseTable(path + "\\Globals\\LmagItm.dbf"));
            Register(typeof(LmodifE).Name, new BaseTable(path + "\\Globals\\LmodifE.dbf"));
            Register(typeof(LModifS).Name, new BaseTable(path + "\\Globals\\LModifS.dbf"));
            Register(typeof(LOrder).Name, new BaseTable(path + "\\Globals\\LOrder.dbf"));
            Register(typeof(Lrace).Name, new BaseTable(path + "\\Globals\\Lrace.dbf"));
            Register(typeof(Lres).Name, new BaseTable(path + "\\Globals\\Lres.dbf"));
            Register(typeof(Lsite).Name, new BaseTable(path + "\\Globals\\Lsite.dbf"));
            Register(typeof(Lspell).Name, new BaseTable(path + "\\Globals\\Lspell.dbf"));
            Register(typeof(LSplPhas).Name, new BaseTable(path + "\\Globals\\LSplPhas.dbf"));
            Register(typeof(LSubRace).Name, new BaseTable(path + "\\Globals\\LSubRace.dbf"));
            Register(typeof(Lterrain).Name, new BaseTable(path + "\\Globals\\Lterrain.dbf"));
            Register(typeof(Ltreaty).Name, new BaseTable(path + "\\Globals\\Ltreaty.dbf"));
            Register(typeof(LunitB).Name, new BaseTable(path + "\\Globals\\LunitB.dbf"));
            Register(typeof(LunitC).Name, new BaseTable(path + "\\Globals\\LunitC.dbf"));
            Register(typeof(TAiMsg).Name, new BaseTable(path + "\\Globals\\TAiMsg.dbf"));
            Register(typeof(Tglobal).Name, new BaseTable(path + "\\Globals\\Tglobal.dbf"));
            Register(typeof(Tleader).Name, new BaseTable(path + "\\Globals\\Tleader.dbf"));
            Register(typeof(Tplayer).Name, new BaseTable(path + "\\Globals\\Tplayer.dbf"));

            Register(typeof(TApp).Name, new BaseTable(path + "\\Interf\\TApp.dbf"));

            _ResourceModel.Clear();
            if (withRes) 
                _ResourceModel.Init(path, this);

            _luaSettings.Read(path + "\\Scripts\\Settings.lua");
            return true;
        }

        public void Save(string path = "")
        {
            foreach (BaseTable table in tableDescs.Values)
            {
                table.Save(path);
            }
        }
    }
}
