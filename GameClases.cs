﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NevendaarTools
{
    public class Gaction
    {
        public string action_id;
        public int category;
        public int chance;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;

        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class GAI
    {
        public int category;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public int b_units;
        public int b_leaders;
        public int b_cap;
        public int b_cities;
        public int b_spell_c;
        public int b_spell_r;
        public int b_iso;
        public int l_min;
        public int l_max;
        public int pct_fightr;
        public int t_min;
        public int t_max;
        public int pct_thief;
        public int r_min;
        public int r_max;
        public int pct_rod;
        public int i_cities_n;
        public int i_cities_r;
        public int i_cities_p;
        public int i_stacks_n;
        public int i_stacks_r;
        public int i_stacks_p;
        public int i_bags;
        public int i_ruins;
        public int i_towers;
        public int i_merchant;
        public int i_h_units;
        public int i_h_mercs;
        public int i_heal;
        public int i_p_rod;
        public int i_a_rod;
        public int i_n_cities;
        public int i_n_stack;
        public int i_trainer;
        public int i_obj_m;
        public int d_easy_m;
        public int d_avg_m;
        public int d_hard_m;
        public int d_vhard_m;
        public int d_easy_xp;
        public int d_avg_xp;
        public int d_hard_xp;
        public int d_vhard_xp;
        public int hp_staymin;
        public int hp_healmin;
        public int cstchnce;
        public int cstchnce_2;
        public int cstdbl_min;
        public int spl1_chnce;
        public int spl2_chnce;
        public int spl3_chnce;
        public int spl_h_w;
        public int spl_h_c;
        public int spl_m_w;
        public int spl_m_c;
        public int spl_l_w;
        public int spl_l_c;
        public int spl_l5_pow;
        public int spl_l4_pow;
        public int spl_l3_pow;
        public int spl_l2_pow;
        public int critspl_w1;
        public int critspl_c1;
        public int critspl_w2;
        public int critspl_c2;
        public int critspl_w3;
        public int critspl_c3;
        public int critspl_w4;
        public int critspl_c4;

        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class Gattack
    {
        public string att_id;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public int initiative;
        public IntLink<LattS> source;
        public IntLink<LattC> clasS;
        public int power;//accuracy
        public IntLink<LAttR> reach;
        public int qty_heal;
        public int qty_dam;
        public int level;
        public StringLink<Gattack> alt_attack;
        public bool infinite;
        public int qty_wards;
        public StringLink<Gmodif> ward1;
        public StringLink<Gmodif> ward2;
        public StringLink<Gmodif> ward3;
        public StringLink<Gmodif> ward4;
        public bool crit_hit;
        public int dam_ratio;
        public bool dr_repeat;
        public bool dam_split;
        public int crit_dam;
        public int crit_power;
        //
        public StringLinkList<Gtransf> transf;
        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class Gbuild
    {
        public string build_id;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public string required;
        public string cost;
        public int level;
        public IntLink<Lbuild> category;
        public IntLink<LunitB> branch;
        public string pic_1;
        public int frame_1;
        public int frame_2;
        public int pos_x;
        public int pos_y;
        public int pos_cx;
        public int pos_cy;
        public int pos_z;
        public string comments;
        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class GbuiList
    {
        public StringLink<Glord> lord_id;
        public StringLink<Gbuild> build_id;
    }
    public class GcityInf
    {
        public string race_id;
        public int size;
        public int scout;
        public int regen_f;
        public string protection;
        public string growth;
        public int res_m;
    }
    public class GDynUpgr
    {
        public string upgrade_id;
        public string enroll_c;
        public int hit_point;
        public int armor;
        public int regen;
        public string revive_c;
        public string heal_c;
        public string training_c;
        public int xp_killed;
        public int xp_next;
        public int move;
        public int negotiate;
        public int damage;
        public int heal;
        public int initiative;
        public int power;
    }
    public class Gimmu
    {
        public string unit_id;
        public IntLink<LattS> immunity;
        public IntLink<LImmune> immunecat;
    }
    public class GimmuC
    {
        public string unit_id;
        public IntLink<LattC> immunity;
        public IntLink<LImmune> immunecat;
    }
    public class GItem
    {
        public enum Category
        {
            Talisman = 12
        }
        public int item_cat;
        public string item_id;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public string image_id;
        public string value;
        public StringLink<Gmodif> mod_equip;
        public StringLink<Gmodif> mod_potion;
        public int hp_potion;
        public StringLink<Gspell> spell_id;
        public string casting;
        public StringLink<Gattack> attack_id;
        public StringLink<Gunit> unit_id;
        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class GLabi
    {
        public string unit_id;
        public IntLink<LleadA> l_ability;
    }
    public class GleaUpg
    {
        public StringLink<Glord> belongs_to;
        public StringLink<Gmodif> modif_id;
        public int min_level;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public int priority;
        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class GLmark
    {
        public string lmark_id;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public int cx;
        public int cy;
        public bool mountain;
        public int category;
        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class Glord
    {
        public string lord_id;
        public StringLink<Grace> race_id;
        public IntLink<Llord> category;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public string pic;

        public StringLinkList<GleaUpg> percs;
    }
    public class GMabi
    {
        public string unit_id;
        public int m_ability;
    }
    public class Gmodif
    {
        public string modif_id;
        public int source;
        public string comments;
        public string script;
        public StringLink<Tglobal> desc_txt;
        public bool display;
        //
        public StringLinkList<GmodifL> effects;
        public override string ToString()
        {
            if (desc_txt.value != null)
                return desc_txt.value.text;
            return "";
        }
    }

    public class GUmodif
    {
        public StringLink <Gunit> unit_id;
        public StringLink<Gmodif> modif_1;
        public StringLink<Gmodif> modif_2;
        public StringLink<Gmodif> modif_3;
        public StringLink<Gmodif> modif_4;
        public StringLink<Gmodif> modif_5;
        public StringLink<Gmodif> modif_6;
        public StringLink<Gmodif> modif_7;
        public StringLink<Gmodif> modif_8;
        public StringLink<Gmodif> modif_9;
    }

    public class GmodifL
    {
        public StringLink<Gmodif> belongs_to;
        public StringLink<Tglobal> desc;
        public int type;
        public int percent;
        public int number;
        public int ability;
        public IntLink<LattS> immunity;
        public IntLink<LImmune> immunecat;
        public IntLink<LattC> immunityc;
        public IntLink<LImmune> immunecatc;
        public int move;
    }
    public class Grace
    {
        public string race_id;
        public StringLink<Gunit> guardian;
        public string noble;
        public StringLink<Gunit> leader_1;
        public StringLink<Gunit> leader_2;
        public StringLink<Gunit> leader_3;
        public StringLink<Gunit> leader_4;
        public StringLink<Gunit> soldier_1;
        public StringLink<Gunit> soldier_2;
        public StringLink<Gunit> soldier_3;
        public StringLink<Gunit> soldier_4;
        public StringLink<Gunit> soldier_5;
        public StringLink<Gunit> soldier_6;
        public int scout;
        public int regen_h;
        public string income;
        public string protection;
        public StringLink<Tglobal> name_txt;
        public IntLink<Lrace> race_type;
        public bool playable;

        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }

        public string ShortName()
        {
            if (race_type.value == null)
                return "";
            return race_type.value.text.Substring(2, 2);
        }
    }
    public class GspellR
    {
        public StringLink<Glord> lord_id;
        public string spell_id;
        public string research;
    }
    public class Gspell
    {
        public string spell_id;
        public int category;
        public int level;
        public string casting_c;
        public string buy_c;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public StringLink<Gunit> unit_id;
        public int restore_m;
        public int area;
        public StringLink<Gmodif> modif_id;
        public StringLink<Tglobal> modif_txt;
        public int damage_src;
        public int damage_qty;
        public int heal_qty;
        public int ai_cat;
        public int ground_cat;
        public bool chngtercat;
        public int qty_wards;
        public string ward1;
        public string ward2;
        public string ward3;
        public string ward4;

        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }

        public StringLinkList<GspellR> researchCost;
    }
    public class GSubRace
    {
        public string race_id;
        public StringLink<Tglobal> name_txt;
        public IntLink<LSubRace> race_type;

        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class GTileDBI
    {
        public string terrain;
        public string ground;
        public int ndx;
        public int qty;
    }
    public class Gtransf
    {
        public string attack_id;
        public StringLink<Gunit> transf_id;
    }
    public class Gunit
    {
        public string unit_id;
        public IntLink<LunitC> unit_cat;
        public int level;
        public StringLink<Gunit> prev_id;
        public StringLink<Grace> race_id;
        public IntLink<LSubRace> subrace;
        public IntLink<LunitB> branch;
        public bool size_small;
        public bool sex_m;
        public string enroll_c;
        public StringLink<Gbuild> enroll_b;
        public StringLink<Tglobal> name_txt;
        public StringLink<Tglobal> desc_txt;
        public StringLink<Tglobal> abil_txt;
        public StringLink<Gattack> attack_id;
        public StringLink<Gattack> attack2_id;
        public bool atck_twice;
        public int hit_point;
        public StringLink<Gunit> base_unit;
        public int armor;
        public int regen;
        public string revive_c;
        public string heal_c;
        public string training_c;
        public int xp_killed;
        public StringLink<Gbuild> upgrade_b;
        public int xp_next;
        public int move;
        public int scout;
        public int life_time;
        public int leadership;
        public int negotiate;
        public IntLink<LleadC> leader_cat;
        public StringLink<GDynUpgr> dyn_upg1;
        public int dyn_upg_lv;
        public StringLink<GDynUpgr> dyn_upg2;
        public bool water_only;
        public int death_anim;
        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }

        //
        public StringLinkList<Gimmu> immun;
        public StringLinkList<GimmuC> immunCategory;
        public StringLinkList<GUmodif> modifs;
        public StringLinkList<GLabi> abils;
    }
    public class GVars
    {
        public int morale_1;
        public int morale_2;
        public int morale_3;
        public int morale_4;
        public int morale_5;
        public int morale_6;
        public int weapn_mstr;
        public int bat_init;
        public int bat_damage;
        public int bat_round;
        public int bat_break;
        public int bat_bmodif;
        public int batboostd1;
        public int batboostd2;
        public int batboostd3;
        public int batboostd4;
        public int batlowerd1;
        public int batlowerd2;
        public int batloweri1;
        public int ldrmaxabil;
        public int spy_discov;
        public int poison_s;
        public int poison_c;
        public int bribe;
        public int steal_race;
        public int steal_neut;
        public int riot_min;
        public int riot_max;
        public int riot_dmg;
        public int sell_ratio;
        public int t_capture;
        public int t_capital;
        public int t_city1;
        public int t_city2;
        public int t_city3;
        public int t_city4;
        public int t_city5;
        public string rod_cost;
        public int rod_range;
        public int crystal_p;
        public int const_urg;
        public int regen_lwar;
        public int regen_ruin;
        public int d_peace;
        public int d_war;
        public int d_neutral;
        public int d_gold;
        public int d_mk_ally;
        public int d_attak_sc;
        public int d_attak_fo;
        public int d_attak_pc;
        public int d_rod;
        public int d_ref_ally;
        public int d_bk_ally;
        public int d_noble;
        public int d_bka_chnc;
        public int d_bka_turn;
        public int prot_1;
        public int prot_2;
        public int prot_3;
        public int prot_4;
        public int prot_5;
        public int prot_cap;
        public int bonus_e;
        public int bonus_a;
        public int bonus_h;
        public int bonus_v;
        public int income_e;
        public int income_a;
        public int income_h;
        public int income_v;
        public int gu_range;
        public int pa_range;
        public int lo_range;
        public int dfendbonus;
        public int talis_chrg;
        public int splpwr_0;
        public int splpwr_1;
        public int splpwr_2;
        public int splpwr_3;
        public int splpwr_4;
        public int gain_spell;
        public string tutorial;
    }
    public class Laction
    {
        public int id;
        public string text;
    }
    public class LaiAtt
    {
        public int id;
        public string text;
    }
    public class LAiMsg
    {
        public int id;
        public string text;
    }
    public class LAiSpell
    {
        public int id;
        public string text;
    }
    public class LattC
    {
        public int id;
        public string text;
    }
    public class LAttR
    {
        public int id;
        public string text;
        public StringLink<TApp> reach_txt;
        public StringLink<TApp> target_txt;
        public int max_targts;
        public bool melee;
    }
    public class LattS
    {
        public int id;
        public string text;
        public StringLink<TApp> name_txt;
        public int immu_ai_r;

        public override string ToString()
        {
            if (name_txt.value != null)
                return name_txt.value.text;
            return "";
        }
    }
    public class Lbuild
    {
        public int id;
        public string text;
    }
    public class Ldiff
    {
        public int id;
        public string text;
    }
    public class LDthAnim
    {
        public int id;
        public string text;
    }
    public class LEvCond
    {
        public int id;
        public string text;
    }
    public class LEvEffct
    {
        public int id;
        public string text;
    }
    public class LFort
    {
        public int id;
        public string text;
    }
    public class Lground
    {
        public int id;
        public string text;
    }
    public class LImmune
    {
        public int id;
        public string text;
    }
    public class LleadA
    {
        public int id;
        public string text;
    }
    public class LleadC
    {
        public int id;
        public string text;
    }
    public class LLMCat
    {
        public int id;
        public string text;
    }
    public class Llord
    {
        public int id;
        public string text;
    }
    public class LmagItm
    {
        public int id;
        public string text;
    }
    public class LmodifE
    {
        public int id;
        public string text;
    }
    public class LModifS
    {
        public int id;
        public string text;
    }
    public class LOrder
    {
        public int id;
        public string text;
    }
    public class Lrace
    {
        public int id;
        public string text;
    }
    public class Lres
    {
        public int id;
        public string text;
    }
    public class Lsite
    {
        public int id;
        public string text;
    }
    public class Lspell
    {
        public int id;
        public string text;
    }
    public class LSplPhas
    {
        public int id;
        public string text;
    }
    public class LSubRace
    {
        public int id;
        public string text;
    }
    public class Lterrain
    {
        public int id;
        public string text;
    }
    public class Ltreaty
    {
        public int id;
        public string text;
    }
    public class LunitB
    {
        public int id;
        public string text;
    }
    public class LunitC
    {
        public int id;
        public string text;
    }
    public class TAiMsg
    {
        public string race_id;
        public int msg_cat;
        public string text;
    }
    public class Tglobal
    {
        public string txt_id;
        public string text;
        public bool verified;
        public string context;
        public override string ToString()
        {
            return text;
        }
    }

    public class TApp
    {
        public string txt_id;
        public string text;
        public override string ToString()
        {
            return text;
        }
    }

    public class Tleader
    {
        public string race_id;
        public bool sex_m;
        public string text;
        public bool verified;
        public string context;
    }
    public class Tplayer
    {
        public string race_id;
        public string text_m;
        public string text_f;
        public bool verified;
        public string context;
    }
}
