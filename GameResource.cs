﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace NevendaarTools
{
    public class GameResource
    {
        public string path;
        List<ResourceDesc> resources = new List<ResourceDesc>();
        byte[] header = new byte [28];
        ResourceDesc names_desc = null;
        ResourceDesc images_opt = null;
        ResourceDesc indexes_opt = null;
        ResourceDesc anim_opt = null;
        
        public bool Read(string filename)
        {
            using (FileStream fstream = File.OpenRead(filename))
            {
                if (fstream.Length < 1)
                    return false;
                path = filename;

                long index = 28;
                fstream.Read(header, 0, (int)index);
                while (index < fstream.Length)
                {
                    fstream.Seek(index, SeekOrigin.Begin);
                    ResourceDesc desc = new ResourceDesc();
                    index = 28 + desc.Read(fstream);
                    if (desc.uid == 2)
                        names_desc = desc;
                    resources.Add(desc);
                }
                
            }
            RestoreNames();
            return true;
        }

        public ResourceDesc ResDescByUid(int uid)
        {
            for (int k = 0; k < resources.Count; ++k)
            {
                if (resources[k].uid == uid)
                {
                    return resources[k];
                }
            }
            return null;
        }

        public ResourceDesc ResDescByName(string name)
        {
            for (int k = 0; k < resources.Count; ++k)
            {
                if (resources[k].name == null)
                    continue;
                if (resources[k].name.Replace(".PNG","") == name)
                {
                    return resources[k];
                }
            }
            return null;
        }

        public List<string> GetAllDescs()
        {
            List<string> result = new List<string>();
            for (int k = 0; k < resources.Count; ++k)
            {
                if (resources[k].name == null)
                    continue;
                result.Add(resources[k].name.Replace(".PNG", ""));
            }
            return result;
        }
        public List<string> GetAllIndexOptKeys()
        {
            List<string> result = new List<string>();
            if (indexes_opt == null)
                return null;
            IndexesData indexesData = indexes_opt.data as IndexesData;
            for (int i = 0; i < indexesData.indexesData.Count; ++i)
            {
                result.Add(indexesData.indexesData[i].name);
            }
            return result;
        }

        public IndexesData.IndexesDesc GetIndexDescByName(string name)
        {
            if (indexes_opt == null)
                return null;
            IndexesData indexesData = indexes_opt.data as IndexesData;
            for (int i = 0; i < indexesData.indexesData.Count; ++i)
            {
                if (indexesData.indexesData[i].name == name)
                {
                    return indexesData.indexesData[i];
                }
            }
            return null;
        }

        Image GetSimpleImageByName(string id, bool makeTransparent = true)
        {
            ResourceDesc resDesc = ResDescByName(id);//names was restored on load
            if (resDesc == null)
                return null;
            byte[] array = resDesc.GetData(this.path);
            if (array.Length < 10)
                return null;
            MemoryStream stream = new MemoryStream(array);
            Image sourceImage = Image.FromStream(stream);
            if (makeTransparent)
                sourceImage = ImageHelper.MakeTransparent(ref sourceImage, null);
            return sourceImage;
        }

        public bool HasResource(string name)
        {
            if (indexes_opt == null)//only mqdb in file
            {
                ResourceDesc resDesc = ResDescByName(name);//names was restored on load
                return (resDesc != null);
            }
            IndexesData.IndexesDesc indexDesc = GetIndexDescByName(name);
            if (indexDesc == null)
                return false;
            
            if (indexDesc.uid != -1)//simple image with restore data
            {
                return (images_opt != null);
            }
            else //indexDesc.uid == -1, this is animation
            {
                return (anim_opt != null);
            }
        }

        public List<Image> GetFramesById_(string id, bool makeTransparent = true)
        {
            List<Image> result = new List<Image>();
            if (indexes_opt == null)//only mqdb in file
            {
                Image img = GetSimpleImageByName(id, makeTransparent);
                if (img != null)
                    result.Add(img);
                return result;
            }
            IndexesData.IndexesDesc indexDesc = GetIndexDescByName(id);
            if (indexDesc == null)
            {
                Image img = GetSimpleImageByName(id, makeTransparent);
                if (img != null)
                    result.Add(img);
                return result;
            }
            ImagesContainer imagesCon = images_opt.data as ImagesContainer;

            if (indexDesc.uid != -1)//simple image with restore data
            {
                if (images_opt == null)//something wrong
                    return result;
                ImagesContainer.ImageData imageInfo = imagesCon.GetImageData(
                    images_opt.offset + 28 + indexDesc.relatedOffset, 
                    indexDesc.relatedSize, this.path);

                ResourceDesc resDesc = ResDescByUid(indexDesc.uid);
                if (resDesc == null)//failed to find source image
                    return result;
                byte[] array = resDesc.GetData(this.path);
                MemoryStream stream = new MemoryStream(array);
                if (stream == null)
                    return result;
                Image sourceImage = Image.FromStream(stream);

                if (imageInfo == null || imageInfo.restoreData.Count < 1)
                    return result;
                for (int i = 0; i < imageInfo.restoreData.Count; ++i)//might be only one, but...
                {
                    if (imageInfo.restoreData[i].name != id)
                        continue;
                    ImagesContainer.ImageRestoreData restoreData = imageInfo.restoreData[i];
                    Image frame = new Bitmap(restoreData.w, restoreData.h);
                    if (restoreData.parts.Count > 0)
                    {
                        using (Graphics g = Graphics.FromImage(frame))
                        {
                            GraphicsUnit units = GraphicsUnit.Pixel;
                            foreach (ImagesContainer.ImageRestoreData.ImagePart data in restoreData.parts)
                                g.DrawImage(sourceImage,
                                    new Rectangle(data.sourceX, data.sourceY, data.w, data.h),
                                    new Rectangle(data.targetX, data.targetY, data.w, data.h),
                                    units);
                        }
                    }
                    if (makeTransparent)
                        frame = ImageHelper.MakeTransparent(ref frame, imageInfo.header);
                    result.Add(frame);
                }
                return result;
            }
            else //indexDesc.uid == -1, this is animation
            {
                AnimationsContainer animData = anim_opt.data as AnimationsContainer;
                AnimationsContainer.AnimationsDesc animDesc = animData.GetAnimationsDesc(
                    anim_opt.offset + 28 + indexDesc.relatedOffset, 
                    indexDesc.relatedSize, this.path);
                ImagesContainer.ImageData imageData = null;
                for (int i = 0; i < animDesc.frameNames.Count; ++i)
                {
                    IndexesData.IndexesDesc sourceDesc = GetIndexDescByName(animDesc.frameNames[i]);
                    if (imageData == null || imageData.localOffset != sourceDesc.relatedOffset)
                    {
                        imageData = imagesCon.GetImageData(
                            images_opt.offset + 28 + sourceDesc.relatedOffset,
                            sourceDesc.relatedSize, this.path);
                    }
                    ImagesContainer.ImageRestoreData restoreData = null;
                    for (int k = 0; k < imageData.restoreData.Count; ++k)
                    {
                        if (imageData.restoreData[k].name == animDesc.frameNames[i])
                        {
                            restoreData = imageData.restoreData[k];
                        }
                    }
                    if (restoreData == null)
                        continue;

                    ResourceDesc resDesc = ResDescByUid(sourceDesc.uid);
                    if (resDesc == null)//failed to find source image
                        return result;

                    byte[] array = resDesc.GetData(this.path);
                    MemoryStream stream = new MemoryStream(array);
                    if (stream == null)
                        return result;
                    Image sourceImage = Image.FromStream(stream);
                    Image frame = new Bitmap(restoreData.w, restoreData.h);
                    using (Graphics g = Graphics.FromImage(frame))
                    {
                        GraphicsUnit units = GraphicsUnit.Pixel;
                        foreach (ImagesContainer.ImageRestoreData.ImagePart data in restoreData.parts)
                            g.DrawImage(sourceImage,
                                new Rectangle(data.sourceX, data.sourceY, data.w, data.h),
                                new Rectangle(data.targetX, data.targetY, data.w, data.h),
                                units);
                    }
                    if (makeTransparent)
                        frame = ImageHelper.MakeTransparent(ref frame, imageData.header);
                    result.Add(frame);
                }
            }

            return result;
        }

        public int GetFramesCountById(string id)
        {
            if (indexes_opt == null)//only mqdb in file
            {
                ResourceDesc resDesc = ResDescByName(id);//names was restored on load
                if (resDesc == null)
                    return 0;
                return 1;
            }
            IndexesData.IndexesDesc indexDesc = GetIndexDescByName(id);
            if (indexDesc == null)
            {
                ResourceDesc resDesc = ResDescByName(id);//names was restored on load
                if (resDesc == null)
                    return 0;
                return 1;
            }
            ImagesContainer imagesCon = images_opt.data as ImagesContainer;

            if (indexDesc.uid != -1)//simple image with restore data
            {
                if (images_opt == null)//something wrong
                    return 0;
                ImagesContainer.ImageData imageInfo = imagesCon.GetImageData(
                    images_opt.offset + 28 + indexDesc.relatedOffset,
                    indexDesc.relatedSize, this.path);

                return imageInfo.restoreData.Count;
            }
            else //indexDesc.uid == -1, this is animation
            {
                AnimationsContainer animData = anim_opt.data as AnimationsContainer;
                AnimationsContainer.AnimationsDesc animDesc = animData.GetAnimationsDesc(
                    anim_opt.offset + 28 + indexDesc.relatedOffset,
                    indexDesc.relatedSize, this.path);

                return animDesc.frameNames.Count;
            }
        }

        void RestoreNames()
        {
            NamesListData namesData = null;
            for (int i = 0; i < resources.Count; ++i)
            {
                if (resources[i].uid == 2)
                {
                    namesData = new NamesListData();
                    namesData.SetData(resources[i].GetData(this.path));
                    resources[i].data = namesData;
                    names_desc = resources[i];
                }
            }
            if (namesData == null)
                return;
            
            for (int i = 0; i < resources.Count; ++i)
            {
                for(int q = 0; q < namesData.bindings.Count; ++q)
                {
                    if (resources[i].uid == namesData.bindings[q].id)
                    {
                        resources[i].name = namesData.bindings[q].name;
                        if (images_opt == null && resources[i].name == "-IMAGES.OPT")
                        {
                            resources[i].data = new ImagesContainer();
                            images_opt = resources[i];
                        }
                        else if (anim_opt == null && resources[i].name == "-ANIMS.OPT")
                        {
                            resources[i].data = new AnimationsContainer();
                            anim_opt = resources[i];
                        }
                        else if (indexes_opt == null && resources[i].name == "-INDEX.OPT")
                        {
                            IndexesData indexesData = new IndexesData();
                            indexesData.SetData(resources[i].GetData(this.path));
                            resources[i].data = indexesData;
                            indexes_opt = resources[i];
                        }
                        break;
                    }
                }
            }
        }  

        bool Write(string fileName)
        {

            return true;
        }
    }
}
