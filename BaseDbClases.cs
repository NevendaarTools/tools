﻿using NevendaarTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using static NevendaarTools.SimpleDBFAdapter;

namespace NevendaarTools
{
    public interface IGameField
    {
        void setKey(object val);
        void setMaped(object val);
        object GetKey();
        bool LoadMapped(BaseDataModel model);
        object GetValue();
    }
    public interface IGameFieldList
    {
        void setKey(object val);
        void setMaped(object val);
        string GetKey();
        bool LoadMapped(BaseDataModel model);
        object GetValue();
    }

    public struct StringLinkList<mappedT> : IGameFieldList where mappedT : class, new()
    {
        public string key;
        public List<mappedT> value;

        public int Count() { return value.Count(); }
        public override string ToString()
        {
            if (value == null)
                return key.ToString();
            return key.ToString() + "-" + value.ToString();
        }
        public void setMaped(object val)
        {
            value = val as List<mappedT>;
        }

        public void setKey(object val)
        {
            if (val == null)
                key = null;
            else
                key = val.ToString();
        }

        public string GetKey()
        {
            return key.ToString();
        }

        public object GetValue()
        {
            return value;
        }

        public bool LoadMapped(BaseDataModel model)
        {
            if (key == null)
                return true;
            value = model.GetObjectListT<mappedT>(key);
            return true;
        }
    }

    public struct StringLink<mappedT> : IGameField where mappedT : class, new()
    {
        public string key;
        public mappedT value;

        public override string ToString()
        {
            if (value == null)
                return key.ToString();
            return key.ToString() + "-" + value.ToString();
        }
        public void setMaped(object val)
        {
            value = val as mappedT;
        }

        public void setKey(object val)
        {
            if (val == null)
                key = null;
            else
                key = val.ToString();
        }

        public object GetKey()
        {
            return key;
        }
        public object GetValue()
        {
            return value;
        }
        public bool LoadMapped(BaseDataModel model)
        {
            if (key == null)
                return true;
            value = model.GetObjectT<mappedT>(key);
            return true;
        }
    }
    public struct IntLink<mappedT> : IGameField where mappedT : class, new()
    {
        public int key;
        public mappedT value;

        public override string ToString()
        {
            if (value == null)
                return key.ToString();
            return key.ToString() + "-" + value.ToString();
        }
        public void setMaped(object val)
        {
            value = val as mappedT;
        }

        public void setKey(object val)
        {
            if (val == null)
                key = -1;
            else
                key = Convert.ToInt32(val);
        }

        public object GetKey()
        {
            return key;
        }
        public object GetValue()
        {
            return value;
        }
        public bool LoadMapped(BaseDataModel model)
        {
            if (key == -1)
                return true;
            value = model.GetObjectT<mappedT>(key);
            return true;
        }
    }

    public class BaseDataModel
    {
        public void Register(string name, BaseTable table)
        {
            tableDescs.Add(name, table);
        }
        public void UpdateObject(object obj)
        {
            string tableName = obj.GetType().Name;
            BaseTable table = tableDescs[tableName];
            int index = 0;
            table.Write(this, obj, index);
        }
        public T GetObjectT<T>(object key) where T : class, new()
        {
            string tableName = typeof(T).Name;
            if (!tableDescs.ContainsKey(tableName))
                return null;
            BaseTable table = tableDescs[tableName];
            return table.GetObjectT<T>(this, key);
        }
        public object GetObject(object key, string tableName) 
        {
            BaseTable table = tableDescs[tableName];
            if (!tableDescs.ContainsKey(tableName))
                return null;
            return table.GetObject(this, key);
        }

        public List<T> GetObjectListT<T>(object key) where T : class, new()
        {
            string tableName = typeof(T).Name;
            if (!tableDescs.ContainsKey(tableName))
                return null;
            BaseTable table = tableDescs[tableName];
            return table.GetObjectListT<T>(this, key);
        }
        public List<T> GetAllT<T>() where T : class, new()
        {
            string tableName = typeof(T).Name;
            if (!tableDescs.ContainsKey(tableName))
                return null;
            BaseTable table = tableDescs[tableName];
            return table.GetAllT<T>(this);
        }

        protected Dictionary<string, BaseTable> tableDescs = new Dictionary<string, BaseTable>();
    }

    public class BaseTable
    {
        protected SimpleDBFAdapter adapter = new SimpleDBFAdapter();
        string path;
        string name;
        Dictionary<string, object> cashedData = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        int keyIndex = 0;

        public BaseTable()
        {

        }
        public BaseTable(string fileName, int keyIndex_ = 0)
        {
            keyIndex = keyIndex_;
            Load(fileName);
        }
        public void Load(string fileName)
        {
            if (!File.Exists(fileName))
            {
                Logger.LogError("Failed to open DBF file: " + fileName);
                return;
            }
            adapter.ReadDBFFile(fileName);
            FileInfo fi = new FileInfo(fileName);
            name = fi.Name;
            path = fi.DirectoryName;
        }
        public bool Save(string path)
        {
            return true;
        }

        public List<T> GetObjectListT<T>(BaseDataModel model, object key) where T : class, new()
        {
            List<T> result = new List<T>();
            for (int i = 0; i < adapter.RowCount(); ++i)
            {
                if (adapter.GetString(keyIndex, i).OrdinalEquals(key.ToString()))
                {
                    Object resItem = new T();
                    if (Read(model, ref resItem, i))
                    {
                        ReadMapped(model, ref resItem);
                        result.Add(resItem as T);
                    }
                }
            }
            
            return result;
        }

        public List<T> GetAllT<T>(BaseDataModel model) where T : class, new()
        {
            List<T> result = new List<T>();
            for (int i = 0; i < adapter.RowCount(); ++i)
            {
                Object resItem = new T();
                if (Read(model, ref resItem, i))
                {
                    ReadMapped(model, ref resItem);
                    result.Add(resItem as T);
                }
            }

            return result;
        }

        public T GetObjectT<T>(BaseDataModel model, int index) where T : class, new()
        {
            Object result = new T();
            if (Read(model, ref result, index))
            {
                return result as T;
            }
            return null;
        }

        public static bool IsNullId(object id)
        {
            if (id == null) 
                return true;
            if (id.GetType().Name.OrdinalEquals("int32"))
            {
                int val = Int32.Parse(id.ToString());
                if (Int32.Parse(id.ToString()) == -1)
                    return true;
            }
            if (id.ToString().Trim() == "" || id.ToString().OrdinalEquals("g000000000"))
                return true;
            return false;
        }
        public T GetObjectT<T>(BaseDataModel model, object key) where T : class, new()
        {
            if (IsNullId(key))
                return null;
            string strKey = key.ToString();
            if (cashedData.ContainsKey(strKey))
            {
                return cashedData[strKey] as T;
            }
            int index = IndexById(key);
            if (index == -1)
            {
                Logger.LogError("TableName = " + name);
                return null;
            }
            Object result = GetObjectT<T>(model, index);
            cashedData.Add(strKey, result);
            ReadMapped(model, ref result);
            return result as T;
        }

        public object GetObject(BaseDataModel model, int index) 
        {
            Object result = new Object();
            if (Read(model, ref result, index))
            {
                return result;
            }
            return null;
        }

        public object GetObject(BaseDataModel model, object key)
        {
            if (IsNullId(key))
                return null;
            string strKey = key.ToString();
            if (cashedData.ContainsKey(strKey))
            {
                return cashedData[strKey];
            }
            int index = IndexById(key);
            if (index == -1)
            {
                Logger.LogError("TableName = " + name);
                return null;
            }
            Object result = GetObject(model, index);
            cashedData.Add(strKey, result);
            ReadMapped(model, ref result);
            return result;
        }

        int IndexById(object key)
        {
            for (int i = 0; i < adapter.RowCount(); ++i)
            {
                if (adapter.GetString(keyIndex, i).OrdinalEquals(key.ToString()))
                    return i;
            }
            Logger.LogError("Id not exist : " + key.ToString());
            return -1;
        }

        public virtual bool ReadMapped(BaseDataModel model, ref Object obj)
        {
            if (obj == null)
                return true;
            foreach (FieldInfo field in obj.GetType().GetFields())
            {
                if (typeof(IGameField).IsAssignableFrom(field.FieldType))
                {
                    IGameField gameField = field.GetValue(obj) as IGameField;
                    gameField.LoadMapped(model);
                    if (gameField.GetValue() == null && !IsNullId(gameField.GetKey()))
                    {
                        Logger.LogWarning("Failed to get mapped value in " + obj.GetType().Name + " " + 
                            obj.GetType().GetFields()[keyIndex].GetValue(obj).ToString() + 
                            " in field " + field.Name);
                    }
                    field.SetValue(obj, gameField);
                    continue;
                }
                if (typeof(IGameFieldList).IsAssignableFrom(field.FieldType))
                {
                    IGameFieldList gameField = field.GetValue(obj) as IGameFieldList;
                    gameField.LoadMapped(model);
                    field.SetValue(obj, gameField);
                    continue;
                }
            }
            return true;
        }

        public virtual bool Read(BaseDataModel model, ref Object obj, int index)
        {
            foreach (FieldInfo field in obj.GetType().GetFields())
            {
                if (typeof(IGameField).IsAssignableFrom(field.FieldType))
                {
                    IGameField gameField = field.GetValue(obj) as IGameField;
                    string value = adapter.GetString(field.Name, index);
                    if (value == null || value.ToString() == "")
                        gameField.setKey(null); 
                    else
                        gameField.setKey(value.Trim());
                    field.SetValue(obj, gameField);
                    continue;
                }
                if (typeof(IGameFieldList).IsAssignableFrom(field.FieldType))
                {
                    IGameFieldList gameField = field.GetValue(obj) as IGameFieldList;
                    string value = adapter.GetString(keyIndex, index).Trim();
                    if (value.ToString() == "" || value == null)
                        gameField.setKey(null);
                    else
                        gameField.setKey(value);
                    field.SetValue(obj, gameField);
                    continue;
                }
                string fieldType = field.FieldType.Name.ToLower();
                switch (fieldType)
                {
                    case "boolean":
                        field.SetValue(obj, adapter.GetBool(field.Name, index));
                        break;
                    case "int32":
                        field.SetValue(obj, adapter.GetInt(field.Name, index, 0));
                        break;

                    case "double":
                        break;

                    case "string":
                        field.SetValue(obj, adapter.GetString(field.Name, index));
                        break;
                }
                
            }
            return true;
        }
        public virtual bool Write(BaseDataModel model, Object obj, int index)
        {
            foreach (FieldInfo field in obj.GetType().GetFields())
            {
                if (typeof(IGameField).IsAssignableFrom(field.FieldType))
                {
                    IGameField gameField = field.GetValue(obj) as IGameField;
                    adapter.Set(field.Name, index, gameField.GetKey().ToString());
                    continue;
                }
                string fieldType = field.FieldType.Name.ToLower();
                switch (fieldType)
                {
                    case "boolean":
                        field.SetValue(obj, adapter.GetBool(field.Name, index));
                        break;
                    case "int32":
                        adapter.Set(field.Name, index, field.GetValue(obj).ToString());
                        break;

                    case "double":
                        break;

                    case "string":
                        adapter.Set(field.Name, index, field.GetValue(obj).ToString());
                        break;
                }

            }
            return true;
        }
    }
    public class DBDescriptionFiller
    {
        public List<string> clases = new List<string>();
        public List<string> descriptionRegister = new List<string>();
        public void Fill(string path)//"E:/Games/Semga_actual/Globals/"
        {
            Append1("namespace NevendaarTools\n{");

            
            string[] AllFiles = Directory.GetFiles(path, "*.dbf", SearchOption.AllDirectories);
            foreach (string filename in AllFiles)
            {
                Process(filename);
            }

            Append1("\n}");
        }
        public void Append1(string text)
        {
            clases.Add(text + "\n");
        }
        public void Append2(string text)
        {
            descriptionRegister.Add(text + "\n");
        }

        public void Process(string fileName)
        {
            SimpleDBFAdapter adapter = new SimpleDBFAdapter();
            adapter.ReadDBFFile(fileName);
            FileInfo fi = new FileInfo(fileName);
            string name = fi.Name;
            name = name.Replace(".dbf", "");
            name = name.Replace(".DBF", "");

            Append1("    public class " + name + "\n    {");
            List<string> fields = new List<string>();
            for (int i = 0; i < adapter.ColumnCount(); ++i)
            {
                HeaderData header = adapter.GetHeader(i);
                string fieldDesc = "        public ";
                switch (header.typeStr)
                {
                    case "C":
                        fieldDesc += "string";
                        break;
                    case "N":
                        fieldDesc += "int";
                        break;
                    case "L":
                        fieldDesc += "bool";
                        break;
                    case "D":
                        fieldDesc += "data";
                        break;
                    case "F":
                        fieldDesc += "float";
                        break;
                    case "M":
                        fieldDesc += "string";
                        break;
                }
                fieldDesc += " " + header.name.ToLower() + ";";
                fields.Add(header.name.ToLower());
                Append1(fieldDesc);
            }
            //Append1("        void read(SimpleDBFAdapter & adapter, GameModel * model, int row)\n{\n        ");
            //foreach(string tmp in fields)
            //{
            //    Append1("            readValue(" + tmp + ", \"" + tmp.ToUpper() + "\", adapter, model, row);");
            //}
            //Append1("        }\n");
            //Append1("        void read(SimpleDBFAdapter & adapter, GameModel * model, int row)\n{\n        ");
            //foreach (string tmp in fields)
            //{
            //    Append1("            writeValue(" + tmp + ", \"" + tmp.ToUpper() + "\", adapter, model, row);");
            //}
            //Append1("    }\n");
            Append1("    }\n");
            Append2("            Register(typeof(" + name + ").Name, new BaseTable(path + \"\\\\" + name + ".dbf\"));");
        }
    }
}
