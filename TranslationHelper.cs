﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NevendaarTools
{
    public class TranslationHelper
    {
        public delegate void TranslationChangedDelegate();
        public TranslationChangedDelegate TranslationChanged;

        public class Translation
        {
            public string lang = "";
            public Dictionary<string, string> _data = new Dictionary<string, string>();
        }

        private static TranslationHelper _helper = null;
        private Dictionary<string, Translation> _translations = new Dictionary<string, Translation>();
        private Translation _current = null;

        public static TranslationHelper Instance()
        {
            if (_helper == null)
                _helper = new TranslationHelper();

            return _helper;
        }

        public static string Tr_(string text)
        {
            return Instance().Tr(text);
        }

        private TranslationHelper()
        {
        }

        public bool LoadExtra(string dir)
        {
            if (_current == null)
                return false;

            StreamReader reader = null;
            FileStream fs = null;
            try
            {
                fs = new FileStream(dir + "/" +  _current.lang + ".tr", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                reader = new StreamReader(fs);

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] data = line.Split(
                      new[] { "==>" },
                      StringSplitOptions.None
                       );
                    if (data.Length < 2)
                        continue;
                    if (!_current._data.ContainsKey(data[0]))
                        _current._data.Add(data[0], data[1]);
                    else
                        _current._data[data[0]] = data[1];
                }
                return true;
            }

            catch (IOException e)
            {
                // handle exception and/or rethrow
            }
            return false;
        }

        public bool LoadTranslation(string lang)
        {
            if (_translations.ContainsKey(lang))
                return true;
            StreamReader reader = null;
            FileStream fs = null;
            try
            {
                Translation tr;
                if (_translations.ContainsKey(lang))
                    tr = _translations[lang];
                else
                {
                    tr = new Translation();
                    tr.lang = lang;
                }
                fs = new FileStream(lang + ".tr", FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                reader = new StreamReader(fs);

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                     string [] data = line.Split(
                       new[] { "==>"},
                       StringSplitOptions.None
                        );
                    if (data.Length < 2)
                        continue;
                    if (!tr._data.ContainsKey(data[0]))
                        tr._data.Add(data[0], data[1]);
                    else
                        tr._data[data[0]] = data[1];
                }

                _translations.Add(lang, tr);
                return true;
            }

            catch (IOException e)
            {
                if (!_translations.ContainsKey(lang))
                {
                    Translation tr = new Translation();
                    tr.lang = lang;
                    _translations.Add(lang, tr);
                }
            }
            return false;
        }

        public bool SetTranslation(string lang)
        {
            if (_translations.ContainsKey(lang))
            {
                _current = _translations[lang];
                TranslationChanged?.Invoke();
                return true;
            }
            _current = null;
            return false;
        }
        public string lang()
        {
            if (_current == null)
            {
                return "en";
            }
            return _current.lang;
        }

        public string Tr(string text)
        {
            if (_current == null)
            {
                //AddMismatchedTranslation(text, "");
                return text;
            }
            if (text == "")
                return text;

            if (text == null)
                return "";

            if (_current._data.ContainsKey(text))
            {
                string result = _current._data[text];
                if (result == "")
                    return text;
                return result;
            }
            AddMismatchedTranslation(text, _current.lang);
            return text;
        }

        private void AddMismatchedTranslation(string text, string lang)
        {
            if (lang == "en")
                return;
            //Console.WriteLine(text + "==>" + lang);
        }
    }
}
