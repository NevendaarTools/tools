﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Stack : DataBlock
    {
        public enum Order : Int16
        {
            Normal = 1,
            Stand = 2,
            Guard = 3,
            Attack = 4,
            Roam = 7,
            Move = 8,
            Defend = 9,
            Berserk = 10
        };
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x10, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidStack@@\0"));
        }

        public static string Type()
        {
            return "Stack";
        }

        private byte[] _header = Header();
        public string _objId = "KC0001";
        public string[] _Unit = { "000000", "000000", "000000", "000000", "000000", "000000" };
        public int[] _Pos = { -1, -1, -1, -1, -1, -1 };

        public List<string> _items = new List<string>();

        public string _srctmplId = "000000";
        public string _leaderId;
        public bool _leaderAliv = true;
        public int _posX = 0;
        public int _posY = 0;
        public int _morale = 0;
        public int _move = 20;
        public int _facing = 0;
        public string _banner = "000000";
        public string _tome = "000000";
        public string _battle1 = "000000";
        public string _battle2 = "000000";
        public string _artifact1 = "000000";
        public string _artifact2 = "000000";
        public string _boots = "000000";
        public string _owner = "PL0000";
        public string _inside = "000000";//FT00000
        public string _subrace = "SR0000";
        public bool _invisible = false;
        public bool _aiIgnore = false;
        public int _upgcount = 0;
        public int _order = (int)Order.Normal;
        public string _orderTarg = "000000";
        public int _aiorder = (int)Order.Stand;
        public string _aiordertar = "000000";
        public int _aipriority = 3;
        public int _creatLvl = 1;
        public int _nbbattle = 0;


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");

            for (int i = 0; i < 6; ++i)
            {
                _Unit[i] = Helper.ReadDefaultString(ref array, ref index, "UNIT_" + i.ToString());
            }

            for (int i = 0; i < 6; ++i)
            {
                _Pos[i] = Helper.ReadDefaultInt(ref array, ref index, "POS_" + i.ToString());
            }

            int itemsCount = Helper.ReadDefaultInt(ref array, ref index, _objId);
            for (int i = 0; i < itemsCount; ++i)
            {
                _items.Add(Helper.ReadDefaultString(ref array, ref index, "ITEM_ID"));
            }

            _srctmplId = Helper.ReadDefaultString(ref array, ref index, "SRCTMPL_ID");
            _leaderId = Helper.ReadDefaultString(ref array, ref index, "LEADER_ID");
            _leaderAliv = Helper.ReadBool(ref array, ref index, "LEADR_ALIV");
            _posX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _posY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _morale = Helper.ReadDefaultInt(ref array, ref index, "MORALE");
            _move = Helper.ReadDefaultInt(ref array, ref index, "MOVE");
            _facing = Helper.ReadDefaultInt(ref array, ref index, "FACING");
            _banner = Helper.ReadDefaultString(ref array, ref index, "BANNER");
            _tome = Helper.ReadDefaultString(ref array, ref index, "TOME");
            _battle1 = Helper.ReadDefaultString(ref array, ref index, "BATTLE1");
            _battle2 = Helper.ReadDefaultString(ref array, ref index, "BATTLE2");
            _artifact1 = Helper.ReadDefaultString(ref array, ref index, "ARTIFACT1");
            _artifact2 = Helper.ReadDefaultString(ref array, ref index, "ARTIFACT2");
            _boots = Helper.ReadDefaultString(ref array, ref index, "BOOTS");
            _owner = Helper.ReadDefaultString(ref array, ref index, "OWNER");
            _inside = Helper.ReadDefaultString(ref array, ref index, "INSIDE");
            _subrace = Helper.ReadDefaultString(ref array, ref index, "SUBRACE");
            _invisible = Helper.ReadBool(ref array, ref index, "INVISIBLE");
            _aiIgnore = Helper.ReadBool(ref array, ref index, "AI_IGNORE");
            _upgcount = Helper.ReadDefaultInt(ref array, ref index, "UPGCOUNT");
            _order = Helper.ReadDefaultInt(ref array, ref index, "ORDER");
            _orderTarg = Helper.ReadDefaultString(ref array, ref index, "ORDER_TARG");
            _aiorder = Helper.ReadDefaultInt(ref array, ref index, "AIORDER");
            _aiordertar = Helper.ReadDefaultString(ref array, ref index, "AIORDERTAR");
            _aipriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");
            _creatLvl = Helper.ReadDefaultInt(ref array, ref index, "CREAT_LVL");
            _nbbattle = Helper.ReadDefaultInt(ref array, ref index, "NBBATTLE");


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("GROUP_ID", version, _objId);

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultString("UNIT_" + i.ToString(), version, _Unit[i]);
            }

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultInt("POS_" + i.ToString(), _Pos[i]);
            }

            int itemsCount = _items.Count;
            buffer.WriteDefaultInt(version + _objId, itemsCount);
            for (int i = 0; i < itemsCount; ++i)
            {
                buffer.WriteDefaultString("ITEM_ID", version, _items[i]);
            }

            buffer.WriteDefaultString("STACK_ID", version, _objId);
            buffer.WriteDefaultString("SRCTMPL_ID", version, _srctmplId);
            buffer.WriteDefaultString("LEADER_ID", version, _leaderId);
            buffer.WriteBool("LEADR_ALIV", _leaderAliv);
            buffer.WriteDefaultInt("POS_X", _posX);
            buffer.WriteDefaultInt("POS_Y", _posY);
            buffer.WriteDefaultInt("MORALE", _morale);
            buffer.WriteDefaultInt("MOVE", _move);
            buffer.WriteDefaultInt("FACING", _facing);
            buffer.WriteDefaultString("BANNER", version, _banner);
            buffer.WriteDefaultString("TOME", version, _tome);
            buffer.WriteDefaultString("BATTLE1", version, _battle1);
            buffer.WriteDefaultString("BATTLE2", version, _battle2);
            buffer.WriteDefaultString("ARTIFACT1", version, _artifact1);
            buffer.WriteDefaultString("ARTIFACT2", version, _artifact2);
            buffer.WriteDefaultString("BOOTS", version, _boots);
            buffer.WriteDefaultString("OWNER", version, _owner);
            buffer.WriteDefaultString("INSIDE", version, _inside);
            buffer.WriteDefaultString("SUBRACE", version, _subrace);
            buffer.WriteBool("INVISIBLE", _invisible);
            buffer.WriteBool("AI_IGNORE", _aiIgnore);
            buffer.WriteDefaultInt("UPGCOUNT", _upgcount);
            buffer.WriteDefaultInt("ORDER", _order);
            buffer.WriteDefaultString("ORDER_TARG", version, _orderTarg);
            buffer.WriteDefaultInt("AIORDER", _aiorder);
            buffer.WriteDefaultString("AIORDERTAR", version, _aiordertar);
            buffer.WriteDefaultInt("AIPRIORITY", _aipriority);
            buffer.WriteDefaultInt("CREAT_LVL", _creatLvl);
            buffer.WriteDefaultInt("NBBATTLE", _nbbattle);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}