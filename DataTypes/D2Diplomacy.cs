using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Diplomacy : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x14, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidDiplomacy@@\0"));
        }

        public static string Type()
        {
            return "Diplomacy";
        }

        private byte[] _header = Header();
        public string _objId = "DP0000";

        public class DiplomacyEntry
        {
            public int _Race1;
            public int _Race2;
            public int _Relation;

        }
        public List<DiplomacyEntry> _Entries = new List<DiplomacyEntry>();


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                DiplomacyEntry elem = new DiplomacyEntry();
                elem._Race1 = Helper.ReadDefaultInt(ref array, ref index, "RACE_1");
                elem._Race2 = Helper.ReadDefaultInt(ref array, ref index, "RACE_2");
                elem._Relation = Helper.ReadDefaultInt(ref array, ref index, "RELATION");
                _Entries.Add(elem);
            } 


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();

            buffer.WriteDefaultInt(version + _objId, _Entries.Count);
            for (int i = 0; i < _Entries.Count; ++i) 
            {
                DiplomacyEntry elem = _Entries[i];
                buffer.WriteDefaultInt( "RACE_1", elem._Race1);
                buffer.WriteDefaultInt( "RACE_2", elem._Race2);
                buffer.WriteDefaultInt( "RELATION", elem._Relation);
            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}