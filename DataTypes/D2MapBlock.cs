﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2MapBlock : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x17, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidgardMapBlock@@\0"));
        }

        public static string Type()
        {
            return "MapBlock";
        }

        public enum TileType
        {
            Empire = 1,
            Dwarfs = 2,
            Demons = 3,
            Undeads = 4,
            Neutrals = 5,
            Elves = 6,
            Tree = 8,
            EmpireTree = Empire | Tree,
            DwarfsTree = Dwarfs | Tree,
            DemonsTree = Demons | Tree,
            UndeadsTree = Undeads | Tree,
            NeutralsTree = Neutrals | Tree,
            ElvesTree = Elves | Tree,
            Water = 29,
            Mountain = 37
        }

        private byte[] _header = Header();
        public string _objId = "MB0000";

        public int[] _blockData = new int[32];
        int _dataSize = 128;

        public int X()
        {
            string tmp = _objId.Substring(4);
            return (int)Convert.ToUInt32(tmp, 16);
        }
        public int Y()
        {
            string tmp = _objId.Substring(2, 2);
            return (int)Convert.ToUInt32(tmp, 16);
        }

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _dataSize = Helper.ReadInt(ref array, ref index, "BLOCKDATA", 4);
            for(int i = 0; i < _blockData.Length; ++i)
            {
                _blockData[i] = Helper.ReadInt(ref array, ref index, 4, false);
            }
            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("BLOCKID", version, _objId);
            buffer.WriteInt("BLOCKDATA", _dataSize, 4);
            for (int i = 0; i < _blockData.Length; ++i)
            {
                buffer.WriteInt(_blockData[i], 4, false);
            }
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
