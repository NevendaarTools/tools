using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Player : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x11, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidPlayer@@\0"));
        }

        public static string Type()
        {
            return "Player";
        }

        private byte[] _header = Header();
        public string _objId = "PL0000";

        public string _nameTxt= "";
        public string _descTxt = "";
        public string _lordId = "G000LR0013";
        public string _raceId = "G000RR0004";
        public string _fogId = "FG0000";
        public string _knownId = "KS0001";
        public string _buildsId = "PB0001";
        public int _face = 1;
        public int _qtyBreaks = 0;
        public string _bank = "G0500:R0000:Y0000:E0000:W0000:B0000";
        public bool _isHuman = false;
        public string _spellBank = "G0000:R0000:Y0000:E0000:W0000:B0000";
        public int _attitude = 1;
        public int _researT = 0;
        public int _constrT = 0;
        public string _spy1 = "000000";
        public string _spy2 = "000000";
        public string _spy3 = "000000";
        public string _captBy = "000000";
        public bool _alwaysai = false;
        public string _exmapid1 = "000000";
        public int _exmapturn1 = 0;
        public string _exmapid2 = "000000";
        public int _exmapturn2 = 0;
        public string _exmapid3 = "000000";
        public int _exmapturn3 = 0;


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _nameTxt = Helper.ReadStringWithLenght(ref array, ref index, "NAME_TXT");
            _descTxt = Helper.ReadStringWithLenght(ref array, ref index, "DESC_TXT");
            _lordId = Helper.ReadStringWithLenght(ref array, ref index, "LORD_ID");
            _raceId = Helper.ReadStringWithLenght(ref array, ref index, "RACE_ID");
            _fogId = Helper.ReadDefaultString(ref array, ref index, "FOG_ID");
            _knownId = Helper.ReadDefaultString(ref array, ref index, "KNOWN_ID");
            _buildsId = Helper.ReadDefaultString(ref array, ref index, "BUILDS_ID");
            _face = Helper.ReadDefaultInt(ref array, ref index, "FACE");
            _qtyBreaks = Helper.ReadDefaultInt(ref array, ref index, "QTY_BREAKS");
            _bank = Helper.ReadStringWithLenght(ref array, ref index, "BANK");
            _isHuman = Helper.ReadBool(ref array, ref index, "IS_HUMAN");
            _spellBank = Helper.ReadStringWithLenght(ref array, ref index, "SPELL_BANK");
            _attitude = Helper.ReadDefaultInt(ref array, ref index, "ATTITUDE");
            _researT = Helper.ReadDefaultInt(ref array, ref index, "RESEAR_T");
            _constrT = Helper.ReadDefaultInt(ref array, ref index, "CONSTR_T");
            _spy1 = Helper.ReadDefaultString(ref array, ref index, "SPY_1");
            _spy2 = Helper.ReadDefaultString(ref array, ref index, "SPY_2");
            _spy3 = Helper.ReadDefaultString(ref array, ref index, "SPY_3");
            _captBy = Helper.ReadDefaultString(ref array, ref index, "CAPT_BY");
            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset == 0)
                _alwaysai = Helper.ReadBool(ref array, ref index, "ALWAYSAI");
            if (map.Header._MapType == "D2EESFISIG")
            {
                _exmapid1 = Helper.ReadDefaultString(ref array, ref index, "EXMAPID1");
                _exmapturn1 = Helper.ReadDefaultInt(ref array, ref index, "EXMAPTURN1");
                _exmapid2 = Helper.ReadDefaultString(ref array, ref index, "EXMAPID2");
                _exmapturn2 = Helper.ReadDefaultInt(ref array, ref index, "EXMAPTURN2");
                _exmapid3 = Helper.ReadDefaultString(ref array, ref index, "EXMAPID3");
                _exmapturn3 = Helper.ReadDefaultInt(ref array, ref index, "EXMAPTURN3");
            }


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("PLAYER_ID", version, _objId);

            buffer.WriteString( "NAME_TXT", _nameTxt);
            buffer.WriteString( "DESC_TXT", _descTxt);
            buffer.WriteString( "LORD_ID", _lordId);
            buffer.WriteString( "RACE_ID", _raceId);
            buffer.WriteDefaultString( "FOG_ID", version, _fogId);
            buffer.WriteDefaultString( "KNOWN_ID", version, _knownId);
            buffer.WriteDefaultString( "BUILDS_ID", version, _buildsId);
            buffer.WriteDefaultInt( "FACE", _face);
            buffer.WriteDefaultInt( "QTY_BREAKS", _qtyBreaks);
            buffer.WriteString( "BANK", _bank);
            buffer.WriteBool( "IS_HUMAN", _isHuman);
            buffer.WriteString( "SPELL_BANK", _spellBank);
            buffer.WriteDefaultInt( "ATTITUDE", _attitude);
            buffer.WriteDefaultInt( "RESEAR_T", _researT);
            buffer.WriteDefaultInt( "CONSTR_T", _constrT);
            buffer.WriteDefaultString( "SPY_1", version, _spy1);
            buffer.WriteDefaultString( "SPY_2", version, _spy2);
            buffer.WriteDefaultString( "SPY_3", version, _spy3);
            buffer.WriteDefaultString( "CAPT_BY", version, _captBy);

            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset == 0)
                buffer.WriteBool("ALWAYSAI", _alwaysai);
            if (map.Header._MapType == "D2EESFISIG")
            {
                buffer.WriteDefaultString("EXMAPID1", version, _exmapid1);
                buffer.WriteDefaultInt("EXMAPTURN1", _exmapturn1);
                buffer.WriteDefaultString("EXMAPID2", version, _exmapid2);
                buffer.WriteDefaultInt("EXMAPTURN2", _exmapturn2);
                buffer.WriteDefaultString("EXMAPID3", version, _exmapid3);
                buffer.WriteDefaultInt("EXMAPTURN3", _exmapturn3);
            }
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}