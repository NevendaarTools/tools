﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools
{
    public class D2TalismanCharges : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x1a, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidTalismanCharges@@\0"));
        }

        public static string Type()
        {
            return "TalismanCharges";
        }

        private byte[] _header = Header();
        public string _ObjId = "TC0000";

        public struct TalismanChargeInfo
        {
            public string talismanId;
            public int count;
        }

        public List<TalismanChargeInfo> _Charges = new List<TalismanChargeInfo>();

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _ObjId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");

            int itemsCount = Helper.ReadDefaultInt(ref array, ref index, _ObjId);
            for (int i = 0; i < itemsCount; ++i)
            {
                string talId = Helper.ReadDefaultString(ref array, ref index, "ID_TALIS");
                int chargesCount = Helper.ReadDefaultInt(ref array, ref index, "CHARGES");
                _Charges.Add(new TalismanChargeInfo() { talismanId = talId, count = chargesCount });
            }

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _ObjId);
            buffer.WriteBegin();
            buffer.WriteDefaultInt(version + _ObjId, _Charges.Count);
            for (int i = 0; i < _Charges.Count; ++i)
            {
                buffer.WriteDefaultString("ID_TALIS", version, _Charges[i].talismanId);
                buffer.WriteDefaultInt("CHARGES", _Charges[i].count);
            }
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _ObjId; }
    }
}
