﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NevendaarTools
{
    public interface DataBlock
    {
        void Read(ref byte[] array, ref int index, ref MapModel map);
        byte[] Data(ref MapModel map);
        string BlockType();
        string ObjId();
    }

    public interface DataBlockFactory
    {
        DataBlock Read(ref byte[] array, ref int index, ref MapModel map);
        string Type();
        Object Convert(DataBlock block);
    }

    public class ByteDataBlock: DataBlock
    {
        
        public void Read(ref byte[] array, ref int index, ref MapModel map) 
        {
            byte[] numbers = Helper.stringToByteArray("ENDOBJECT\0");
            ReadRange(ref array, ref index, Helper.ByteSearch(array, numbers, index) + 10);
        }

        public void ReadRange(ref byte[] array, ref int index, int lastIndex)
        {
            _Data = new byte[lastIndex - index];
            Array.Copy(array, index, _Data, 0, lastIndex - index);
            index = lastIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            return _Data;
        }

        private byte[] _Data;

        public string BlockType() { return "unknown"; }
        public string ObjId() { return "unknown"; }
    }

    public class TagDataBlock : DataBlock
    {
        private string _Type;
        private string _ObjId;

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int startTypeIndex = Helper.ByteSearch(array, Helper.stringToByteArray(".?"), index);
            int endTypeIndex = Helper.ByteSearch(array, Helper.stringToByteArray("@@\0"), startTypeIndex);

            _Type = Helper.ReadString(array, startTypeIndex, endTypeIndex - startTypeIndex);
            _ObjId = Helper.ReadString(array, endTypeIndex + 13, 10);//@@\0OBJ_ID\b\0\0\0

            byte[] numbers = Helper.stringToByteArray("ENDOBJECT\0");
            ReadRange(ref array, ref index, Helper.ByteSearch(array, numbers, index) + 10);
        }

        public void ReadRange(ref byte[] array, ref int index, int lastIndex)
        {
            _Data = new byte[lastIndex - index];
            Array.Copy(array, index, _Data, 0, lastIndex - index);
            index = lastIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            return _Data;
        }

        private byte[] _Data;

        public string BlockType() { return _Type; }
        public string ObjId() { return _ObjId; }
    }

    public class Factory<T> : DataBlockFactory where T : class, DataBlock, new()
    {
        public DataBlock Read(ref byte[] array, ref int index, ref MapModel map)
        {
            T dataBlock = new T();
            dataBlock.Read(ref array, ref index, ref map);
            return dataBlock;
        }

        public Object Convert(DataBlock block)
        {
            return block as T;
        }

        public string Type() 
        {
            T t = new T();
            return t.BlockType();
        }
    }

    //byte[] result = buffer.GetData();
    //bool equal = localData.SequenceEqual(result);
    //if (!equal)
    //{
    //    for (int i = 0; i<result.Length; ++i)
    //    {
    //        if (result[i] != localData[i])
    //        {
    //            File.WriteAllBytes("Compare_before.sg", localData);
    //            File.WriteAllBytes("Compare_after.sg", result);
    //            int gg = 0; gg++;
    //        }
    //    }
    //}
}
