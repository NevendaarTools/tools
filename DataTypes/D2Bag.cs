﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools
{
    public class D2Bag : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x0e, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidBag@@\0"));
        }

        public static string Type()
        {
            return "Bag";
        }

        private byte[] _header = Header();
        public string _objId = "BG0000";

        public int _posX;
        public int _posY;
        public int _image;
        public int _aipriority;
        public List<string> _items = new List<string>();

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _posX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _posY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _image = Helper.ReadDefaultInt(ref array, ref index, "IMAGE");
            _aipriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");

            int count = Helper.ReadDefaultInt(ref array, ref index, _objId);
            for (int i = 0; i < count; ++i)
                _items.Add(Helper.ReadDefaultString(ref array, ref index, "ITEM_ID"));

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("BAG_ID", version, _objId);
            buffer.WriteDefaultInt("POS_X", _posX);
            buffer.WriteDefaultInt("POS_Y", _posY);
            buffer.WriteDefaultInt("IMAGE", _image);
            buffer.WriteDefaultInt("AIPRIORITY", _aipriority);
            buffer.WriteDefaultInt(version + _objId, _items.Count);
            for (int i = 0; i < _items.Count; ++i)
            {
                buffer.WriteDefaultString("ITEM_ID", version, _items[i]);
            }
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
