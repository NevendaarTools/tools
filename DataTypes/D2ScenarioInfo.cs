using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2ScenarioInfo : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x14, 0, 0, 0 }, Helper.stringToByteArray(".?AVCScenarioInfo@@\0"));
        }

        public static string Type()
        {
            return "ScenarioInfo";
        }

        private byte[] _header = Header();
        public string _objId = "IF0000";

        public string _campaign = "C000CC0001";
        public bool _sourceM = false;
        public int _qtyCities = 0;
        public string _name = "";
        public string _desc = "";
        public string _briefing = "";
        public string _debunkw = "";
        public string _debunkw2 = "";
        public string _debunkw3 = "";
        public string _debunkw4 = "";
        public string _debunkw5 = "";
        public string _debunkl = "";
        public string _brieflong1 = "";
        public string _brieflong2 = "";
        public string _brieflong3 = "";
        public string _brieflong4 = "";
        public string _brieflong5 = "";
        public bool _ignore = false;
        public int _curTurn = 0;
        public int _maxUnit = 5;
        public int _maxSpell = 5;
        public int _maxLeader = 99;
        public int _maxCity = 5;
        public int _mapSize = 144;
        public int _diffscen = 3;
        public int _diffgame = 1;
        public string _creator="";
        public int[] _players = new int[13] {4, 99,99,99,99,99,99,99,99,99,99,99,99}; 
        public int _suggLvl = 1;
        public int _mapSeed = 1679258820;


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _campaign = Helper.ReadStringWithLenght(ref array, ref index, "CAMPAIGN");
            _sourceM = Helper.ReadBool(ref array, ref index, "SOURCE_M");
            _qtyCities = Helper.ReadDefaultInt(ref array, ref index, "QTY_CITIES");
            _name = Helper.ReadStringWithLenght(ref array, ref index, "NAME");
            _desc = Helper.ReadStringWithLenght(ref array, ref index, "DESC");
            _briefing = Helper.ReadStringWithLenght(ref array, ref index, "BRIEFING");
            _debunkw = Helper.ReadStringWithLenght(ref array, ref index, "DEBUNKW");
            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset != 34)
            {
                _debunkw2 = Helper.ReadStringWithLenght(ref array, ref index, "DEBUNKW2");
                _debunkw3 = Helper.ReadStringWithLenght(ref array, ref index, "DEBUNKW3");
                _debunkw4 = Helper.ReadStringWithLenght(ref array, ref index, "DEBUNKW4");
                _debunkw5 = Helper.ReadStringWithLenght(ref array, ref index, "DEBUNKW5");
            }
            _debunkl = Helper.ReadStringWithLenght(ref array, ref index, "DEBUNKL");
            _brieflong1 = Helper.ReadStringWithLenght(ref array, ref index, "BRIEFLONG1");
            _brieflong2 = Helper.ReadStringWithLenght(ref array, ref index, "BRIEFLONG2");
            _brieflong3 = Helper.ReadStringWithLenght(ref array, ref index, "BRIEFLONG3");
            _brieflong4 = Helper.ReadStringWithLenght(ref array, ref index, "BRIEFLONG4");
            _brieflong5 = Helper.ReadStringWithLenght(ref array, ref index, "BRIEFLONG5");
            _ignore = Helper.ReadBool(ref array, ref index, "O");
            _curTurn = Helper.ReadDefaultInt(ref array, ref index, "CUR_TURN");
            _maxUnit = Helper.ReadDefaultInt(ref array, ref index, "MAX_UNIT");
            _maxSpell = Helper.ReadDefaultInt(ref array, ref index, "MAX_SPELL");
            _maxLeader = Helper.ReadDefaultInt(ref array, ref index, "MAX_LEADER");
            _maxCity = Helper.ReadDefaultInt(ref array, ref index, "MAX_CITY");
            _mapSize = Helper.ReadDefaultInt(ref array, ref index, "MAP_SIZE");
            _diffscen = Helper.ReadDefaultInt(ref array, ref index, "DIFFSCEN");
            _diffgame = Helper.ReadDefaultInt(ref array, ref index, "DIFFGAME");
            _creator = Helper.ReadStringWithLenght(ref array, ref index, "CREATOR");
            for(int i = 0; i < 13; ++i)
                _players[i] = Helper.ReadDefaultInt(ref array, ref index, "PLAYER_" + (i+1).ToString());

            _suggLvl = Helper.ReadDefaultInt(ref array, ref index, "SUGG_LVL");
            _mapSeed = Helper.ReadDefaultInt(ref array, ref index, "MAP_SEED");


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString( "INFO_ID", version, _objId);
            buffer.WriteString( "CAMPAIGN", _campaign);
            buffer.WriteBool( "SOURCE_M", _sourceM);
            buffer.WriteDefaultInt( "QTY_CITIES", _qtyCities);
            buffer.WriteString( "NAME", _name);
            buffer.WriteString( "DESC", _desc);
            buffer.WriteString( "BRIEFING", _briefing);
            buffer.WriteString( "DEBUNKW", _debunkw);
            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset != 34)
            {
                buffer.WriteString("DEBUNKW2", _debunkw2);
                buffer.WriteString("DEBUNKW3", _debunkw3);
                buffer.WriteString("DEBUNKW4", _debunkw4);
                buffer.WriteString("DEBUNKW5", _debunkw5);
            }
            buffer.WriteString( "DEBUNKL", _debunkl);
            buffer.WriteString( "BRIEFLONG1", _brieflong1);
            buffer.WriteString( "BRIEFLONG2", _brieflong2);
            buffer.WriteString( "BRIEFLONG3", _brieflong3);
            buffer.WriteString( "BRIEFLONG4", _brieflong4);
            buffer.WriteString( "BRIEFLONG5", _brieflong5);
            buffer.WriteBool( "O", _ignore);
            buffer.WriteDefaultInt( "CUR_TURN", _curTurn);
            buffer.WriteDefaultInt( "MAX_UNIT", _maxUnit);
            buffer.WriteDefaultInt( "MAX_SPELL", _maxSpell);
            buffer.WriteDefaultInt( "MAX_LEADER", _maxLeader);
            buffer.WriteDefaultInt( "MAX_CITY", _maxCity);
            buffer.WriteDefaultInt( "MAP_SIZE", _mapSize);
            buffer.WriteDefaultInt( "DIFFSCEN", _diffscen);
            buffer.WriteDefaultInt( "DIFFGAME", _diffgame);
            buffer.WriteString( "CREATOR", _creator);

            for (int i = 0; i < 13; ++i)
                buffer.WriteDefaultInt("PLAYER_" + (i + 1).ToString(), _players[i]);

            buffer.WriteDefaultInt( "SUGG_LVL", _suggLvl);
            buffer.WriteDefaultInt( "MAP_SEED", _mapSeed);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}