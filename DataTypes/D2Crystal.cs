using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Crystal : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x12, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidCrystal@@\0"));
        }

        public static string Type()
        {
            return "Crystal";
        }

        private byte[] _header = Header();
        public string _ObjId = "СК0000";
        public int _Resource = 0;
        public int _PosX = 0;
        public int _PosY = 0;
        public int _Aipriority = 3;


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_ObjId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _Resource = Helper.ReadDefaultInt(ref array, ref index, "RESOURCE");
            _PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _Aipriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _ObjId);
            buffer.WriteBegin();
            buffer.WriteDefaultString( "CRYSTAL_ID", version, _ObjId);
            buffer.WriteDefaultInt( "RESOURCE",  _Resource);
            buffer.WriteDefaultInt("POS_X", _PosX);
            buffer.WriteDefaultInt("POS_Y", _PosY);
            buffer.WriteDefaultInt( "AIPRIORITY",  _Aipriority);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _ObjId; }
    }
}