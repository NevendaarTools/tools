﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using NevendaarTools.DataTypes;

namespace NevendaarTools
{
    public class D2Village : D2City, DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x12, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidVillage@@\0"));
        }

        public static string Type()
        {
            return "Village";
        }

        private byte[] _Header = Header();
        public string _ObjId = "FT0001";
        public string _Name = "";
        public string _Desk = "";
        public string _Owner = "PL0000";
        public string _SubRace = "SR0000";
        public string _Stack = "000000";
        public int _PosX = 0;
        public int _PosY = 0;
        public string[] _Unit = { "000000", "000000", "000000", "000000", "000000", "000000" };
        public int[] _Pos = { -1, -1, -1, -1, -1, -1 };
        public List<string> _Items = new List<string>();
        public int _AIPriority = 3;
        public string _ProtectB = "000000";
        public int _RegenB = 0;
        public int _Morale = 0;
        public int _GrowthT = 0;
        public int _Size = 1;
        public bool _POUN = false;
        public bool _POHE = false;
        public bool _POHU = false;
        public bool _PODW = false;
        public bool _POEL = false;

        public int _RiotT = 0;

        public bool HasUnits()
        {
            for (int i = 0; i < 6; ++i)
            {
                if (_Unit[i] != "000000")
                    return true;
            }
            return false;
        }

        void DataBlock.Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _ObjId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _Name  = Helper.ReadStringWithLenght(ref array, ref index, "NAME_TXT");
            _Desk  = Helper.ReadStringWithLenght(ref array, ref index, "DESC_TXT");
            _Owner = Helper.ReadDefaultString(ref array, ref index, "OWNER");
            _SubRace = Helper.ReadDefaultString(ref array, ref index, "SUBRACE");
            _Stack = Helper.ReadDefaultString(ref array, ref index, "STACK");
            _PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");

            for (int i = 0; i < 6; ++i)
            {
                _Unit[i] = Helper.ReadDefaultString(ref array, ref index, "UNIT_" + i.ToString());
            }

            for(int i = 0; i < 6; ++i)
            {
                _Pos[i] = Helper.ReadDefaultInt(ref array, ref index, "POS_" + i.ToString());
            }

            int itemsCount = Helper.ReadDefaultInt(ref array, ref index, _ObjId);
            for (int i = 0; i < itemsCount; ++i)
            {
                _Items.Add(Helper.ReadDefaultString(ref array, ref index, "ITEM_ID"));
            }
            
            _AIPriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");
            _ProtectB = Helper.ReadDefaultString(ref array, ref index, "PROTECT_B");
            _RegenB = Helper.ReadDefaultInt(ref array, ref index, "REGEN_B");
            _Morale = Helper.ReadDefaultInt(ref array, ref index, "MORALE");
            _GrowthT = Helper.ReadDefaultInt(ref array, ref index, "GROWTH_T");
            _Size = Helper.ReadDefaultInt(ref array, ref index, "SIZE");

            _POUN = Helper.ReadBool(ref array, ref index, "P_O_UN");
            _POHE = Helper.ReadBool(ref array, ref index, "P_O_HE");
            _POHU = Helper.ReadBool(ref array, ref index, "P_O_HU");
            _PODW = Helper.ReadBool(ref array, ref index, "P_O_DW");

            if(map.Header._MapType == "D2EESFISIG")
                _POEL = Helper.ReadBool(ref array, ref index, "P_O_EL");

            _RiotT = Helper.ReadDefaultInt(ref array, ref index, "RIOT_T");

            index = endIndex;
        }

        byte[] DataBlock.Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_Header);
            buffer.WriteDefaultString("OBJ_ID", version, _ObjId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("CITY_ID", version, _ObjId);
            buffer.WriteString("NAME_TXT", _Name);
            buffer.WriteString("DESC_TXT", _Desk);
            buffer.WriteDefaultString("OWNER", version, _Owner);
            buffer.WriteDefaultString("SUBRACE", version, _SubRace);
            buffer.WriteDefaultString("STACK", version, _Stack);
            buffer.WriteDefaultInt("POS_X", _PosX);
            buffer.WriteDefaultInt("POS_Y", _PosY);
            buffer.WriteDefaultString("GROUP_ID", version, _ObjId);

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultString("UNIT_" + i.ToString(), version,_Unit[i]);
            }

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultInt("POS_" + i.ToString(), _Pos[i]);
            }

            int itemsCount = _Items.Count;
            buffer.WriteDefaultInt(version + _ObjId, itemsCount);
            for (int i = 0; i < itemsCount; ++i)
            {
                buffer.WriteDefaultString("ITEM_ID", version, _Items[i]);
            }

            buffer.WriteDefaultInt("AIPRIORITY", _AIPriority);
            buffer.WriteDefaultString("PROTECT_B", version, _ProtectB);

            buffer.WriteDefaultInt("REGEN_B", _RegenB);
            buffer.WriteDefaultInt("MORALE", _Morale);
            buffer.WriteDefaultInt("GROWTH_T", _GrowthT);
            buffer.WriteDefaultInt("SIZE", _Size);

            buffer.WriteBool("P_O_UN", _POUN);
            buffer.WriteBool("P_O_HE", _POHE);
            buffer.WriteBool("P_O_HU", _POHU);
            buffer.WriteBool("P_O_DW", _PODW);
            if (map.Header._MapType == "D2EESFISIG")
                buffer.WriteBool("P_O_EL", _POEL);


            buffer.WriteDefaultInt("RIOT_T", _RiotT);
            buffer.WriteEnd();

            return buffer.GetData();
        }

        string DataBlock.BlockType() { return Type(); }
        string DataBlock.ObjId() { return _ObjId; }
    }
}
