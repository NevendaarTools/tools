using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2SpellCast : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x14, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidSpellCast@@\0"));
        }

        public static string Type()
        {
            return "SpellCast";
        }

        private byte[] _header = Header();
        public string _objId = "ST0000";

        public int _Unknownint1 = 0;
        public int _Unknownint2 = 0;


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            index += 10;
            _Unknownint1 = Helper.ReadDefaultInt(ref array, ref index, _objId);
            _Unknownint2 = Helper.ReadDefaultInt(ref array, ref index, _objId);


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultInt(version + _objId,  _Unknownint1);
            buffer.WriteDefaultInt(version + _objId,  _Unknownint2);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}