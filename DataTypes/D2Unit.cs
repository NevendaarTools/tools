﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace NevendaarTools
{
    public class D2Unit : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x0f, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidUnit@@\0"));
        }

        public static string Type()
        {
            return "Unit";
        }

        public class TransInfo
        {
            public string _orig;
            public bool _keepHP;
            public int _XP;
            public int _HPBefore;
            public int _HPBeforeMax;
        };
        private byte[] _Header = Header();
        public string _ObjId = "UN0002";
        public string _Type = "G000UU5130";
        public int _Level = 1;
        public List<string> _Mods = new List<string>();
        public int _Creation = 0;
        public string _Name = "";
        public TransInfo _Transf;

        public bool _DynLevel = false;
        public int _HP = 65;
        public int _XP = 0;

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);
            _Mods.Clear();
            _ObjId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _Type = Helper.ReadStringWithLenght(ref array, ref index, "TYPE");
            _Level = Helper.ReadDefaultInt(ref array, ref index, "LEVEL");

            int count = Helper.ReadDefaultInt(ref array, ref index, _ObjId);
            for (int i = 0; i < count; ++i)
                _Mods.Add(Helper.ReadStringWithLenght(ref array, ref index, "MODIF_ID"));

            _Creation = Helper.ReadDefaultInt(ref array, ref index, "CREATION");
            _Name = Helper.ReadStringWithLenght(ref array, ref index, "NAME_TXT");
            bool transf = Helper.ReadBool(ref array, ref index, "TRANSF");
            if (transf)
            {
                _Transf = new TransInfo();
                _Transf._orig = Helper.ReadStringWithLenght(ref array, ref index, "ORIGTYPEID");
                _Transf._keepHP = Helper.ReadBool(ref array, ref index, "KEEP_HP");
                _Transf._XP = Helper.ReadDefaultInt(ref array, ref index, "ORIG_XP");
                _Transf._HPBefore = Helper.ReadDefaultInt(ref array, ref index, "HP_BEFORE");
                _Transf._HPBeforeMax = Helper.ReadDefaultInt(ref array, ref index, "HP_BEF_MAX");
            }

            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset != 34)
                _DynLevel = Helper.ReadBool(ref array, ref index, "DYNLEVEL");

            _HP = Helper.ReadDefaultInt(ref array, ref index, "HP");
            _XP = Helper.ReadDefaultInt(ref array, ref index, "XP");

           index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_Header);
            buffer.WriteDefaultString("OBJ_ID", version, _ObjId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("UNIT_ID", version, _ObjId);
            buffer.WriteString("TYPE", _Type);
            buffer.WriteDefaultInt("LEVEL", _Level);
            buffer.WriteDefaultInt(version + _ObjId, _Mods.Count);
            for (int i = 0; i < _Mods.Count; ++i)
            {
                buffer.WriteString("MODIF_ID", _Mods[i]);
            }
            buffer.WriteDefaultInt("CREATION", _Creation);
            buffer.WriteString("NAME_TXT", _Name);
            if (_Transf == null)
            {
                buffer.WriteBool("TRANSF", false);
            }
            else
            {
                buffer.WriteBool("TRANSF", true);
                buffer.WriteString("ORIGTYPEID", _Transf._orig);
                buffer.WriteBool("KEEP_HP", _Transf._keepHP);
                buffer.WriteDefaultInt("ORIG_XP", _Transf._XP);
                buffer.WriteDefaultInt("HP_BEFORE", _Transf._HPBefore);
                buffer.WriteDefaultInt("HP_BEF_MAX", _Transf._HPBeforeMax);
            }
            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset != 34)
                buffer.WriteBool("DYNLEVEL", _DynLevel);

            buffer.WriteDefaultInt("HP", _HP);
            buffer.WriteDefaultInt("XP", _XP);
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _ObjId; }
    }
}
