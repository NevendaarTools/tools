﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools
{
    public class D2Item : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x0f, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidItem@@\0"));
        }

        public static string Type()
        {
            return "Item";
        }

        private byte[] _header = Header();
        public string _objId = "IM0001";
        public string _type = "G000IG2001";

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.ByteSearch(array, Helper.stringToByteArray("ENDOBJECT\0"), index) + 10;
            
            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _type = Helper.ReadStringWithLenght(ref array, ref index, "ITEM_TYPE");

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("ITEM_ID", version, _objId);
            buffer.WriteString("ITEM_TYPE", _type);
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
