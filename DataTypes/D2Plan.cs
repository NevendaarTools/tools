﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevendaarTools
{ 
    public class D2Plan : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x13, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidgardPlan@@\0"));
        }

        public static string Type()
        {
            return "Plan";
        }

        public class PlanElement
        {
            public int _posX;
            public int _posY;
            public string _id;
        }

        private byte[] _header = Header();
        public string _objId = "PN0000";
        public int _mapSize = 0;
        public List<PlanElement> _elements = new List<PlanElement>();

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _mapSize = Helper.ReadDefaultInt(ref array, ref index, _objId);
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId);
            for (int i = 0; i < count; ++i)
            {
                PlanElement elem = new PlanElement();
                elem._posX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
                elem._posY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
                elem._id = Helper.ReadDefaultString(ref array, ref index, "ELEMENT");

                _elements.Add(elem);
            }

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultInt(version + _objId, _mapSize);
            buffer.WriteDefaultInt(version + _objId, _elements.Count);
            for (int i = 0; i < _elements.Count; ++i)
            {
                buffer.WriteDefaultInt("POS_X", _elements[i]._posX);
                buffer.WriteDefaultInt("POS_Y", _elements[i]._posY);
                buffer.WriteDefaultString("ELEMENT", version, _elements[i]._id);
            }
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
