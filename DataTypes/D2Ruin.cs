﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools
{
    public class D2Ruin : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x0f, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidRuin@@\0"));
        }

        public static string Type()
        {
            return "Ruin";
        }

        private byte[] _header = Header();
        public string _objId = "RU0000";
        public string _name = "";
        public string _desc = "";
        public int _image = 0;
        public int _posX = 0;
        public int _posY = 0;
        public string _cash = "G0100:R0000:Y0000:E0000:W0000:B0000";
        public string _item = "000000";
        public string _looter = "000000";
        public int _AIPriority = 3;
        public string[] _unit = { "000000", "000000", "000000", "000000", "000000", "000000" };
        public int[] _pos = { -1, -1, -1, -1, -1, -1 };

        List<string> _visiters = new List<string>();
        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _name = Helper.ReadStringWithLenght(ref array, ref index, "TITLE");
            _desc = Helper.ReadStringWithLenght(ref array, ref index, "DESC");
            _image = Helper.ReadDefaultInt(ref array, ref index, "IMAGE");
            _posX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _posY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _cash = Helper.ReadStringWithLenght(ref array, ref index, "CASH");
            _item = Helper.ReadStringWithLenght(ref array, ref index, "ITEM");
            _looter = Helper.ReadDefaultString(ref array, ref index, "LOOTER");
            _AIPriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId);

            for (int i = 0; i < count; ++i)
                _visiters.Add(Helper.ReadDefaultString(ref array, ref index, "VISITER"));

            for (int i = 0; i < 6; ++i)
            {
                _unit[i] = Helper.ReadDefaultString(ref array, ref index, "UNIT_" + i.ToString());
            }

            for (int i = 0; i < 6; ++i)
            {
                _pos[i] = Helper.ReadDefaultInt(ref array, ref index, "POS_" + i.ToString());
            }

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();
            
            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("RUIN_ID", version, _objId);
            buffer.WriteString("TITLE", _name);
            buffer.WriteString("DESC", _desc);
            buffer.WriteDefaultInt("IMAGE", _image);
            buffer.WriteDefaultInt("POS_X", _posX);
            buffer.WriteDefaultInt("POS_Y", _posY);
            buffer.WriteString("CASH", _cash);
            buffer.WriteDefaultString("ITEM", "", _item);
            buffer.WriteDefaultString("LOOTER", version, _looter);
            buffer.WriteDefaultInt("AIPRIORITY", _AIPriority);            
            buffer.WriteDefaultInt(version + _objId, _visiters.Count);

            for (int i = 0; i < _visiters.Count; ++i)
            {
                buffer.WriteDefaultString("RUIN_ID", version, _objId);
                buffer.WriteDefaultString("VISITER", version, _visiters[i]);
            }
            buffer.WriteDefaultString("GROUP_ID", version, _objId);


            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultString("UNIT_" + i.ToString(), version, _unit[i]);
            }

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultInt("POS_" + i.ToString(), _pos[i]);
            }

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
