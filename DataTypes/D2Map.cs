﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Map : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x12, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidgardMap@@\0"));
        }

        public static string Type()
        {
            return "Map";
        }

        private byte[] _header = Header();
        public string _objId = "MP0000";

        public int _mapSize = 48;

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _mapSize = Helper.ReadDefaultInt(ref array, ref index, _objId);
            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultInt(version + _objId, _mapSize);
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
