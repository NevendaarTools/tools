using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Mercs : D2Site, DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x14, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidSiteMercs@@\0"));
        }

        public static string Type()
        {
            return "Mercs";
        }

        private byte[] _header = Header();
        public string _objId = "SI0000";

        public int _ImgIso = 0;
        public string _ImgIntf = "";
        public string _Title = "";
        public string _Desc = "";
        public int _PosX = 0;
        public int _PosY = 0;
        public string _Visiter = "000000";
        public int _AiPriority = 3;

        public class MercsUnitEntry
        {
            public string _UnitId;
            public int _UnitLevel;
            public bool _UnitUniq = false;

        } 
        public List <MercsUnitEntry> _Units = new List<MercsUnitEntry>();

        public List <SiteVisitor> _Visitors = new List<SiteVisitor>();


        void DataBlock.Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _ImgIso = Helper.ReadDefaultInt(ref array, ref index, "IMG_ISO");
            _ImgIntf = Helper.ReadStringWithLenght(ref array, ref index, "IMG_INTF");
            _Title = Helper.ReadStringWithLenght(ref array, ref index, "TXT_TITLE");
            _Desc = Helper.ReadStringWithLenght(ref array, ref index, "TXT_DESC");
            _PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _Visiter = Helper.ReadDefaultString(ref array, ref index, "VISITER");
            _AiPriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");

            int count = Helper.ReadDefaultInt(ref array, ref index, "QTY_UNIT");
            for (int i = 0; i < count; ++i) 
            {
                MercsUnitEntry elem = new MercsUnitEntry();
                elem._UnitId = Helper.ReadStringWithLenght(ref array, ref index, "UNIT_ID");
                elem._UnitLevel = Helper.ReadDefaultInt(ref array, ref index, "UNIT_LEVEL");
                elem._UnitUniq = Helper.ReadBool(ref array, ref index, "UNIT_UNIQ");
                _Units.Add(elem);

            } 

            count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                SiteVisitor elem = new SiteVisitor();
                elem._SiteId = Helper.ReadDefaultString(ref array, ref index, "SITE_ID");
                elem._Visiter = Helper.ReadDefaultString(ref array, ref index, "VISITER");
                _Visitors.Add(elem);

            } 


            index = endIndex;
        }

        byte[] DataBlock.Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("SITE_ID", version, _objId);
            buffer.WriteDefaultInt("IMG_ISO", _ImgIso);
            buffer.WriteString("IMG_INTF", _ImgIntf);
            buffer.WriteString("TXT_TITLE", _Title);
            buffer.WriteString("TXT_DESC", _Desc);
            buffer.WriteDefaultInt("POS_X", _PosX);
            buffer.WriteDefaultInt("POS_Y", _PosY);
            buffer.WriteDefaultString("VISITER", _Visiter);
            buffer.WriteDefaultInt("AIPRIORITY", _AiPriority);
            buffer.WriteDefaultInt( "QTY_UNIT",  _Units.Count);
            for (int i = 0; i < _Units.Count; ++i) 
            {
                MercsUnitEntry elem = _Units[i];
                buffer.WriteString( "UNIT_ID", elem._UnitId);
                buffer.WriteDefaultInt( "UNIT_LEVEL", elem._UnitLevel);
                buffer.WriteBool( "UNIT_UNIQ", elem._UnitUniq);

            } 
            buffer.WriteDefaultInt(version + _objId, _Visitors.Count);
            for (int i = 0; i < _Visitors.Count; ++i) 
            {
                SiteVisitor elem = _Visitors[i];
                buffer.WriteDefaultString( "SITE_ID", version, elem._SiteId);
                buffer.WriteDefaultString( "VISITER", version, elem._Visiter);

            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        string DataBlock.BlockType() { return Type(); }
        string DataBlock.ObjId() { return _objId; }
    }
}