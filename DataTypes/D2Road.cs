﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    class D2Road : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x0f, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidRoad@@\0"));
        }

        public static string Type()
        {
            return "Road";
        }

        private byte[] _header = Header();
        public string _ObjId = "RA0000";
        public int roadIndex = 0;
        public int _PosX = 0;
        public int _PosY = 0;
        public int varValue = 3;


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _ObjId = Helper.ReadDefaultString(ref array, ref index, "ROAD_ID");
            roadIndex = Helper.ReadDefaultInt(ref array, ref index, "INDEX");
            varValue = Helper.ReadDefaultInt(ref array, ref index, "VAR");
            _PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _ObjId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("ROAD_ID", version, _ObjId);
            buffer.WriteDefaultInt("INDEX", roadIndex);
            buffer.WriteDefaultInt("VAR", varValue);
            buffer.WriteDefaultInt("POS_X", _PosX);
            buffer.WriteDefaultInt("POS_Y", _PosY);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _ObjId; }
    }
}
