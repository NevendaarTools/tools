﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2City : DataBlock
    {
        string DataBlock.BlockType()
        {
            throw new NotImplementedException();
        }

        byte[] DataBlock.Data(ref MapModel map)
        {
            throw new NotImplementedException();
        }

        string DataBlock.ObjId()
        {
            throw new NotImplementedException();
        }

        void DataBlock.Read(ref byte[] array, ref int index, ref MapModel map)
        {
            throw new NotImplementedException();
        }

        public class SiteVisitor
        {
            public string _SiteId;
            public string _Visiter;
        }
    }
    public class D2Capital : D2City, DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x0f, 0, 0, 0 }, Helper.stringToByteArray(".?AVCCapital@@\0"));
        }

        public static string Type()
        {
            return "Capital";
        }

        private byte[] _header = Header();
        public string _objId = "FT0000";

        public string _NameTxt = "";
        public string _DescTxt = "";
        public string _Owner;
        public string _Subrace;
        public string _Stack;
        public int _PosX = 0;
        public int _PosY = 0;
        public string[] _Unit = { "000000", "000000", "000000", "000000", "000000", "000000" };
        public int[] _Pos = { -1, -1, -1, -1, -1, -1 };

        public List<string> _Items = new List<string>();
        public int _Aipriority = 3;


        void DataBlock.Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _NameTxt = Helper.ReadStringWithLenght(ref array, ref index, "NAME_TXT");
            _DescTxt = Helper.ReadStringWithLenght(ref array, ref index, "DESC_TXT");
            _Owner = Helper.ReadDefaultString(ref array, ref index, "OWNER");
            _Subrace = Helper.ReadDefaultString(ref array, ref index, "SUBRACE");
            _Stack = Helper.ReadDefaultString(ref array, ref index, "STACK");
            _PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");

            for (int i = 0; i < 6; ++i)
            {
                _Unit[i] = Helper.ReadDefaultString(ref array, ref index, "UNIT_" + i.ToString());
            }

            for (int i = 0; i < 6; ++i)
            {
                _Pos[i] = Helper.ReadDefaultInt(ref array, ref index, "POS_" + i.ToString());
            }

            int count = Helper.ReadDefaultInt(ref array, ref index, _objId);
            for (int i = 0; i < count; ++i)
            {
                _Items.Add(Helper.ReadDefaultString(ref array, ref index, "ITEM_ID"));
            }
            _Aipriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");


            index = endIndex;
        }

        byte[] DataBlock.Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("CITY_ID", version, _objId);
            buffer.WriteString("NAME_TXT", _NameTxt);
            buffer.WriteString("DESC_TXT", _DescTxt);
            buffer.WriteDefaultString("OWNER", version, _Owner);
            buffer.WriteDefaultString("SUBRACE", version, _Subrace);
            buffer.WriteDefaultString("STACK", version, _Stack);
            buffer.WriteDefaultInt("POS_X", _PosX);
            buffer.WriteDefaultInt("POS_Y",  _PosY);
            buffer.WriteDefaultString("GROUP_ID", version, _objId);

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultString("UNIT_" + i.ToString(), version, _Unit[i]);
            }

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultInt("POS_" + i.ToString(), _Pos[i]);
            }

            int itemsCount = _Items.Count;
            buffer.WriteDefaultInt(version + _objId, itemsCount);
            for (int i = 0; i < itemsCount; ++i)
            {
                buffer.WriteDefaultString("ITEM_ID", version, _Items[i]);
            }

            buffer.WriteDefaultInt("AIPRIORITY", _Aipriority);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        string DataBlock.BlockType() { return Type(); }
        string DataBlock.ObjId() { return _objId; }
    }
}