using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2QuestLog : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x13, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidQuestLog@@\0"));
        }

        public static string Type()
        {
            return "QuestLog";
        }

        private byte[] _header = Header();
        public string _objId = "QL0000";

        public class MidQuestLogSequence
        {
            public string _IdPlayer;
            public int _SeqNum;
            public int _CurTurn;
            public int _Type;
            public string _IdEvent;
            public int _EffNum;

        } 
        List <MidQuestLogSequence> _Sequences = new List<MidQuestLogSequence>();


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                MidQuestLogSequence elem = new MidQuestLogSequence();
                elem._IdPlayer = Helper.ReadDefaultString(ref array, ref index, "ID_PLAYER");
                elem._SeqNum = Helper.ReadDefaultInt(ref array, ref index, "SEQ_NUM");
                elem._CurTurn = Helper.ReadDefaultInt(ref array, ref index, "CUR_TURN");
                elem._Type = Helper.ReadDefaultInt(ref array, ref index, "TYPE");
                elem._IdEvent = Helper.ReadDefaultString(ref array, ref index, "ID_EVENT");
                elem._EffNum = Helper.ReadDefaultInt(ref array, ref index, "EFF_NUM");
                _Sequences.Add(elem);
            } 


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
                        
            buffer.WriteDefaultInt(version + _objId, _Sequences.Count);
            for (int i = 0; i < _Sequences.Count; ++i) 
            {
                MidQuestLogSequence elem = _Sequences[i];
                buffer.WriteDefaultString( "ID_PLAYER", version, elem._IdPlayer);
                buffer.WriteDefaultInt( "SEQ_NUM", elem._SeqNum);
                buffer.WriteDefaultInt( "CUR_TURN", elem._CurTurn);
                buffer.WriteDefaultInt( "TYPE", elem._Type);
                buffer.WriteDefaultString( "ID_EVENT", version, elem._IdEvent);
                buffer.WriteDefaultInt( "EFF_NUM", elem._EffNum);
            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}