using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2StackDestroyed : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x19, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidStackDestroyed@@\0"));
        }

        public static string Type()
        {
            return "StackDestroyed";
        }

        private byte[] _header = Header();
        public string _objId = "SD0000";

        
        public class MidStackDestroyedEntry
        {
            public string _IdStack;
            public string _IdKiller;
            public string _SrctmplId;

        } 
        List <MidStackDestroyedEntry> _Stacks = new List<MidStackDestroyedEntry>();


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");

            int count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                MidStackDestroyedEntry elem = new MidStackDestroyedEntry();
                elem._IdStack = Helper.ReadDefaultString(ref array, ref index, "ID_STACK");
                elem._IdKiller = Helper.ReadDefaultString(ref array, ref index, "ID_KILLER");
                elem._SrctmplId = Helper.ReadDefaultString(ref array, ref index, "SRCTMPL_ID");
                _Stacks.Add(elem);

            } 


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();

            buffer.WriteDefaultInt(version + _objId, _Stacks.Count);
            for (int i = 0; i < _Stacks.Count; ++i) 
            {
                MidStackDestroyedEntry elem = _Stacks[i];
                buffer.WriteDefaultString( "ID_STACK", version, elem._IdStack);
                buffer.WriteDefaultString( "ID_KILLER", version, elem._IdKiller);
                buffer.WriteDefaultString( "SRCTMPL_ID", version, elem._SrctmplId);
            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}