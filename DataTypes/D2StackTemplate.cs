﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools
{
    public class D2StackTemplate : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x18, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidStackTemplate@@\0"));
        }

        public static string Type()
        {
            return "StackTemplate";
        }

        public class UnitTemplateMod
        {
            public int _UnitNum = 0;
            public string _Modid = "G000UM9001";
        }

        private byte[] _Header = Header();
        public string _ObjId = "RU0000";
        public string _Owner = "PL001";
        public string _Leader = "G000UU7505";
        public int _LeaderLvl = 1;
        public string _Name = "Name";
        public string _OrderTarg = "G000000000";
        public string _SubRace = "SR0000";
        public int _Order = 2;
        public string[] _Unit = { "G000UU7505", "G000000000", "G000000000", "G000000000", "G000000000", "G000000000" };
        public int[] _UnitLvl = { 1, 0, 0, 0, 0, 0};
        public int[] _Pos = { 0, -1, -1, -1, -1, -1 };
        public bool _UseFacing = false;
        public int _Facing = 0;
        public List<UnitTemplateMod> _Mods = new List<UnitTemplateMod>();
        public int _AIPriority = 3;

        public string ModsToString()
        {
            string result = _Name + "\n";
            for (int i = 0; i < 6; i++)
            {
                result += i.ToString();
                if (_Pos[i] != -1)
                {
                    result += " " + _Unit[_Pos[i]];
                    foreach (UnitTemplateMod mod in _Mods)
                    {
                        if (mod._UnitNum == i)
                            result += " " + mod._Modid;
                    }
                }
                result += "\n";
            }
            return result;
        }

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _ObjId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _Owner = Helper.ReadDefaultString(ref array, ref index, "OWNER");
            _Leader = Helper.ReadStringWithLenght(ref array, ref index, "LEADER");
            _LeaderLvl = Helper.ReadDefaultInt(ref array, ref index, "LEADER_LVL");
            _Name = Helper.ReadStringWithLenght(ref array, ref index, "NAME_TXT");
            _OrderTarg = Helper.ReadStringWithLenght(ref array, ref index, "ORDER_TARG");
            _SubRace = Helper.ReadDefaultString(ref array, ref index, "SUBRACE");
            _Order = Helper.ReadDefaultInt(ref array, ref index, "ORDER");

            for (int i = 0; i < 6; ++i)
            {
                _Unit[i] = Helper.ReadStringWithLenght(ref array, ref index, "UNIT_" + i.ToString());
                _UnitLvl[i] = Helper.ReadDefaultInt(ref array, ref index, "UNIT_" + i.ToString() + "_LVL");
            }

            for (int i = 0; i < 6; ++i)
            {
                _Pos[i] = Helper.ReadDefaultInt(ref array, ref index, "POS_" + i.ToString());
            }

            _UseFacing = Helper.ReadBool(ref array, ref index, "USE_FACING");
            _Facing = Helper.ReadDefaultInt(ref array, ref index, "FACING");

            int modCount = Helper.ReadDefaultInt(ref array, ref index, _ObjId);
            for (int i = 0; i < modCount; ++i)
            {
                _Mods.Add(new UnitTemplateMod
                {
                    _UnitNum = Helper.ReadDefaultInt(ref array, ref index, "UNIT_POS"),
                    _Modid = Helper.ReadStringWithLenght(ref array, ref index, "MODIF_ID")
                });
            }

            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset == 0)
               _AIPriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_Header);
            buffer.WriteDefaultString("OBJ_ID", version, _ObjId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("ID", version, _ObjId);
            buffer.WriteDefaultString("OWNER", version, _Owner);
            buffer.WriteString("LEADER", _Leader);
            buffer.WriteDefaultInt("LEADER_LVL", _LeaderLvl);
            buffer.WriteString("NAME_TXT", _Name);
            buffer.WriteString("ORDER_TARG", _OrderTarg);
            buffer.WriteDefaultString("SUBRACE", version, _SubRace);
            buffer.WriteDefaultInt("ORDER", _Order);

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteString("UNIT_" + i.ToString(), _Unit[i]);
                buffer.WriteDefaultInt("UNIT_" + i.ToString() + "_LVL", _UnitLvl[i]);
            }

            for (int i = 0; i < 6; ++i)
            {
                buffer.WriteDefaultInt("POS_" + i.ToString(), _Pos[i]);
            }

            buffer.WriteBool("USE_FACING", _UseFacing);
            buffer.WriteDefaultInt("FACING", _Facing);
            buffer.WriteSimpleString(version + _ObjId);
            buffer.WriteDefaultInt("", _Mods.Count);

            for (int i = 0; i < _Mods.Count; ++i)
            {
                buffer.WriteDefaultInt("UNIT_POS", _Mods[i]._UnitNum);
                buffer.WriteString("MODIF_ID", _Mods[i]._Modid);
            }

            if (map.Header._MapType == "D2EESFISIG" || map.Header._offset == 0)
                buffer.WriteDefaultInt("AIPRIORITY", _AIPriority);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _ObjId; }
    }
}
