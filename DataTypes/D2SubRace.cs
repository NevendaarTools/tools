using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2SubRace : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x12, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidSubRace@@\0"));
        }

        public static string Type()
        {
            return "SubRace";
        }

        private byte[] _header = Header();
        public string _objId = "SR0000";

        public int _Subrace;
        public string _PlayerId;
        public int _Number = 0;
        public string _NameTxt = "";
        public int _Banner;


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "SUBRACE_ID");
            _Subrace = Helper.ReadDefaultInt(ref array, ref index, "SUBRACE");
            _PlayerId = Helper.ReadDefaultString(ref array, ref index, "PLAYER_ID");
            _Number = Helper.ReadDefaultInt(ref array, ref index, "NUMBER");
            _NameTxt = Helper.ReadStringWithLenght(ref array, ref index, "NAME_TXT");
            _Banner = Helper.ReadDefaultInt(ref array, ref index, "BANNER");


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString( "SUBRACE_ID", version,  _objId);
            buffer.WriteDefaultInt( "SUBRACE",  _Subrace);
            buffer.WriteDefaultString( "PLAYER_ID", version,  _PlayerId);
            buffer.WriteDefaultInt( "NUMBER",  _Number);
            buffer.WriteString( "NAME_TXT", _NameTxt);
            buffer.WriteDefaultInt( "BANNER",  _Banner);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}