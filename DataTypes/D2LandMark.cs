using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2LandMark : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x13, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidLandmark@@\0"));
        }

        public static string Type()
        {
            return "LandMark";
        }

        private byte[] _header = Header();
        public string _objId = "MM0000";

        public string _Type;
        public int _posX;
        public int _posY;
        public string _DescTxt = "";


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _Type = Helper.ReadStringWithLenght(ref array, ref index, "TYPE");
            _posX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _posY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");

            if ((map.Header._MapType == "D2EESFISIG" || map.Header._offset == 0) && index < endIndex - 10)
                _DescTxt = Helper.ReadStringWithLenght(ref array, ref index, "DESC_TXT");

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString( "LMARK_ID", version, _objId);
            buffer.WriteString( "TYPE", _Type);
            buffer.WriteDefaultInt("POS_X", _posX);
            buffer.WriteDefaultInt("POS_Y", _posY);
            if ((map.Header._MapType == "D2EESFISIG" || map.Header._offset == 0) && _DescTxt != "")
                buffer.WriteString( "DESC_TXT", _DescTxt);

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}