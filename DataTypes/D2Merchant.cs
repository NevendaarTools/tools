using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Merchant : D2Site, DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x17, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidSiteMerchant@@\0"));
        }

        public static string Type()
        {
            return "Merchant";
        }

        private byte[] _header = Header();
        public string _objId = "SI0000";

        public int _ImgIso = 0;
        public string _ImgIntf = "";
        public string _Title = "";
        public string _Desc = "";
        public int _PosX = 0;
        public int _PosY = 0;
        public string _Visiter = "000000";
        public int _AiPriority = 3;

        public bool _BuyArmor = true;
        public bool _BuyJewel = true;
        public bool _BuyWeapon = true;
        public bool _BuyBanner = true;
        public bool _BuyPotion = true;
        public bool _BuyScroll = true;
        public bool _BuyWand = true;
        public bool _BuyValue = true;

        public class MerchantItemEntry
        {
            public string _ItemId;
            public int _ItemCount;

        }
        public List<MerchantItemEntry> _Items = new List<MerchantItemEntry>();
        public bool _Mission;

        public List<SiteVisitor> _Unknown = new List<SiteVisitor>();


        void DataBlock.Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _ImgIso = Helper.ReadDefaultInt(ref array, ref index, "IMG_ISO");
            _ImgIntf = Helper.ReadStringWithLenght(ref array, ref index, "IMG_INTF");
            _Title = Helper.ReadStringWithLenght(ref array, ref index, "TXT_TITLE");
            _Desc = Helper.ReadStringWithLenght(ref array, ref index, "TXT_DESC");
            _PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _Visiter = Helper.ReadDefaultString(ref array, ref index, "VISITER");
            _AiPriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");

            _BuyArmor = Helper.ReadBool(ref array, ref index, "BUY_ARMOR");
            _BuyJewel = Helper.ReadBool(ref array, ref index, "BUY_JEWEL");
            _BuyWeapon = Helper.ReadBool(ref array, ref index, "BUY_WEAPON");
            _BuyBanner = Helper.ReadBool(ref array, ref index, "BUY_BANNER");
            _BuyPotion = Helper.ReadBool(ref array, ref index, "BUY_POTION");
            _BuyScroll = Helper.ReadBool(ref array, ref index, "BUY_SCROLL");
            _BuyWand = Helper.ReadBool(ref array, ref index, "BUY_WAND");
            _BuyValue = Helper.ReadBool(ref array, ref index, "BUY_VALUE");
            int count = Helper.ReadDefaultInt(ref array, ref index, "QTY_ITEM"); 
            for (int i = 0; i < count; ++i) 
            {
                MerchantItemEntry elem = new MerchantItemEntry();
                elem._ItemId = Helper.ReadStringWithLenght(ref array, ref index, "ITEM_ID");
                elem._ItemCount = Helper.ReadDefaultInt(ref array, ref index, "ITEM_COUNT");
                _Items.Add(elem);

            } 
            _Mission = Helper.ReadBool(ref array, ref index, "MISSION");
            count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                SiteVisitor elem = new SiteVisitor();
                elem._SiteId = Helper.ReadDefaultString(ref array, ref index, "SITE_ID");
                elem._Visiter = Helper.ReadDefaultString(ref array, ref index, "VISITER");
                _Unknown.Add(elem);

            } 


            index = endIndex;
        }

        byte[] DataBlock.Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();

            buffer.WriteDefaultString("SITE_ID", version, _objId);
            buffer.WriteDefaultInt("IMG_ISO", _ImgIso);
            buffer.WriteString("IMG_INTF", _ImgIntf);
            buffer.WriteString("TXT_TITLE", _Title);
            buffer.WriteString("TXT_DESC", _Desc);
            buffer.WriteDefaultInt("POS_X", _PosX);
            buffer.WriteDefaultInt("POS_Y", _PosY);
            buffer.WriteDefaultString("VISITER", _Visiter);
            buffer.WriteDefaultInt("AIPRIORITY", _AiPriority);

            buffer.WriteBool( "BUY_ARMOR",  _BuyArmor);
            buffer.WriteBool( "BUY_JEWEL",  _BuyJewel);
            buffer.WriteBool( "BUY_WEAPON",  _BuyWeapon);
            buffer.WriteBool( "BUY_BANNER",  _BuyBanner);
            buffer.WriteBool( "BUY_POTION",  _BuyPotion);
            buffer.WriteBool( "BUY_SCROLL",  _BuyScroll);
            buffer.WriteBool( "BUY_WAND",  _BuyWand);
            buffer.WriteBool( "BUY_VALUE",  _BuyValue);
            buffer.WriteDefaultInt( "QTY_ITEM", _Items.Count);
            for (int i = 0; i < _Items.Count; ++i) 
            {
                MerchantItemEntry elem = _Items[i];
                buffer.WriteString( "ITEM_ID", elem._ItemId);
                buffer.WriteDefaultInt( "ITEM_COUNT", elem._ItemCount);

            } 
            buffer.WriteBool( "MISSION",  _Mission);
            buffer.WriteDefaultInt(version + _objId, _Unknown.Count);
            for (int i = 0; i < _Unknown.Count; ++i) 
            {
                SiteVisitor elem = _Unknown[i];
                buffer.WriteDefaultString( "SITE_ID", elem._SiteId);
                buffer.WriteDefaultString( "VISITER", version, elem._Visiter);

            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        string DataBlock.BlockType() { return Type(); }
        string DataBlock.ObjId() { return _objId; }
    }
}