using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2PlayerBuildings : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x17, 0, 0, 0 }, Helper.stringToByteArray(".?AVCPlayerBuildings@@\0"));
        }

        public static string Type()
        {
            return "PlayerBuildings";
        }

        private byte[] _header = Header();
        public string _objId = "PB0001";

        public List <string> _buildings = new List<string>();


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");

            int count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                _buildings.Add(Helper.ReadStringWithLenght(ref array, ref index, "BUILD_ID"));
            } 


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
                        
            buffer.WriteDefaultInt(version + _objId, _buildings.Count);
            for (int i = 0; i < _buildings.Count; ++i) 
            {
                buffer.WriteString("BUILD_ID", _buildings[i]);
            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}