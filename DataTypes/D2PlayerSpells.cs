using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2PlayerSpells : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x19, 0, 0, 0 }, Helper.stringToByteArray(".?AVCPlayerKnownSpells@@\0"));
        }

        public static string Type()
        {
            return "PlayerSpells";
        }

        private byte[] _header = Header();
        public string _objId = "KS0000";
     
        List <string> _spells = new List<string>();


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                _spells.Add(Helper.ReadStringWithLenght(ref array, ref index, "SPELL_ID"));
            } 

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
                        
            buffer.WriteDefaultInt(version + _objId, _spells.Count);
            for (int i = 0; i < _spells.Count; ++i) 
            {
                buffer.WriteString("SPELL_ID", _spells[i]);
            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}