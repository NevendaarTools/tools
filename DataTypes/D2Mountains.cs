﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Mountains : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x14, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidMountains@@\0"));
        }

        public static string Type()
        {
            return "Mountains";
        }

        private byte[] _header = Header();
        public string _objId = "ML0000";

        public class Mountain
        {
            public int id;
            public int sizeX;
            public int sizeY;
            public int x;
            public int y;
            public int image = 0;
            public int race = 0;
        }
        public List<Mountain> _mountains = new List<Mountain>();

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            int dataSize = Helper.ReadInt(ref array, ref index, _objId, 4);
            for (int i = 0; i < dataSize; ++i)
            {
                Mountain mountain = new Mountain();
                mountain.id = Helper.ReadDefaultInt(ref array, ref index, "ID_MOUNT");
                mountain.sizeX = Helper.ReadDefaultInt(ref array, ref index, "SIZE_X");
                mountain.sizeY = Helper.ReadDefaultInt(ref array, ref index, "SIZE_Y");
                mountain.x = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
                mountain.y = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
                mountain.image = Helper.ReadDefaultInt(ref array, ref index, "IMAGE");
                mountain.race = Helper.ReadDefaultInt(ref array, ref index, "RACE");

                _mountains.Add(mountain);
            }
            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteInt(version + _objId, _mountains.Count, 4);
            foreach(Mountain mountain in _mountains)
            {
                buffer.WriteDefaultInt("ID_MOUNT", mountain.id);
                buffer.WriteDefaultInt("SIZE_X", mountain.sizeX);
                buffer.WriteDefaultInt("SIZE_Y", mountain.sizeY);
                buffer.WriteDefaultInt("POS_X", mountain.x);
                buffer.WriteDefaultInt("POS_Y", mountain.y);
                buffer.WriteDefaultInt("IMAGE", mountain.image);
                buffer.WriteDefaultInt("RACE", mountain.race);
            }

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
