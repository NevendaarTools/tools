using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2TurnSummary : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x13, 0, 0, 0 }, Helper.stringToByteArray(".?AVCTurnSummary@@\0"));
        }

        public static string Type()
        {
            return "TurnSummary";
        }

        private byte[] _header = Header();
        public string _objId = "TS0000";

        public class TurnSummaryEntry
        {
            public string _IdPlayer;
            public int _Type;
            public int _PosX;
            public int _PosY;

            public TurnSummaryEntryType0 _entry0;
            public TurnSummaryEntryType1 _entry1;
            public TurnSummaryEntryType2 _entry2;
            public TurnSummaryEntryType3 _entry3;
            public TurnSummaryEntryType4 _entry4;
            public TurnSummaryEntryType5 _entry5;
            public TurnSummaryEntryType6 _entry6;

            public class TurnSummaryEntryType0
            {
                public string _IdPlayer2;
                public string _IdSpell;
                public string _IdStkD;
                public string _StrStkD;
            }

            public class TurnSummaryEntryType1
            {
                public string _IdPlayer2;
                public string _IdStkA;
                public string _StrStkA;
                public string _IdStkD;
                public string _StrStkD;
            } 
            public class TurnSummaryEntryType2
            {
                public string _IdStkD;
                public string _StrStkD;
            }
            public class TurnSummaryEntryType3
            {
                public string _IdCity;
            }

            public class TurnSummaryEntryType4
            {

            }
            public class TurnSummaryEntryType5
            {
                public string _IdStkD;
                public string _StrStkD;
            }
            public class TurnSummaryEntryType6
            {
                public string _IdStkD;
                public string _StrStkD;
                public bool _Asquire;
            }
        } 
        public List <TurnSummaryEntry> _Summary = new List<TurnSummaryEntry>();


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
                        
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId);
            for (int i = 0; i < count; ++i)
            {
                TurnSummaryEntry elem = new TurnSummaryEntry();
                elem._IdPlayer = Helper.ReadDefaultString(ref array, ref index, "ID_PLAYER");
                elem._Type = Helper.ReadDefaultInt(ref array, ref index, "TYPE");
                elem._PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
                elem._PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
                if (elem._Type == 0)
                {
                    elem._entry0 = new TurnSummaryEntry.TurnSummaryEntryType0();

                    elem._entry0._IdPlayer2 = Helper.ReadDefaultString(ref array, ref index, "ID_PLAYER2");
                    elem._entry0._IdSpell = Helper.ReadStringWithLenght(ref array, ref index, "ID_SPELL");
                    elem._entry0._IdStkD = Helper.ReadDefaultString(ref array, ref index, "ID_STK_D");
                    elem._entry0._StrStkD = Helper.ReadStringWithLenght(ref array, ref index, "STR_STK_D");
                }
                else if (elem._Type == 1)
                {
                    elem._entry1 = new TurnSummaryEntry.TurnSummaryEntryType1();
                    
                    elem._entry1._IdPlayer2 = Helper.ReadDefaultString(ref array, ref index, "ID_PLAYER2");
                    elem._entry1._IdStkA = Helper.ReadDefaultString(ref array, ref index, "ID_STK_A");
                    elem._entry1._StrStkA = Helper.ReadStringWithLenght(ref array, ref index, "STR_STK_A");
                    elem._entry1._IdStkD = Helper.ReadDefaultString(ref array, ref index, "ID_STK_D");
                    elem._entry1._StrStkD = Helper.ReadStringWithLenght(ref array, ref index, "STR_STK_D");
                }
                else if (elem._Type == 2)
                {
                    elem._entry2 = new TurnSummaryEntry.TurnSummaryEntryType2();

                    elem._entry2._IdStkD = Helper.ReadDefaultString(ref array, ref index, "ID_STK_D");
                    elem._entry2._StrStkD = Helper.ReadStringWithLenght(ref array, ref index, "STR_STK_D");
                }
                else if (elem._Type == 3)
                {
                    elem._entry3 = new TurnSummaryEntry.TurnSummaryEntryType3();

                    elem._entry3._IdCity = Helper.ReadDefaultString(ref array, ref index, "ID_CITY");
                }
                else  if (elem._Type == 4)
                {
                    elem._entry4 = new TurnSummaryEntry.TurnSummaryEntryType4();
                }
                else if (elem._Type == 5)
                {
                    elem._entry5 = new TurnSummaryEntry.TurnSummaryEntryType5();

                    elem._entry5._IdStkD = Helper.ReadDefaultString(ref array, ref index, "ID_STK_D");
                    elem._entry5._StrStkD = Helper.ReadStringWithLenght(ref array, ref index, "STR_STK_D");
                }
                else if (elem._Type == 6)
                {
                    elem._entry6 = new TurnSummaryEntry.TurnSummaryEntryType6();
                    elem._entry6._IdStkD = Helper.ReadDefaultString(ref array, ref index, "ID_STK_D");
                    elem._entry6._StrStkD = Helper.ReadStringWithLenght(ref array, ref index, "STR_STK_D");
                    elem._entry6._Asquire = Helper.ReadBool(ref array, ref index, "ACQUIRE");
                }

                _Summary.Add(elem);
            }

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
                       
            buffer.WriteDefaultInt(version + _objId, _Summary.Count);
            for (int i = 0; i < _Summary.Count; ++i) 
            {
                TurnSummaryEntry elem = _Summary[i];
                buffer.WriteDefaultString( "ID_PLAYER", version, elem._IdPlayer);
                buffer.WriteDefaultInt( "TYPE", elem._Type);
                buffer.WriteDefaultInt("POS_X", elem._PosX);
                buffer.WriteDefaultInt("POS_Y", elem._PosY);

                if (elem._Type == 0)
                {
                    buffer.WriteDefaultString("ID_PLAYER2", version, elem._entry0._IdPlayer2);
                    buffer.WriteString("ID_SPELL", elem._entry0._IdSpell);
                    buffer.WriteDefaultString("ID_STK_D", version, elem._entry0._IdStkD);
                    buffer.WriteString("STR_STK_D", elem._entry0._StrStkD);
                }
                else if (elem._Type == 1)
                {
                    buffer.WriteDefaultString("ID_PLAYER2", version, elem._entry1._IdPlayer2);
                    buffer.WriteDefaultString("ID_STK_A", version, elem._entry1._IdStkA);
                    buffer.WriteString("STR_STK_A", elem._entry1._StrStkA);
                    buffer.WriteDefaultString("ID_STK_D", version, elem._entry1._IdStkD);
                    buffer.WriteString("STR_STK_D", elem._entry1._StrStkD);
                }
                else if (elem._Type == 2)
                {
                    buffer.WriteDefaultString("ID_STK_D", version, elem._entry2._IdStkD);
                    buffer.WriteDefaultString("STR_STK_D", version, elem._entry2._StrStkD);
                }
                else if (elem._Type == 3)
                {
                    buffer.WriteDefaultString("ID_CITY", version, elem._entry3._IdCity);
                }
                else if (elem._Type == 4)
                {

                }
                else if (elem._Type == 5)
                {
                    buffer.WriteDefaultString("ID_STK_D", version, elem._entry5._IdStkD);
                    buffer.WriteString("STR_STK_D", elem._entry5._StrStkD);
                }
                else if (elem._Type == 6)
                {
                    buffer.WriteDefaultString("ID_STK_D", version, elem._entry6._IdStkD);
                    buffer.WriteString("STR_STK_D", elem._entry6._StrStkD);
                    buffer.WriteBool("ACQUIRE", elem._entry6._Asquire);
                }
            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}