﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools
{
    public class MapHeaderBlock: DataBlock
    {
        public string _MapType = "D2EESFISIG";//"MidFile" for old maps
        public string _Version = "S143";
        public string _Description = "";
        public string _Author = "";
        public string _Name = "";
        public int _offset = 2760;
        public bool _strategyFirst = false;
        public int _mapSize = 48;
        public int[] _unknownUnt = new int[3] { 1, 0, 0 };
        public string _compaign = "C000CC0001";
        public BitArray _unknownBits = new BitArray(new Byte[]{ 1, 0, 0, 0, 1});
        public string _playerName = "C000CC0001";
        public byte[] _unknownData;
        public int _unknownInt1 = 190;
        public BitArray _unknownPadding;
        public byte[] _unknownData2;
        public byte[] _unknownData3;
        
        public void EmptyInit()
        {
            _unknownData = new byte[1065 - 6];
            for (int i = 0; i < _unknownData.Length; ++i)
                _unknownData[i] = 0;
            _unknownData2 = new byte[1000];
            for (int i = 0; i < _unknownData2.Length; ++i)
                _unknownData2[i] = 0;
        }

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.ByteSearch(array, Helper.stringToByteArray("WHAT"), 0);
            _MapType = Helper.ReadStringWithTerm(ref array, ref index);
            if (_MapType == "D2EESFISIG")
            {
                index = 16;
                _offset = Helper.ReadInt(ref array, ref index, 4);
                index = 43;
                _Description = Helper.ReadStringWithTerm(ref array, ref index);
                index = 43 + 256;
                _Author = Helper.ReadStringWithTerm(ref array, ref index);
                index = 43 + 256 + 21;
                _strategyFirst = Helper.ReadBool(ref array, ref index);
                _Name = Helper.ReadStringWithTerm(ref array, ref index);
                index += 64;
                index += 192;
                _mapSize = Helper.ReadInt(ref array, ref index, 4);
                _unknownUnt[0] = Helper.ReadInt(ref array, ref index, 4);
                _unknownUnt[1] = Helper.ReadInt(ref array, ref index, 4);
                _unknownUnt[2] = Helper.ReadInt(ref array, ref index, 4);
                _compaign = Helper.ReadString(ref array, ref index, 10);
                _unknownBits = Helper.ReadBitArray(ref array, ref index, 5);
                _playerName = Helper.ReadStringWithTerm(ref array, ref index);
                index += 16;
                _unknownData = Helper.ReadRange(ref array, ref index, 1065 - 6);
                _unknownInt1 = Helper.ReadInt(ref array, ref index, 4);
                _unknownData2 = Helper.ReadRange(ref array, ref index, 250 * 4);
                int unknownPaddingSize = Helper.ReadInt(ref array, ref index, 4);
                _unknownData3 = Helper.ReadRange(ref array, ref index, _offset - 2668 - 8);

                if (unknownPaddingSize > 0)
                    _unknownPadding = Helper.ReadBitArray(ref array, ref index, unknownPaddingSize);
            }
            else
            {
                index = 16;
                _offset = Helper.ReadInt(ref array, ref index, 4);
                index = 42;
                _Description = Helper.ReadStringWithTerm(ref array, ref index);
                index = 42 + 256;
                _Author = Helper.ReadStringWithTerm(ref array, ref index);
                index = 42 + 256 + 21;
                _strategyFirst = Helper.ReadBool(ref array, ref index);
                _Name = Helper.ReadStringWithTerm(ref array, ref index);
                endIndex = Helper.ByteSearch(array, Helper.stringToByteArray("WHAT"), index);
                index = 576;
                _mapSize = Helper.ReadInt(ref array, ref index, 4);
                ReadRange(array, 42 + 256 + 22 + 64, endIndex - 14);
            }
            _Version = Helper.ReadString(array, endIndex - 14, 4);

            index =  endIndex;
        }

        public int ReadRange(byte[] array, int index, int lastIndex)
        {
            _Data = new byte[lastIndex - index];
            Array.Copy(array, index, _Data, 0, lastIndex - index);
            return lastIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            int objCount = map.DataList.Count;
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            if (_MapType == "D2EESFISIG")
            {
                buffer.WriteSimpleString(_MapType);
                buffer.Write(new byte[] { 0x0, 0x0 });
                buffer.WriteDefaultInt("", -1);
                buffer.WriteDefaultInt("", _offset);
                buffer.Write(new byte[] { 1, 0, 0, 0, 0x23, 0, 0, 0, 0, 0, 0, 0 });
                buffer.WriteSimpleString(_Version + "SC0000");
                buffer.WriteSimpleString("\0");
                buffer.WriteSimpleString(_Description);
                buffer.WriteRepitableString("\0", 256 - _Description.Length);
                buffer.WriteSimpleString(_Author);
                buffer.WriteRepitableString("\0", 21 - _Author.Length);
                buffer.WriteBool("", _strategyFirst);
                buffer.WriteSimpleString(_Name);
                buffer.WriteRepitableString("\0", 64 - _Name.Length);
                buffer.WriteRepitableString("\0", 192);
                buffer.WriteDefaultInt("", _mapSize);
                buffer.WriteDefaultInt("", _unknownUnt[0]);
                buffer.WriteDefaultInt("", _unknownUnt[1]);
                buffer.WriteDefaultInt("", _unknownUnt[2]);
                buffer.WriteSimpleString(_compaign);
                buffer.Write(new byte[] { 0 });
                buffer.WriteBitArray(ref _unknownBits);
                buffer.WriteSimpleString(_playerName);
                buffer.WriteRepitableString("\0", 16 - _playerName.Length);
                buffer.Write(_unknownData);
                buffer.WriteDefaultInt("", _unknownInt1);
                buffer.Write(_unknownData2);
                if (_unknownPadding != null)
                    buffer.WriteDefaultInt("", _unknownPadding.Length / 8);
                else
                    buffer.WriteDefaultInt("", 0);

                buffer.Write(_unknownData3);
                if (_unknownPadding != null && _unknownPadding.Length > 0)
                    buffer.WriteBitArray(ref _unknownPadding);

                buffer.WriteSimpleString(_Version);
                buffer.WriteSimpleString("OB0000");
                buffer.Write(Helper.IntToByteArray(objCount, 4));
                return buffer.GetData();
            }
            buffer.WriteSimpleString(_MapType);
            buffer.WriteSimpleString("\0\0\0\0\0\0\0\0\0");
            buffer.WriteDefaultInt("", _offset);
            buffer.WriteSimpleString(_Version + "SC0000");
            buffer.WriteRepitableString("\0", 12);
            buffer.WriteSimpleString(_Description);
            buffer.WriteRepitableString("\0", 256 - _Description.Length);
            buffer.WriteSimpleString(_Author);
            buffer.WriteRepitableString("\0", 21 - _Author.Length);
            buffer.WriteBool("", _strategyFirst);
            buffer.WriteSimpleString(_Name);
            buffer.WriteRepitableString("\0", 64 - _Name.Length);

            buffer.Write(_Data);
            buffer.WriteSimpleString(_Version);
            buffer.WriteSimpleString("OB0000");
            buffer.Write(Helper.IntToByteArray(objCount, 4));
            return buffer.GetData();
        }

        private byte[] _Data;

        public string BlockType() { return "MapHeader"; }
        public string ObjId() { return "map"; }
    }
}
