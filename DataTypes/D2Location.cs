﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevendaarTools
{
    public class D2Location : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x13, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidLocation@@\0"));
        }

        public static string Type()
        {
            return "Location";
        }

        private byte[] _header = Header();
        public string _objId;

        public int _posX;
        public int _posY;
        public int _radius;// 0 - 1x1, 1 - 3x3, 2 - 5x5
        public string _name;

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.ByteSearch(array, Helper.stringToByteArray("ENDOBJECT\0"), index) + 10;

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _posX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _posY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _name = Helper.ReadStringWithLenght(ref array, ref index, "NAME_TXT");
            _radius = Helper.ReadDefaultInt(ref array, ref index, "RADIUS");

            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("LOC_ID", version, _objId);
            buffer.WriteDefaultInt("POS_X", _posX);
            buffer.WriteDefaultInt("POS_Y", _posY);
            buffer.WriteString("NAME_TXT", _name);
            buffer.WriteDefaultInt("RADIUS", _radius);
            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}
