using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2SceneVariables : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x18, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidScenVariables@@\0"));
        }

        public static string Type()
        {
            return "SceneVariables";
        }

        private byte[] _header = Header();
        public string _objId = "SV0000";

        public class SceneVariable
        {
            public int _Id;
            public string _Name;
            public int _Value;

        } 
        public List <SceneVariable> _Variables = new List<SceneVariable>();


        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                SceneVariable elem = new SceneVariable();
                elem._Id = Helper.ReadDefaultInt(ref array, ref index, "ID");
                elem._Name = Helper.ReadStringWithLenght(ref array, ref index, "NAME");
                elem._Value = Helper.ReadDefaultInt(ref array, ref index, "VALUE");
                _Variables.Add(elem);
            } 


            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultInt(version + _objId, _Variables.Count);
            for (int i = 0; i < _Variables.Count; ++i) 
            {
                SceneVariable elem = _Variables[i];
                buffer.WriteDefaultInt( "ID", elem._Id);
                buffer.WriteString( "NAME", elem._Name);
                buffer.WriteDefaultInt( "VALUE", elem._Value);

            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}