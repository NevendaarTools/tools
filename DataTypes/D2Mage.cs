using System;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2Mage : D2Site, DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x13, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidSiteMage@@\0"));
        }

        public static string Type()
        {
            return "Mage";
        }

        private byte[] _header = Header();
        public string _objId = "SI0000";

        public List <string> _Spells = new List<String>();
        public List <SiteVisitor> _Visitors = new List<SiteVisitor>();
        public int _ImgIso = 0;
        public string _ImgIntf = "";
        public string _Title = "";
        public string _Desc = "";
        public int _PosX = 0;
        public int _PosY = 0;
        public string _Visiter = "000000";
        public int _AiPriority = 3;


        void DataBlock.Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

            _objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            _ImgIso = Helper.ReadDefaultInt(ref array, ref index, "IMG_ISO");
            _ImgIntf = Helper.ReadStringWithLenght(ref array, ref index, "IMG_INTF");
            _Title = Helper.ReadStringWithLenght(ref array, ref index, "TXT_TITLE");
            _Desc = Helper.ReadStringWithLenght(ref array, ref index, "TXT_DESC");
            _PosX = Helper.ReadDefaultInt(ref array, ref index, "POS_X");
            _PosY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
            _Visiter = Helper.ReadDefaultString(ref array, ref index, "VISITER");
            _AiPriority = Helper.ReadDefaultInt(ref array, ref index, "AIPRIORITY");

            int count = Helper.ReadDefaultInt(ref array, ref index, "QTY_SPELL");
            for (int i = 0; i < count; ++i)
            {
                _Spells.Add(Helper.ReadStringWithLenght(ref array, ref index, "SPELL_ID"));
            }

            count = Helper.ReadDefaultInt(ref array, ref index, _objId);
            for (int i = 0; i < count; ++i)
            {
                SiteVisitor elem = new SiteVisitor();
                elem._SiteId = Helper.ReadDefaultString(ref array, ref index, "SITE_ID");
                elem._Visiter = Helper.ReadDefaultString(ref array, ref index, "VISITER");
                _Visitors.Add(elem);
            }

            index = endIndex;
        }

        byte[] DataBlock.Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
            buffer.WriteDefaultString("SITE_ID", version, _objId);
            buffer.WriteDefaultInt("IMG_ISO", _ImgIso);
            buffer.WriteString("IMG_INTF", _ImgIntf);
            buffer.WriteString("TXT_TITLE", _Title);
            buffer.WriteString("TXT_DESC", _Desc);
            buffer.WriteDefaultInt("POS_X", _PosX);
            buffer.WriteDefaultInt("POS_Y", _PosY);
            buffer.WriteDefaultString("VISITER", _Visiter);
            buffer.WriteDefaultInt("AIPRIORITY", _AiPriority);
            buffer.WriteDefaultInt("QTY_SPELL", _Spells.Count);
            for (int i = 0; i < _Spells.Count; ++i)
            {
                buffer.WriteString("SPELL_ID", _Spells[i]);
            }
            buffer.WriteDefaultInt(version + _objId, _Visitors.Count);
            for (int i = 0; i < _Visitors.Count; ++i)
            {
                SiteVisitor elem = _Visitors[i];
                buffer.WriteDefaultString("SITE_ID", elem._SiteId);
                buffer.WriteDefaultString("VISITER", version, elem._Visiter);

            }
            buffer.WriteEnd();

            return buffer.GetData();
        }

        string DataBlock.BlockType() { return Type(); }
        string DataBlock.ObjId() { return _objId; }
    }
}