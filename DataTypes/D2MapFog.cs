using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace NevendaarTools.DataTypes
{
    public class D2MapFog : DataBlock
    {
        public static byte[] Header()
        {
            return Helper.Merge(Helper.stringToByteArray("WHAT"), new byte[] { 0x15, 0, 0, 0 }, Helper.stringToByteArray(".?AVCMidgardMapFog@@\0"));
        }

        public static string Type()
        {
            return "MapFog";
        }

        private byte[] _header = Header();
        public string _objId = "FG0000";

        public class FogEntry
        {
            public int _posY;
            public BitArray _fog;
        } 
        public List <FogEntry> _entries = new List<FogEntry>();

        public void Read(ref byte[] array, ref int index, ref MapModel map)
        {
            int endIndex = Helper.EndIndex(ref array, index);

			_objId = Helper.ReadDefaultString(ref array, ref index, "OBJ_ID");
            int count = Helper.ReadDefaultInt(ref array, ref index, _objId); 
            for (int i = 0; i < count; ++i) 
            {
                FogEntry elem = new FogEntry();
                elem._posY = Helper.ReadDefaultInt(ref array, ref index, "POS_Y");
                int byteCount = Helper.ReadDefaultInt(ref array, ref index, "FOG");
                elem._fog = Helper.ReadBitArray(ref array, ref index, byteCount);
                _entries.Add(elem);
            } 
            
            index = endIndex;
        }

        public byte[] Data(ref MapModel map)
        {
            string version = map.Header._Version;
            ByteBuffer buffer = new ByteBuffer();

            buffer.Write(_header);
            buffer.WriteDefaultString("OBJ_ID", version, _objId);
            buffer.WriteBegin();
                        
            buffer.WriteDefaultInt(version + _objId, _entries.Count);
            for (int i = 0; i < _entries.Count; ++i) 
            {
                FogEntry elem = _entries[i];
                buffer.WriteDefaultInt("POS_Y", elem._posY);
                buffer.WriteDefaultInt("FOG", (elem._fog.Length - 1) / 8 + 1);
                buffer.WriteBitArray(ref elem._fog);
            } 

            buffer.WriteEnd();

            return buffer.GetData();
        }

        public string BlockType() { return Type(); }
        public string ObjId() { return _objId; }
    }
}