﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;
using System.Linq;
using NevendaarTools.DataTypes;
using System.Collections;

namespace NevendaarTools
{
    public class ObjCompareData
    {
        public string type;
        public string objId;
        public byte[] data1;
        public byte[] data2;
        public DataBlock datablock1;
        public DataBlock datablock2;
    }

    public class ComparationReport
    {
        public MapModel map1;
        public MapModel map2;
        public string name1;
        public string name2;
        public List<string> log = new List<string>();
        public List<ObjCompareData> mismatched = new List<ObjCompareData>();
    }

    class D2MapComparator
    {
        public D2MapComparator()
        {
            
        }
        private MapReader _MapReader = new MapReader();
        private void Log(ComparationReport report, string text)
        {
            report.log.Add(text);
        }
        public ComparationReport Compare(MapModel map1, string name1, MapModel map2, string name2)
        {
            ComparationReport result = new ComparationReport() { name1 = name1, name2 = name2 };

            result.mismatched.Add(new ObjCompareData()
            {
                type = "Header",
                objId = "0000",
                data1 = map1.Header.Data(ref map1),
                data2 = map2.Header.Data(ref map2),
                datablock1 = map1.Header,
                datablock2 = map2.Header
            });
            CompareObjects(result, map1.Header, map2.Header);

            result.map1 = map1;
            result.map2 = map2;

            Dictionary<string, List<DataBlock>> mapping1 = new Dictionary<string, List<DataBlock>>();
            Dictionary<string, List<DataBlock>> mapping2 = new Dictionary<string, List<DataBlock>>();

            foreach (DataBlock block in map1.DataList)
            {
                string type = block.BlockType();
                if (!mapping1.ContainsKey(type))
                    mapping1.Add(type, new List<DataBlock>());
                mapping1[type].Add(block);
            }

            foreach (DataBlock block in map2.DataList)
            {
                string type = block.BlockType();
                if (!mapping2.ContainsKey(type))
                    mapping2.Add(type, new List<DataBlock>());
                mapping2[type].Add(block);
            }
            List<string> used = new List<string>();

            foreach (string type in mapping1.Keys)
            {
                string text = type + ": ";
                text += mapping1[type].Count.ToString() + " / ";
                if (mapping2.ContainsKey(type))
                {
                    text += mapping2[type].Count.ToString();
                    Log(result, text);
                    CompareObjects(ref map2, result, mapping1[type], mapping2[type], type);
                }
                else
                {
                    text += "-";
                    Log(result, text);
                    CompareObjects(ref map1, result, mapping1[type], new List<DataBlock>(), type);
                }

                used.Add(type);
            }
            foreach (string type in mapping1.Keys)
            {
                if (used.Contains(type))
                    continue;

                string text = type + ": - / " + mapping2[type].Count.ToString();
                Log(result, text);
                CompareObjects(ref map2, result, new List<DataBlock>(), mapping2[type], type);
            }
            return result;
        }

        public ComparationReport Compare(string path1, string path2)
        {
            string name1 = Path.GetFileNameWithoutExtension(path1) + ".sg";
            string name2 = Path.GetFileNameWithoutExtension(path2) + ".sg";

            MapModel map1 = new MapModel();
            MapModel map2 = new MapModel();

            _MapReader.ReadMap(map1, path1);
            _MapReader.ReadMap(map2, path2);

            return Compare(map1, name1, map2, name2);
        }

        public ComparationReport CompareStacks(string path1, string path2, GameModel model)
        {
            string name1 = Path.GetFileNameWithoutExtension(path1) + ".sg";
            string name2 = Path.GetFileNameWithoutExtension(path2) + ".sg";

            MapModel map1 = new MapModel();
            MapModel map2 = new MapModel();

            _MapReader.ReadMap(map1, path1);
            _MapReader.ReadMap(map2, path2);

            ComparationReport result = new ComparationReport() { name1 = name1, name2 = name2 };

            List<D2Stack> stacks1 = map1.GetListByType<D2Stack>();
            List<D2Stack> stacks2 = map2.GetListByType<D2Stack>();

            List<D2Unit> units1 = map1.GetListByType<D2Unit>();
            List<D2Unit> units2 = map2.GetListByType<D2Unit>();

            List<D2Item> items1 = map1.GetListByType<D2Item>();
            List<D2Item> items2 = map2.GetListByType<D2Item>();

            result.log.Add("Stacks :" + stacks1.Count.ToString() + "/" + stacks2.Count.ToString());
            int minIndex = Math.Min(stacks1.Count, stacks2.Count);

            for (int i = 0; i < minIndex; ++i)
            {
                string desc1 = DescriptionHelper.StackDescription2(stacks1[i], units1, items1, model);
                string desc2 = DescriptionHelper.StackDescription2(stacks2[i], units2, items2, model);
                if (desc1 == desc2)
                {
                    //Log(result, stacks1[i].ObjId() + " +");
                    //Log(result, desc1);
                }
                else
                {
                    Log(result, stacks1[i].ObjId() + " -");
                    Log(result, desc1);
                    Log(result, desc2);
                }
            }

            return result;
        }

        public static string ToString(Object obj)
        {
            if (obj == null)
                return "null";
            if (obj is IList list)
            {
                var sb = new StringBuilder();
                sb.Append(list.Count.ToString());
                sb.Append(" = [");
                for (int i = 0; i < list.Count; i++)
                {
                    sb.Append(ToString(list[i]));
                    if (i < list.Count - 1)
                    {
                        sb.Append(", ");
                    }
                }
                sb.Append("]");
                return sb.ToString();
            }
            else if (obj is Array array)
            {
                var sb = new StringBuilder();
                sb.Append(array.Length.ToString());
                sb.Append(" = [");
                for (int i = 0; i < array.Length; i++)
                {
                    sb.Append(ToString(array.GetValue(i)));
                    if (i < array.Length - 1)
                    {
                        sb.Append(", ");
                    }
                }
                sb.Append("]");
                return sb.ToString();
            }
            else if (obj is ValueType || obj is string)
            {
                return obj.ToString();
            }
            else
            {
                var properties = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public);
                var sb = new StringBuilder();
                sb.Append("{");
                for (int i = 0; i < properties.Length; i++)
                {
                    var value = properties[i].GetValue(obj);
                    sb.Append(properties[i].Name + ":" + ToString(value));
                    if (i < properties.Length - 1)
                    {
                        sb.Append(",\n ");
                    }
                }
                sb.Append("}\n");
                return sb.ToString();
            }
        }

        public static bool CompareArray(Array array1, Array array2)
        {
            if (array1.Length != array2.Length)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < array1.Length; i++)
                {
                    if (!array1.GetValue(i).Equals(array2.GetValue(i)))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public static bool CompareList(IList array1, IList array2)
        {
            if (array1.Count != array2.Count)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < array1.Count; i++)
                {
                    if (!array1[i].Equals(array2[i]))
                    {
                        return false;
                    }
                }
                return true;
            }
        }


        private void CompareObjects(ComparationReport report, Object obj1, Object obj2)
        {
            var type = obj1.GetType();
            var fields = type.GetFields();// (/*BindingFlags.Public | BindingFlags.Instance*/);
            foreach (var property in fields)
            {
                var value1 = property.GetValue(obj1);
                var value2 = property.GetValue(obj2);
                if (value1 == null && value2 == null)
                    continue;
                if (value1 is IList && value2 is IList)
                {
                    if (!CompareList((IList)value1, (IList)value2))
                    {
                        Log(report, property.Name + " does not match. \nv1 = " + ToString(value1) + "\nv2 = " + ToString(value2));
                    }

                }
                else if (value1 is Array && value2 is Array)
                {
                    if (!CompareArray((Array)value1, (Array)value2))
                    {
                        Log(report, property.Name + " does not match. \nv1 = " + ToString(value1) + "\nv2 = " + ToString(value2));
                    }
                    
                }
                else if (value1 == null)
                {
                    Log(report, property.Name + " does not match. \nv1 = " + ToString(value1) + "\nv2 = " + ToString(value2));
                }
                else if (!value1.Equals(value2))
                {
                    //Console.WriteLine(property.Name + " does not match. v1 = " + value1.ToString() + " v2 = " + value2.ToString());
                    Log(report, property.Name + " does not match. \nv1 = " + ToString(value1) + "\nv2 = " + ToString(value2));
                }
            }
        }

        private void CompareObjects(ref MapModel map, ComparationReport report, List<DataBlock> list1, List<DataBlock> list2, string type)
        {
            List<string> used = new List<string>();

            foreach (DataBlock block1 in list1)
            {
                bool found = false;
                foreach (DataBlock block2 in list2)
                {
                    if (block1.ObjId().OrdinalEquals(block2.ObjId()))
                    {
                        found = true;
                        used.Add(block1.ObjId());
                        bool equal = block1.Data(ref map).SequenceEqual(block2.Data(ref map));
                        string text = block1.ObjId();
                        if (equal)
                        {
                            text = "";
                        }
                        else
                        {
                            Log(report, "object mismatch : " + block1.ObjId());
                            CompareObjects(report, block1, block2);
                            report.mismatched.Add(new ObjCompareData()
                            {
                                type = type,
                                objId = block1.ObjId(),
                                data1 = block1.Data(ref map),
                                data2 = block2.Data(ref map),
                                datablock1 = block1,
                                datablock2 = block2
                            });
                            text += " ------------ ";
                        }
                        if (text != "")
                            Log(report, text);
                        break;
                    }
                }
                if (!found)
                {
                    Log(report, block1.ObjId() + " exists only in first file!");
                    report.mismatched.Add(new ObjCompareData()
                    {
                        type = type,
                        objId = block1.ObjId(),
                        data1 = block1.Data(ref map),
                        data2 = new byte[0],
                        datablock1 = block1,
                        datablock2 = null
                    });
                }
            }

            foreach (DataBlock block1 in list2)
            {
                if (used.Contains(block1.ObjId(), StringComparer.OrdinalIgnoreCase))
                    continue;

                Log(report, block1.ObjId() + " exists only in second file!");
                report.mismatched.Add(new ObjCompareData()
                {
                    type = type,
                    objId = block1.ObjId(),
                    data1 = new byte[0],
                    data2 = block1.Data(ref map),
                    datablock1 = null,
                    datablock2 = block1
                });
            }
        }
    }
}
