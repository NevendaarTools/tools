﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NevendaarTools
{
    public class Logger
    {
        public enum LogLevel
        {
            Error = 0,
            Warning,
            Info
        }

        private static Logger _instance = null;
        public delegate void LogDelegate(LogLevel level, string text);
        public LogDelegate _LogDelegate;
        private void Log(LogLevel level, string text)
        {
            _LogDelegate?.Invoke(level, text);
        }
        Logger() { }
        public static Logger Instance()
        {
            if (_instance == null)
                _instance = new Logger();

            return _instance;
        }

        public static void LogError(string text)
        {
            Instance().Log(LogLevel.Error, text);
        }
        public static void LogWarning(string text)
        {
            Instance().Log(LogLevel.Warning, text);
        }
        public static void Log(string text)
        {
            Instance().Log(LogLevel.Info, text);
        }
    }
}
