﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NevendaarTools
{    
    public class MapModel
    {
        MapHeaderBlock _Header = new MapHeaderBlock();
        List<DataBlock> _DataList = new List<DataBlock>();
        
        public List<DataBlock> DataList { get => _DataList;}
        public MapHeaderBlock Header { get => _Header; set => _Header = value; }

        public void ClearBlocks()
        {
            DataList.Clear();
        }

        public void AddDataBlock(DataBlock block)
        {
            DataList.Add(block);
        }

        public void InsertDataBlick(int index, DataBlock block)
        {
            DataList.Insert(index, block);
        }

        public DataBlock GetBlockById(string id)
        {
            for (int i = 0; i < _DataList.Count; i++)
            {
                if (_DataList[i].ObjId() == id)
                {
                    return DataList[i];
                }
            }
            return null;
        }

        public T GetById<T>(string id) where T : class, DataBlock
        {
            for (int i = 0; i < _DataList.Count; i++)
            {
                if (_DataList[i].ObjId().OrdinalEquals(id))
                {
                    return DataList[i] as T;
                }
            }
            return null;
        }

        public DataBlock PopById(string id)
        {
            for (int i = 0; i < _DataList.Count; i++)
            {
                if (_DataList[i].ObjId() == id)
                {
                    DataBlock result = DataList[i];
                    DataList.RemoveAt(i);
                    return result;
                }
            }
            return null;
        }

        public List<T> GetListByType<T>() where T: class, DataBlock
        {
            List<T> result = new List<T>();
            int index = 0;
            foreach(DataBlock block in _DataList)
            {
                if (block is T)
                {
                    T resultBlock = block as T;
                    result.Add(resultBlock);
                }
                index++;
            }
            return result;
        }
        public List<DataBlock> GetListByTypeName(string name) 
        {
            List<DataBlock> result = new List<DataBlock>();
            int index = 0;
            foreach (DataBlock block in _DataList)
            {
                if (block.BlockType() == name)
                {
                    result.Add(block);
                }
                index++;
            }
            return result;
        }


        public List<int> GetAvailableIds<T>(int count = 65536) where T : class, DataBlock
        {
            List<T> list = GetListByType<T>();
            bool [] used = new bool[count];
            for (int i = 0; i < used.Length; i++)
                used[i] = false;

            foreach (T item in list)
            {
                string number = item.ObjId().Substring(2);
                int num = int.Parse(number, System.Globalization.NumberStyles.HexNumber);
                used[num] = true;
            }

            List<int> result = new List<int>();
            for (int i = 0; i < used.Length; i++)
                if (!used[i])
                    result.Add(i);

            return result;
        }

        public bool RemoveDataBlockById(string id)
        {
            if (id == "G000000000" || id == "000000")
                return true;

            for (int i = 0; i < _DataList.Count; i++)
            {
                if (_DataList[i].ObjId() == id)
                {
                    _DataList.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }
        public bool Replace(DataBlock block)
        {
            return ReplaceById(block.ObjId(), block);
        }

        public bool ReplaceById(string id, DataBlock block)
        {
            for (int i = 0; i < _DataList.Count; i++)
            {
                if (_DataList[i].ObjId() == id)
                {
                    _DataList[i] = block;
                    return true;
                }
            }
            return false;
        }
    }
}
