﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NevendaarTools.DataTypes;

namespace NevendaarTools
{
    public class MapReader
    {
        public delegate void LogDelegate(string str);
        private bool _TestMode = false;

        Dictionary<byte[], DataBlockFactory> _FactoryBinding = new Dictionary<byte[], DataBlockFactory>();
        DataBlockFactory _DefaultFactory = new Factory<TagDataBlock>();
        public List<string> _DataTypes = new List<string>();

        public MapReader()
        {
            RegisterFactory(D2Unit.Header(), new Factory<D2Unit>());
            RegisterFactory(D2Location.Header(), new Factory<D2Location>());
            RegisterFactory(D2Plan.Header(), new Factory<D2Plan>());
            RegisterFactory(D2Stack.Header(), new Factory<D2Stack>());
            RegisterFactory(D2Item.Header(), new Factory<D2Item>());
            RegisterFactory(D2Capital.Header(), new Factory<D2Capital>());
            RegisterFactory(D2Village.Header(), new Factory<D2Village>());
            RegisterFactory(D2Ruin.Header(), new Factory<D2Ruin>());
            RegisterFactory(D2StackTemplate.Header(), new Factory<D2StackTemplate>());
            RegisterFactory(D2TalismanCharges.Header(), new Factory<D2TalismanCharges>());
            RegisterFactory(D2Bag.Header(), new Factory<D2Bag>());
            RegisterFactory(D2MapBlock.Header(), new Factory<D2MapBlock>());
            RegisterFactory(D2Mountains.Header(), new Factory<D2Mountains>());
            RegisterFactory(D2ScenarioInfo.Header(), new Factory<D2ScenarioInfo>());
            RegisterFactory(D2Map.Header(), new Factory<D2Map>());
            RegisterFactory(D2Player.Header(), new Factory<D2Player>());
            RegisterFactory(D2MapFog.Header(), new Factory<D2MapFog>());
            RegisterFactory(D2PlayerBuildings.Header(), new Factory<D2PlayerBuildings>());
            RegisterFactory(D2PlayerSpells.Header(), new Factory<D2PlayerSpells>());
            RegisterFactory(D2Diplomacy.Header(), new Factory<D2Diplomacy>());
            RegisterFactory(D2QuestLog.Header(), new Factory<D2QuestLog>());
            RegisterFactory(D2TurnSummary.Header(), new Factory<D2TurnSummary>());
            RegisterFactory(D2SubRace.Header(), new Factory<D2SubRace>());
            RegisterFactory(D2StackDestroyed.Header(), new Factory<D2StackDestroyed>());
            RegisterFactory(D2SpellCast.Header(), new Factory<D2SpellCast>());
            RegisterFactory(D2SceneVariables.Header(), new Factory<D2SceneVariables>());
            RegisterFactory(D2SpellEffects.Header(), new Factory<D2SpellEffects>());
            RegisterFactory(D2Merchant.Header(), new Factory<D2Merchant>());
            RegisterFactory(D2Mercs.Header(), new Factory<D2Mercs>());
            RegisterFactory(D2LandMark.Header(), new Factory<D2LandMark>());
            RegisterFactory(D2Crystal.Header(), new Factory<D2Crystal>());
            RegisterFactory(D2Trainer.Header(), new Factory<D2Trainer>());
            RegisterFactory(D2Mage.Header(), new Factory<D2Mage>());
            RegisterFactory(D2Road.Header(), new Factory<D2Road>());
        }

        public void RegisterFactory(byte[] header, DataBlockFactory factory)
        {
            _FactoryBinding.Add(header, factory);
        }

        public bool ReadMap(MapModel map, string filename)
        {
            using (FileStream fstream = File.OpenRead(filename))
            {
                if (fstream.Length < 1)
                    return false;

                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                return ReadMap(ref map, ref array);
            }
        }
        public MapModel ReadMap(string filename)
        {
            MapModel result = new MapModel();
            ReadMap(result, filename);
            return result;
        }


        public bool Test(string filename, LogDelegate log)
        {
            _TestMode = true;
            MapModel map = new MapModel();
            
            byte[] array;

            using (FileStream fstream = File.OpenRead(filename))
            {
                if (fstream.Length < 1)
                    return false;

                array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                ReadMap(ref map, ref array);
                foreach (DataBlock block in map.DataList)
                {
                    string type = block.BlockType();
                    if (!_DataTypes.Contains(type))
                        _DataTypes.Add(type);
                }
            }

            byte[] result = map.Header.Data(ref map);

            foreach (DataBlock block in map.DataList)
            {
                result = Helper.Merge(result, block.Data(ref map));
            }
            if (result.SequenceEqual(array))
            {
                log(filename + " parsed ok\n");
                return true;
            }
            else
            {
                log(filename + " parsed with errors\n");
                SaveMap(map, "test.sg");
                using (var fs = new FileStream("origin.sg", FileMode.Create, FileAccess.Write))
                {
                    fs.Write(array, 0, array.Length);
                }
                return false;
            }
        }

        public Object DataBlockToType(DataBlock block)
        {
            return FactoryByType(block.BlockType()).Convert(block);
        }

        public Object GetById(ref MapModel map, string name)
        {
            return DataBlockToType(map.GetBlockById(name));
        }

        public List<Object> GetListByType(ref MapModel map, string type)
        {
            List<Object> result = new List<object>();
            List<DataBlock> tmp = map.GetListByTypeName(type);
            for (int i = 0; i < tmp.Count; i++)
            {
                result.Add(DataBlockToType(tmp[i]));
            }
            return result;
        }

        public bool ReadMap(ref MapModel map, ref byte[] array)
        {
            int index = 0;
            map.Header.Read(ref array, ref index, ref map);
            map.ClearBlocks();

            while (true)
            {
                ReadBlock(ref map, ref array, ref index);
                if (index < 0)
                    break;
            }
            return true;
        }

        public bool Save(MapModel map, string path)
        {
            return SaveMap(map, path);
        }
        public static bool SaveMap(MapModel map, string path)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    byte[] header = map.Header.Data(ref map);
                    fs.Write(header, 0, header.Length);
                    
                    foreach (DataBlock block in map.DataList)
                    {
                        Console.WriteLine(block.BlockType() + " " + block.ObjId());
                        byte[] blockData = block.Data(ref map);
                        fs.Write(blockData, 0, blockData.Length);
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }

        DataBlockFactory FactoryByType(string type)
        {
            foreach (DataBlockFactory factory in _FactoryBinding.Values)
            {
                if (factory.Type() == type)
                {
                    return factory;
                }
            }
            return _DefaultFactory;
        }

        DataBlockFactory FactoryByHeader(byte[] header)
        {
            foreach (byte[] factoryHeader in _FactoryBinding.Keys)
            {
                if (header.SequenceEqual(factoryHeader))
                {
                    return _FactoryBinding[factoryHeader];
                }
            }
            return _DefaultFactory;
        }

        void ReadBlock(ref MapModel map, ref byte[] array, ref int index)
        {
            int headerEndIndex = Helper.ByteSearch(array, Helper.stringToByteArray("@@\0"), index) + 3;
            if (headerEndIndex < 3)
            {
                index = -1;
                return;
            }
            int initialIndex = index;
            int initialIndex2 = index;
            int size = headerEndIndex - index;
            byte[] header = new byte[size];
            Array.Copy(array, index, header, 0, size);
            DataBlock res = FactoryByHeader(header).Read(ref array, ref index, ref map);
            if (index < initialIndex)
            {
                int gg = 0; gg++;
            }
            if (_TestMode)
            {
                TagDataBlock tmp = new TagDataBlock();
                tmp.Read(ref array, ref initialIndex2, ref map);
                byte[] result = tmp.Data(ref map);
                byte[] localData = res.Data(ref map);
                bool equal = localData.SequenceEqual(result);
                if (!equal)
                {
                    tmp = new TagDataBlock();
                    tmp.Read(ref array, ref initialIndex, ref map);
                    File.WriteAllBytes("Compare_before.sg", result);
                    File.WriteAllBytes("Compare_after.sg", localData);
                    int gg = 0; gg++;
                }
            }
            map.AddDataBlock(res);
        }
    }
}
