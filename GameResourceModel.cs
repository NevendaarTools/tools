﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace NevendaarTools
{
    public class GameResourceModel
    {
        public delegate void LogDelegate(string text);
        public LogDelegate _LogDelegate;
        private void Log(string str)
        {
            _LogDelegate?.Invoke(str);
        }
        public struct Mountain
        {
            public string name;
            public int size;
        }
        
        public List<string> ruins = new List<string>();
        public List<string> mages = new List<string>();
        public List<string> mercs = new List<string>();
        public List<string> merhs = new List<string>();
        public List<string> trainers = new List<string>();
        public List<Mountain> mountains = new List<Mountain>();
        public List<string> mines = new List<string>() { "G000CR0000GL", "G000CR0000RD", "G000CR0000YE", "G000CR0000RG", "G000CR0000WH", "G000CR0000GR" };
        public Dictionary<int, int> trees = new Dictionary<int, int>();

        public Dictionary<string, GameResource> resources = new Dictionary<string, GameResource>();

        public void Clear()
        {
            ruins.Clear();
            mages.Clear();
            mercs.Clear();
            merhs.Clear();
            trainers.Clear();
            mountains.Clear();
            resources.Clear();
            trees.Clear();
        }

        public static Bitmap CropImage(Bitmap source, Rectangle section)
        {
            var bitmap = new Bitmap(section.Width, section.Height);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.DrawImage(source, 0, 0, section, GraphicsUnit.Pixel);
                return bitmap;
            }
        }

        public static Image ResizeImage(Image imgToResize, Size size)
        {
            if (imgToResize == null)
                return new Bitmap(size.Width, size.Height);
            return (Image)(new Bitmap(imgToResize, size));
        }

        public static Image MakeTransparent(ref Image image)
        {
            return image;
            if (image.Palette.Entries.Count() > 0)
            {
                ColorPalette palette = image.Palette;
                Color[] entries = palette.Entries;
                entries[0] = Color.Transparent;
                for (int i = 1; i < entries.Count(); ++i)
                {
                    if (entries[i].R > 254 && entries[i].B > 254)
                        entries[i] = Color.FromArgb(0, entries[i].R, entries[i].G, entries[i].B);
                }
                image.Palette = palette;
                return image;
            }
            else
            {
                Bitmap processedBitmap = new Bitmap(image);
                BitmapData bitmapData = processedBitmap.LockBits(new Rectangle(0, 0, processedBitmap.Width, processedBitmap.Height), ImageLockMode.ReadWrite, processedBitmap.PixelFormat);

                int bytesPerPixel = Bitmap.GetPixelFormatSize(processedBitmap.PixelFormat) / 8;
                int byteCount = bitmapData.Stride * processedBitmap.Height;
                byte[] pixels = new byte[byteCount];
                IntPtr ptrFirstPixel = bitmapData.Scan0;
                Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
                int heightInPixels = bitmapData.Height;
                int widthInBytes = bitmapData.Width * bytesPerPixel;

                for (int y = 0; y < heightInPixels; y++)
                {
                    int currentLine = y * bitmapData.Stride;
                    for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        byte oldBlue = pixels[currentLine + x];
                        byte oldGreen = pixels[currentLine + x + 1];
                        byte oldRed = pixels[currentLine + x + 2];
                        byte oldAlpha = 255;
                        if (bytesPerPixel == 4)
                            oldAlpha = pixels[currentLine + x + 3];

                        pixels[currentLine + x + 0] = oldBlue;
                        pixels[currentLine + x + 1] = oldGreen;
                        pixels[currentLine + x + 2] = oldRed;

                        if (oldRed > 254 && oldBlue > 254)
                        {
                            if (oldGreen < 1 || oldAlpha < 5)
                                pixels[currentLine + x + 3] = 0;
                            else
                            {
                                int g = 0; g++;
                            }
                        }
                    }
                }

                // copy modified bytes back
                Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);
                processedBitmap.UnlockBits(bitmapData);
                return processedBitmap;
            }
        }



        public void Init(string path, GameModel model = null)
        {
            Log("GameResourceModel  initialisation");

            //LoadResourceDesc(path + "\\Imgs", "BatItems");
            if (true)
            {
                ReadObjects(path + "\\Imgs\\IsoCmon.ff");
                ReadObjects(path + "\\Imgs\\IsoAnim.ff");
                if (model != null)
                {
                    var tiles = model.GetAllT<GTileDBI>();
                    FillMountains(ref tiles);
                    FillTrees(ref tiles);
                }
                LoadResourceDesc(path + "\\Imgs", "IsoCmon");
                LoadResourceDesc(path + "\\Imgs", "IsoAnim");
                LoadResourceDesc(path + "\\Imgs", "Faces");
                LoadResourceDesc(path + "\\Imgs", "Icons");
                LoadResourceDesc(path + "\\Imgs", "Ground");
                LoadResourceDesc(path + "\\Imgs", "City");
                LoadResourceDesc(path + "\\Interf", "Interf");
                LoadResourceDesc(path + "\\Interf", "IntfScen");
                LoadResourceDesc(path + "\\Imgs", "IconBld");
                LoadResourceDesc(path + "\\Imgs", "IconSpel");
                LoadResourceDesc(path + "\\Imgs", "Lords");
                LoadResourceDesc(path + "\\Imgs", "Events");
                LoadResourceDesc(path + "\\Imgs", "PalMap");
                //LoadResourceDesc(path + "\\Imgs", "Wrapper");
                //LoadResourceDesc(path + "\\Imgs", "IsoClouds");
            }
                LoadResourceDesc(path + "\\Imgs", "IconItem");
            Log("GameResourceModel  initialised!");
        }

        public GameResource GetResource(string name)
        {
            if (resources.ContainsKey(name))
                return resources[name];
            return null;
        }
        public Image GetImageById(string resourceName, string id, bool makeTransparent = true)
        {
            if (resources.ContainsKey(resourceName))
            {
                List<Image> images = resources[resourceName].GetFramesById_(id, makeTransparent);
                if (images.Count > 0)
                {
                    Image result = images[0];
                    //if (makeTransparent)
                    //    return MakeTransparent(ref result);
                    return result;
                }
            }
            Console.WriteLine("failed to get image:" + resourceName + "->" + id);
            return new Bitmap(20, 20);
        }
        public Image GetImageById(string id, bool makeTransparent = true)
        {
            foreach (GameResource source in resources.Values)
            { 
                if (source.HasResource(id))
                {
                    List<Image> images = source.GetFramesById_(id, makeTransparent);
                    if (images.Count > 0)
                    {
                        Image result = images[0];
                        //if (makeTransparent)
                        //    return MakeTransparent(ref result);
                        return result;
                    }

                }
            }
                   
            Console.WriteLine("failed to get image:" + "->" + id);
            return new Bitmap(20, 20);
        }


        public bool HasResource(string id)
        {
            foreach (GameResource source in resources.Values)
                if (source.HasResource(id))
                    return true;
            return false;
        }

        public bool HasResource(string resourceName, string id)
        {
            if (resources.ContainsKey(resourceName))
            {
                return (resources[resourceName].HasResource(id));
            }
            return false;
        }

        public bool LoadResourceDesc(string path, string name)
        {
            string resultName = path + "/" + name + ".ff";
            if (!File.Exists(resultName))
            {
                Logger.LogError("Failed to load resource: " + resultName);
                return false;
            }
            GameResource resource = new GameResource();
            if (!resource.Read(resultName))
                return false;
            resources.Add(name, resource);
            return true;
        }

        static int SubraceNumberByShortRace(string race)
        {
            if (race == "HU")
                return 1;
            if (race == "UN")
                return 2;
            if (race == "HE")
                return 3;
            if (race == "DW")
                return 4;
            if (race == "NE")
                return 5;
            if (race == "EL")
                return 14;
            return 99;
        }

        public void FillTrees(ref List<GTileDBI> GTiles)
        {
            trees.Clear();
            for (int i = 0; i < GTiles.Count; i++)
            {
                if (GTiles[i].ground == "F")
                {
                    GTileDBI tile = GTiles[i];
                    Log(GTiles[i].terrain);
                    int race = SubraceNumberByShortRace(GTiles[i].terrain);
                    if (!trees.ContainsKey(race))
                    {
                        trees.Add(race, tile.qty);
                    }
                    else
                    {
                        trees[race] = trees[race] + tile.qty;
                    }
                }
            }
        }

        public void FillMountains(ref List<GTileDBI> GTiles)
        {
            mountains.Clear();
            for (int i = 0; i < GTiles.Count; i++)
            {
                if (GTiles[i].terrain == "NE" && GTiles[i].ground == "M")
                {
                    for (int k = 0; k < GTiles[i].qty; ++k)
                    {
                        string id = "MOMNE" + GTiles[i].ndx.ToString("D2") + k.ToString("D2");
                        mountains.Add(new Mountain { name = id, size = GTiles[i].ndx });
                        //Logger.Log(id);
                    }
                }
            }
        }

        public bool ReadObjects(string filename)
        {
            if (!File.Exists(filename))
            {
                Logger.LogError("failed to get objects from file(file not exist)! " + filename);
                return false;
            }
            using (FileStream fstream = File.OpenRead(filename))
            {
                if (fstream.Length < 1)
                    return false;

                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);

                fillList(ref ruins, ref array, "G000RU00000");
                fillList(ref mages, ref array, "G000SI0000MAGE");
                fillList(ref mercs, ref array, "G000SI0000MERC");
                fillList(ref merhs, ref array, "G000SI0000MERH");
                fillList(ref trainers, ref array, "G000SI0000TRAI");
            }
            return true;
        }

        public void fillList(ref List<string> list, ref byte [] array, string text)
        {
            int index = 0;
            byte[] search = Helper.stringToByteArray(text);

            while (index != -1)
            {
                index = Helper.ByteSearch(ref array, search, index);
                if (index < 0)
                    break;
                string id = Helper.ReadString(ref array, ref index, search.Length + 2);
                string ext = Helper.ReadString(ref array, ref index, 3);
                if (ext.OrdinalEquals("png"))
                {
                    //Log(id + "." + ext + " -skipped");
                    continue;
                }
                if (!list.Contains(id))
                {
                    list.Add(id);
                    //Log(id);
                }
            }
        }
    }
}
