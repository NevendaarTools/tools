﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace NevendaarTools
{
    public class SettingsManager
    {
        string settingsPath = "";
        private static SettingsManager _instance = null;

        private SettingsManager()
        {

        }

        public static SettingsManager Instance()
        {
            if (_instance == null)
                _instance = new SettingsManager();

            return _instance;
        }

        public static SettingsManager Instance(string path)
        {
            SettingsManager result = Instance();
            result.Load(path);
            return result;
        }

        private SettingsManager(string path)
        {
            Load(path);
        }

        ~SettingsManager()
        {
            if (settingsPath != "")
                Save(settingsPath);
        }

        Dictionary<string, string> _Settings = new Dictionary<string, string>();

        public bool Load(string filename)
        {
            if (settingsPath == filename)
                return true;
            settingsPath = filename;
            if (File.Exists(filename))// Checking the file if exist
            {
                try
                {
                    XmlDocument doc = new XmlDocument();
                    doc.Load(filename);

                    foreach (XmlNode node in doc.DocumentElement)
                    {
                        string key = node.Name;
                        string value = node.InnerText;
                        if (_Settings.ContainsKey(key))
                            _Settings[key] = value;
                        else
                            _Settings.Add(key, value);
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in process: {0}", ex);
                    Logger.LogError("Filed to read settings file: Exception caught in process: " + ex.Message);
                    return false;
                }
                
            }
            return false;
        }

        public string Value(string key, string defaultVal = "")
        {
            if (_Settings.ContainsKey(key))
                return _Settings[key];

            return defaultVal;
        }

        public void SetValue(string key, string value)
        {
            if (_Settings.ContainsKey(key))
                _Settings[key] = value;
            else
                _Settings.Add(key, value);
        }

        public bool HasValue(string key)
        {
            return _Settings.ContainsKey(key);
        }

        public bool Save(string filename)
        {
            XmlDocument doc = new XmlDocument();
            using (XmlWriter writer = doc.CreateNavigator().AppendChild())
            {
                // Do this directly 
                writer.WriteStartDocument();
                writer.WriteStartElement("settings");
                foreach (string key in _Settings.Keys)
                {
                    writer.WriteElementString(key, _Settings[key]);
                    
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                // or anything else you want to with writer, like calling functions etc.
            }
            doc.Save(filename);

            return true;
        }
    }
}
